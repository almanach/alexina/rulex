<?xml version="1.0" encoding="UTF-8"?>
<description lang="ru">
  <!-- INVARIABLE -->
  <table name="inv" rads="..*" fast="-">
    <form suffix="" tag=""/>
  </table>
  <table name="pref" rads="..*" fast="-">
    <form suffix="_" tag=""/>
  </table>
  <table name="UNK1" rads=".*">
    <!-- 19540 members -->
    <form  suffix="ий" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="его" tag="UNKA"/>
    <form  suffix="ее" tag="UNKA"/>
    <form  suffix="ей" tag="UNKA"/>
    <form  suffix="ем" tag="UNKA"/>
    <form  suffix="ему" tag="UNKA"/>
    <form  suffix="ею" tag="UNKA"/>
    <form  suffix="ие" tag="UNKA"/>
    <form  suffix="им" tag="UNKA"/>
    <form  suffix="ими" tag="UNKA"/>
    <form  suffix="их" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
  </table>
  <table name="UNK2" rads=".*">
    <!-- 16843 members -->
    <form  suffix="ый" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="ого" tag="UNKA"/>
    <form  suffix="ое" tag="UNKA"/>
    <form  suffix="ой" tag="UNKA"/>
    <form  suffix="ом" tag="UNKA"/>
    <form  suffix="ому" tag="UNKA"/>
    <form  suffix="ою" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
    <form  suffix="ые" tag="UNKA"/>
    <form  suffix="ым" tag="UNKA"/>
    <form  suffix="ыми" tag="UNKA"/>
    <form  suffix="ых" tag="UNKA"/>
  </table>
  <table name="UNK3" rads=".*">
    <!-- 16065 members -->
    <form  suffix="" tag="UNK"/>
  </table>
  <table name="UNK4" rads=".*">
    <!-- 8220 members -->
    <form  suffix="ийся" tag="UNK"/>
    <form  suffix="аяся" tag="UNKA"/>
    <form  suffix="егося" tag="UNKA"/>
    <form  suffix="ееся" tag="UNKA"/>
    <form  suffix="ейся" tag="UNKA"/>
    <form  suffix="емся" tag="UNKA"/>
    <form  suffix="емуся" tag="UNKA"/>
    <form  suffix="еюся" tag="UNKA"/>
    <form  suffix="иеся" tag="UNKA"/>
    <form  suffix="имися" tag="UNKA"/>
    <form  suffix="имся" tag="UNKA"/>
    <form  suffix="ихся" tag="UNKA"/>
    <form  suffix="уюся" tag="UNKA"/>
  </table>
  <table name="UNK5" rads=".*">
    <!-- 6711 members -->
    <form  suffix="" tag="UNK"/>
    <form  suffix="а" tag="UNKK"/>
    <form  suffix="ам" tag="UNKK"/>
    <form  suffix="ами" tag="UNKK"/>
    <form  suffix="ах" tag="UNKK"/>
    <form  suffix="е" tag="UNKK"/>
    <form  suffix="ов" tag="UNKK"/>
    <form  suffix="ом" tag="UNKK"/>
    <form  suffix="у" tag="UNKK"/>
    <form  suffix="ы" tag="UNKK"/>
  </table>
  <table name="UNK6" rads=".*">
    <!-- 5186 members -->
    <form  suffix="е" tag="UNK"/>
    <form  suffix="ем" tag="UNKJ"/>
    <form  suffix="и" tag="UNKJ"/>
    <form  suffix="ю" tag="UNKJ"/>
    <form  suffix="я" tag="UNKJ"/>
  </table>
  <table name="UNK7" rads=".*">
    <!-- 5129 members -->
    <form  suffix="" tag="UNK"/>
    <form  suffix="а" tag="UNKJ"/>
    <form  suffix="е" tag="UNKJ"/>
    <form  suffix="ом" tag="UNKJ"/>
    <form  suffix="у" tag="UNKJ"/>
  </table>
  <table name="UNK8" rads=".*">
    <!-- 4737 members -->
    <form  suffix="ный" tag="UNK"/>
    <form  suffix="ная" tag="UNKA"/>
    <form  suffix="ного" tag="UNKA"/>
    <form  suffix="ное" tag="UNKA"/>
    <form  suffix="ной" tag="UNKA"/>
    <form  suffix="ном" tag="UNKA"/>
    <form  suffix="ному" tag="UNKA"/>
    <form  suffix="ною" tag="UNKA"/>
    <form  suffix="ную" tag="UNKA"/>
    <form  suffix="ные" tag="UNKA"/>
    <form  suffix="ным" tag="UNKA"/>
    <form  suffix="ными" tag="UNKA"/>
    <form  suffix="ных" tag="UNKA"/>
    <form  suffix="" tag="UNKS"/>
    <form  suffix="а" tag="UNKS"/>
    <form  suffix="о" tag="UNKS"/>
    <form  suffix="ы" tag="UNKS"/>
  </table>
  <table name="UNK9" rads=".*">
    <!-- 4706 members -->
    <form  suffix="ь" tag="UNK"/>
    <form  suffix="и" tag="UNKF"/>
    <form  suffix="ью" tag="UNKF"/>
  </table>
  <table name="UNK10" rads=".*">
    <!-- 4089 members -->
    <form  suffix="ий" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="ие" tag="UNKA"/>
    <form  suffix="им" tag="UNKA"/>
    <form  suffix="ими" tag="UNKA"/>
    <form  suffix="их" tag="UNKA"/>
    <form  suffix="ого" tag="UNKA"/>
    <form  suffix="ое" tag="UNKA"/>
    <form  suffix="ой" tag="UNKA"/>
    <form  suffix="ом" tag="UNKA"/>
    <form  suffix="ому" tag="UNKA"/>
    <form  suffix="ою" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
  </table>
  <table name="UNK11" rads=".*">
    <!-- 3218 members -->
    <form  suffix="" tag="UNK"/>
    <form  suffix="а" tag="UNKK"/>
    <form  suffix="ам" tag="UNKK"/>
    <form  suffix="ами" tag="UNKK"/>
    <form  suffix="ах" tag="UNKK"/>
    <form  suffix="е" tag="UNKK"/>
    <form  suffix="и" tag="UNKK"/>
    <form  suffix="ов" tag="UNKK"/>
    <form  suffix="ом" tag="UNKK"/>
    <form  suffix="у" tag="UNKK"/>
  </table>
  <table name="UNK12" rads=".*">
    <!-- 2755 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="й" tag="UNKB"/>
    <form  suffix="йте" tag="UNKB"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="ю" tag="UNKM"/>
    <form  suffix="ют" tag="UNKM"/>
    <form  suffix="я" tag="UNKP"/>
  </table>
  <table name="UNK13" rads=".*">
    <!-- 2325 members -->
    <form  suffix="ка" tag="UNK"/>
    <form  suffix="кам" tag="UNKI"/>
    <form  suffix="ками" tag="UNKI"/>
    <form  suffix="ках" tag="UNKI"/>
    <form  suffix="ке" tag="UNKI"/>
    <form  suffix="ки" tag="UNKI"/>
    <form  suffix="кой" tag="UNKI"/>
    <form  suffix="кою" tag="UNKI"/>
    <form  suffix="ку" tag="UNKI"/>
    <form  suffix="ок" tag="UNKI"/>
  </table>
  <table name="UNK14" rads=".*">
    <!-- 2080 members -->
    <form  suffix="я" tag="UNK"/>
    <form  suffix="ей" tag="UNKH"/>
    <form  suffix="ею" tag="UNKH"/>
    <form  suffix="и" tag="UNKH"/>
    <form  suffix="ю" tag="UNKH"/>
  </table>
  <table name="UNK15" rads=".*">
    <!-- 1809 members -->
    <form  suffix="а" tag="UNK"/>
    <form  suffix="" tag="UNKI"/>
    <form  suffix="ам" tag="UNKI"/>
    <form  suffix="ами" tag="UNKI"/>
    <form  suffix="ах" tag="UNKI"/>
    <form  suffix="е" tag="UNKI"/>
    <form  suffix="ой" tag="UNKI"/>
    <form  suffix="ою" tag="UNKI"/>
    <form  suffix="у" tag="UNKI"/>
    <form  suffix="ы" tag="UNKI"/>
  </table>
  <table name="UNK16" rads=".*">
    <!-- 1700 members -->
    <form  suffix="а" tag="UNK"/>
    <form  suffix="е" tag="UNKH"/>
    <form  suffix="и" tag="UNKH"/>
    <form  suffix="ой" tag="UNKH"/>
    <form  suffix="ою" tag="UNKH"/>
    <form  suffix="у" tag="UNKH"/>
  </table>
  <table name="UNK17" rads=".*">
    <!-- 1270 members -->
    <form  suffix="а" tag="UNK"/>
    <form  suffix="е" tag="UNKH"/>
    <form  suffix="ой" tag="UNKH"/>
    <form  suffix="ою" tag="UNKH"/>
    <form  suffix="у" tag="UNKH"/>
    <form  suffix="ы" tag="UNKH"/>
  </table>
  <table name="UNK18" rads=".*">
    <!-- 1257 members -->
    <form  suffix="ый" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="ого" tag="UNKA"/>
    <form  suffix="ое" tag="UNKA"/>
    <form  suffix="ой" tag="UNKA"/>
    <form  suffix="ом" tag="UNKA"/>
    <form  suffix="ому" tag="UNKA"/>
    <form  suffix="ою" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
    <form  suffix="ые" tag="UNKA"/>
    <form  suffix="ым" tag="UNKA"/>
    <form  suffix="ыми" tag="UNKA"/>
    <form  suffix="ых" tag="UNKA"/>
    <form  suffix="" tag="UNKS"/>
    <form  suffix="а" tag="UNKS"/>
    <form  suffix="о" tag="UNKS"/>
    <form  suffix="ы" tag="UNKS"/>
  </table>
  <table name="UNK19" rads=".*">
    <!-- 1243 members -->
    <form  suffix="а" tag="UNK"/>
    <form  suffix="" tag="UNKI"/>
    <form  suffix="ам" tag="UNKI"/>
    <form  suffix="ами" tag="UNKI"/>
    <form  suffix="ах" tag="UNKI"/>
    <form  suffix="е" tag="UNKI"/>
    <form  suffix="ей" tag="UNKI"/>
    <form  suffix="ею" tag="UNKI"/>
    <form  suffix="у" tag="UNKI"/>
    <form  suffix="ы" tag="UNKI"/>
  </table>
  <table name="UNK20" rads=".*">
    <!-- 1057 members -->
    <form  suffix="о" tag="UNK"/>
    <form  suffix="а" tag="UNKJ"/>
    <form  suffix="е" tag="UNKJ"/>
    <form  suffix="ом" tag="UNKJ"/>
    <form  suffix="у" tag="UNKJ"/>
  </table>
  <table name="UNK21" rads=".*">
    <!-- 998 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="ю" tag="UNKM"/>
    <form  suffix="ют" tag="UNKM"/>
    <form  suffix="я" tag="UNKP"/>
  </table>
  <table name="UNK22" rads=".*">
    <!-- 970 members -->
    <form  suffix="ённый" tag="UNK"/>
    <form  suffix="ённая" tag="UNKA"/>
    <form  suffix="ённого" tag="UNKA"/>
    <form  suffix="ённое" tag="UNKA"/>
    <form  suffix="ённой" tag="UNKA"/>
    <form  suffix="ённом" tag="UNKA"/>
    <form  suffix="ённому" tag="UNKA"/>
    <form  suffix="ённою" tag="UNKA"/>
    <form  suffix="ённую" tag="UNKA"/>
    <form  suffix="ённые" tag="UNKA"/>
    <form  suffix="ённым" tag="UNKA"/>
    <form  suffix="ёнными" tag="UNKA"/>
    <form  suffix="ённых" tag="UNKA"/>
    <form  suffix="ена" tag="UNKS"/>
    <form  suffix="ено" tag="UNKS"/>
    <form  suffix="ены" tag="UNKS"/>
    <form  suffix="ён" tag="UNKS"/>
    <form  suffix="ёна" tag="UNKS"/>
    <form  suffix="ёно" tag="UNKS"/>
    <form  suffix="ёны" tag="UNKS"/>
  </table>
  <table name="UNK23" rads=".*">
    <!-- 947 members -->
    <form  suffix="е" tag="UNK"/>
    <form  suffix="ем" tag="UNKK"/>
    <form  suffix="и" tag="UNKK"/>
    <form  suffix="й" tag="UNKK"/>
    <form  suffix="ю" tag="UNKK"/>
    <form  suffix="я" tag="UNKK"/>
    <form  suffix="ям" tag="UNKK"/>
    <form  suffix="ями" tag="UNKK"/>
    <form  suffix="ях" tag="UNKK"/>
  </table>
  <table name="UNK24" rads=".*">
    <!-- 946 members -->
    <form  suffix="ка" tag="UNK"/>
    <form  suffix="ек" tag="UNKI"/>
    <form  suffix="кам" tag="UNKI"/>
    <form  suffix="ками" tag="UNKI"/>
    <form  suffix="ках" tag="UNKI"/>
    <form  suffix="ке" tag="UNKI"/>
    <form  suffix="ки" tag="UNKI"/>
    <form  suffix="кой" tag="UNKI"/>
    <form  suffix="кою" tag="UNKI"/>
    <form  suffix="ку" tag="UNKI"/>
  </table>
  <table name="UNK25" rads=".*">
    <!-- 894 members -->
    <form  suffix="ый" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="ого" tag="UNKA"/>
    <form  suffix="ое" tag="UNKA"/>
    <form  suffix="ой" tag="UNKA"/>
    <form  suffix="ом" tag="UNKA"/>
    <form  suffix="ому" tag="UNKA"/>
    <form  suffix="ою" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
    <form  suffix="ые" tag="UNKA"/>
    <form  suffix="ым" tag="UNKA"/>
    <form  suffix="ыми" tag="UNKA"/>
    <form  suffix="ых" tag="UNKA"/>
    <form  suffix="о" tag="UNKZ"/>
  </table>
  <table name="UNK26" rads=".*">
    <!-- 848 members -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="йся" tag="UNKB"/>
    <form  suffix="йтесь" tag="UNKB"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="емся" tag="UNKM"/>
    <form  suffix="етесь" tag="UNKM"/>
    <form  suffix="ется" tag="UNKM"/>
    <form  suffix="ешься" tag="UNKM"/>
    <form  suffix="юсь" tag="UNKM"/>
    <form  suffix="ются" tag="UNKM"/>
    <form  suffix="ясь" tag="UNKP"/>
  </table>
  <table name="UNK27" rads=".*">
    <!-- 814 members -->
    <form  suffix="ный" tag="UNK"/>
    <form  suffix="ная" tag="UNKA"/>
    <form  suffix="ного" tag="UNKA"/>
    <form  suffix="ное" tag="UNKA"/>
    <form  suffix="ной" tag="UNKA"/>
    <form  suffix="ном" tag="UNKA"/>
    <form  suffix="ному" tag="UNKA"/>
    <form  suffix="ною" tag="UNKA"/>
    <form  suffix="ную" tag="UNKA"/>
    <form  suffix="ные" tag="UNKA"/>
    <form  suffix="ным" tag="UNKA"/>
    <form  suffix="ными" tag="UNKA"/>
    <form  suffix="ных" tag="UNKA"/>
    <form  suffix="ен" tag="UNKS"/>
    <form  suffix="на" tag="UNKS"/>
    <form  suffix="но" tag="UNKS"/>
    <form  suffix="ны" tag="UNKS"/>
  </table>
  <table name="UNK28" rads=".*">
    <!-- 812 members -->
    <form  suffix="ой" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="ого" tag="UNKA"/>
    <form  suffix="ое" tag="UNKA"/>
    <form  suffix="ом" tag="UNKA"/>
    <form  suffix="ому" tag="UNKA"/>
    <form  suffix="ою" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
    <form  suffix="ые" tag="UNKA"/>
    <form  suffix="ым" tag="UNKA"/>
    <form  suffix="ыми" tag="UNKA"/>
    <form  suffix="ых" tag="UNKA"/>
  </table>
  <table name="UNK29" rads=".*">
    <!-- 751 members -->
    <form  suffix="ь" tag="UNK"/>
    <form  suffix="е" tag="UNKK"/>
    <form  suffix="ей" tag="UNKK"/>
    <form  suffix="ем" tag="UNKK"/>
    <form  suffix="и" tag="UNKK"/>
    <form  suffix="ю" tag="UNKK"/>
    <form  suffix="я" tag="UNKK"/>
    <form  suffix="ям" tag="UNKK"/>
    <form  suffix="ями" tag="UNKK"/>
    <form  suffix="ях" tag="UNKK"/>
  </table>
  <table name="UNK30" rads=".*">
    <!-- 731 members -->
    <form  suffix="" tag="UNK"/>
    <form  suffix="те" tag="UNKB"/>
  </table>
  <table name="UNK31" rads=".*">
    <!-- 686 members -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="ется" tag="UNKD"/>
    <form  suffix="ются" tag="UNKD"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
  </table>
  <table name="UNK32" rads=".*">
    <!-- 637 members -->
    <form  suffix="я" tag="UNK"/>
    <form  suffix="ей" tag="UNKI"/>
    <form  suffix="ею" tag="UNKI"/>
    <form  suffix="и" tag="UNKI"/>
    <form  suffix="й" tag="UNKI"/>
    <form  suffix="ю" tag="UNKI"/>
    <form  suffix="ям" tag="UNKI"/>
    <form  suffix="ями" tag="UNKI"/>
    <form  suffix="ях" tag="UNKI"/>
  </table>
  <table name="UNK33" rads=".*">
    <!-- 611 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="й" tag="UNKB"/>
    <form  suffix="йте" tag="UNKB"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="ю" tag="UNKM"/>
    <form  suffix="ют" tag="UNKM"/>
    <form  suffix="в" tag="UNKR"/>
  </table>
  <table name="UNK34" rads=".*">
    <!-- 603 members -->
    <form  suffix="ец" tag="UNK"/>
    <form  suffix="ца" tag="UNKO"/>
    <form  suffix="цам" tag="UNKO"/>
    <form  suffix="цами" tag="UNKO"/>
    <form  suffix="цах" tag="UNKO"/>
    <form  suffix="це" tag="UNKO"/>
    <form  suffix="цу" tag="UNKO"/>
    <form  suffix="цы" tag="UNKO"/>
  </table>
  <table name="UNK35" rads=".*">
    <!-- 597 members -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="ется" tag="UNKD"/>
    <form  suffix="ются" tag="UNKD"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="ясь" tag="UNKP"/>
  </table>
  <table name="UNK36" rads=".*">
    <!-- 570 members -->
    <form  suffix="овать" tag="UNK"/>
    <form  suffix="овал" tag="UNKL"/>
    <form  suffix="овала" tag="UNKL"/>
    <form  suffix="овали" tag="UNKL"/>
    <form  suffix="овало" tag="UNKL"/>
    <form  suffix="уя" tag="UNKQ"/>
    <form  suffix="уй" tag="UNKT"/>
    <form  suffix="уйте" tag="UNKT"/>
    <form  suffix="уем" tag="UNKU"/>
    <form  suffix="ует" tag="UNKU"/>
    <form  suffix="уете" tag="UNKU"/>
    <form  suffix="уешь" tag="UNKU"/>
    <form  suffix="ую" tag="UNKU"/>
    <form  suffix="уют" tag="UNKU"/>
  </table>
  <table name="UNK37" rads=".*">
    <!-- 533 members -->
    <form  suffix="ок" tag="UNK"/>
    <form  suffix="ка" tag="UNKO"/>
    <form  suffix="кам" tag="UNKO"/>
    <form  suffix="ками" tag="UNKO"/>
    <form  suffix="ках" tag="UNKO"/>
    <form  suffix="ке" tag="UNKO"/>
    <form  suffix="ки" tag="UNKO"/>
    <form  suffix="ков" tag="UNKO"/>
    <form  suffix="ком" tag="UNKO"/>
    <form  suffix="ку" tag="UNKO"/>
  </table>
  <table name="UNK38" rads=".*">
    <!-- 482 members -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="емся" tag="UNKM"/>
    <form  suffix="етесь" tag="UNKM"/>
    <form  suffix="ется" tag="UNKM"/>
    <form  suffix="ешься" tag="UNKM"/>
    <form  suffix="юсь" tag="UNKM"/>
    <form  suffix="ются" tag="UNKM"/>
    <form  suffix="ясь" tag="UNKP"/>
  </table>
  <table name="UNK39" rads=".*">
    <!-- 468 members -->
    <form  suffix="ь" tag="UNK"/>
    <form  suffix="ей" tag="UNKN"/>
    <form  suffix="и" tag="UNKN"/>
    <form  suffix="ью" tag="UNKN"/>
    <form  suffix="ям" tag="UNKN"/>
    <form  suffix="ями" tag="UNKN"/>
    <form  suffix="ях" tag="UNKN"/>
  </table>
  <table name="UNK40" rads=".*">
    <!-- 419 members -->
    <form  suffix="ы" tag="UNK"/>
    <form  suffix="ам" tag="UNKO"/>
    <form  suffix="ами" tag="UNKO"/>
    <form  suffix="ах" tag="UNKO"/>
  </table>
  <table name="UNK41" rads=".*[шжчщ]">
    <!-- 414 members -->
    <form  suffix="" tag="UNK"/>
    <form  suffix="а" tag="UNKK"/>
    <form  suffix="ам" tag="UNKK"/>
    <form  suffix="ами" tag="UNKK"/>
    <form  suffix="ах" tag="UNKK"/>
    <form  suffix="е" tag="UNKK"/>
    <form  suffix="ей" tag="UNKK"/>
    <form  suffix="и" tag="UNKK"/>
    <form  suffix="у" tag="UNKK"/>
  </table>
  <table name="UNK42" rads=".*">
    <!-- 412 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="и" tag="UNKB"/>
    <form  suffix="ите" tag="UNKB"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="ив" tag="UNKR"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="ю" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK43" rads=".*">
    <!-- 389 members -->
    <form  suffix="овать" tag="UNK"/>
    <form  suffix="овал" tag="UNKL"/>
    <form  suffix="овала" tag="UNKL"/>
    <form  suffix="овали" tag="UNKL"/>
    <form  suffix="овало" tag="UNKL"/>
    <form  suffix="овав" tag="UNKR"/>
    <form  suffix="уй" tag="UNKT"/>
    <form  suffix="уйте" tag="UNKT"/>
    <form  suffix="уем" tag="UNKU"/>
    <form  suffix="ует" tag="UNKU"/>
    <form  suffix="уете" tag="UNKU"/>
    <form  suffix="уешь" tag="UNKU"/>
    <form  suffix="ую" tag="UNKU"/>
    <form  suffix="уют" tag="UNKU"/>
  </table>
  <table name="UNK44" rads=".*">
    <!-- 376 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="й" tag="UNKB"/>
    <form  suffix="йте" tag="UNKB"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="я" tag="UNKP"/>
    <form  suffix="ем" tag="UNKU"/>
    <form  suffix="ет" tag="UNKU"/>
    <form  suffix="ете" tag="UNKU"/>
    <form  suffix="ешь" tag="UNKU"/>
    <form  suffix="ю" tag="UNKU"/>
    <form  suffix="ют" tag="UNKU"/>
  </table>
  <table name="UNK45" rads=".*">
    <!-- 371 members -->
    <form  suffix="ий" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="ие" tag="UNKA"/>
    <form  suffix="им" tag="UNKA"/>
    <form  suffix="ими" tag="UNKA"/>
    <form  suffix="их" tag="UNKA"/>
    <form  suffix="ого" tag="UNKA"/>
    <form  suffix="ое" tag="UNKA"/>
    <form  suffix="ой" tag="UNKA"/>
    <form  suffix="ом" tag="UNKA"/>
    <form  suffix="ому" tag="UNKA"/>
    <form  suffix="ою" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
    <form  suffix="и" tag="UNKZ"/>
  </table>
  <table name="UNK46" rads=".*">
    <!-- 357 members -->
    <form  suffix="а" tag="UNK"/>
    <form  suffix="" tag="UNKI"/>
    <form  suffix="ам" tag="UNKI"/>
    <form  suffix="ами" tag="UNKI"/>
    <form  suffix="ах" tag="UNKI"/>
    <form  suffix="е" tag="UNKI"/>
    <form  suffix="и" tag="UNKI"/>
    <form  suffix="ой" tag="UNKI"/>
    <form  suffix="ою" tag="UNKI"/>
    <form  suffix="у" tag="UNKI"/>
  </table>
  <table name="UNK47" rads=".*">
    <!-- 344 members -->
    <form  suffix="ный" tag="UNK"/>
    <form  suffix="ная" tag="UNKA"/>
    <form  suffix="ного" tag="UNKA"/>
    <form  suffix="ное" tag="UNKA"/>
    <form  suffix="ной" tag="UNKA"/>
    <form  suffix="ном" tag="UNKA"/>
    <form  suffix="ному" tag="UNKA"/>
    <form  suffix="ною" tag="UNKA"/>
    <form  suffix="ную" tag="UNKA"/>
    <form  suffix="ные" tag="UNKA"/>
    <form  suffix="ным" tag="UNKA"/>
    <form  suffix="ными" tag="UNKA"/>
    <form  suffix="ных" tag="UNKA"/>
    <form  suffix="нее" tag="UNKE"/>
    <form  suffix="ней" tag="UNKE"/>
    <form  suffix="ен" tag="UNKS"/>
    <form  suffix="на" tag="UNKS"/>
    <form  suffix="но" tag="UNKS"/>
    <form  suffix="ны" tag="UNKS"/>
  </table>
  <table name="UNK48" rads=".*">
    <!-- 337 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="ю" tag="UNKM"/>
    <form  suffix="ют" tag="UNKM"/>
    <form  suffix="в" tag="UNKR"/>
  </table>
  <table name="UNK49" rads=".*">
    <!-- 327 members -->
    <form  suffix="ьный" tag="UNK"/>
    <form  suffix="ьная" tag="UNKA"/>
    <form  suffix="ьного" tag="UNKA"/>
    <form  suffix="ьное" tag="UNKA"/>
    <form  suffix="ьной" tag="UNKA"/>
    <form  suffix="ьном" tag="UNKA"/>
    <form  suffix="ьному" tag="UNKA"/>
    <form  suffix="ьною" tag="UNKA"/>
    <form  suffix="ьную" tag="UNKA"/>
    <form  suffix="ьные" tag="UNKA"/>
    <form  suffix="ьным" tag="UNKA"/>
    <form  suffix="ьными" tag="UNKA"/>
    <form  suffix="ьных" tag="UNKA"/>
    <form  suffix="ен" tag="UNKS"/>
    <form  suffix="ьна" tag="UNKS"/>
    <form  suffix="ьно" tag="UNKS"/>
    <form  suffix="ьны" tag="UNKS"/>
  </table>
  <table name="UNK50" rads=".*">
    <!-- 319 members -->
    <form  suffix="" tag="UNK"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
  </table>
  <table name="UNK51" rads=".*">
    <!-- 313 members -->
    <form  suffix="е" tag="UNK"/>
    <form  suffix="ем" tag="UNKJ"/>
    <form  suffix="ю" tag="UNKJ"/>
    <form  suffix="я" tag="UNKJ"/>
  </table>
  <table name="UNK52" rads=".*">
    <!-- 307 members -->
    <form  suffix="е" tag="UNK"/>
    <form  suffix="м" tag="UNKA"/>
    <form  suffix="ми" tag="UNKA"/>
    <form  suffix="х" tag="UNKA"/>
  </table>
  <table name="UNK53" rads=".*">
    <!-- 296 members -->
    <form  suffix="овать" tag="UNK"/>
    <form  suffix="овал" tag="UNKL"/>
    <form  suffix="овала" tag="UNKL"/>
    <form  suffix="овали" tag="UNKL"/>
    <form  suffix="овало" tag="UNKL"/>
    <form  suffix="уя" tag="UNKQ"/>
    <form  suffix="уем" tag="UNKU"/>
    <form  suffix="ует" tag="UNKU"/>
    <form  suffix="уете" tag="UNKU"/>
    <form  suffix="уешь" tag="UNKU"/>
    <form  suffix="ую" tag="UNKU"/>
    <form  suffix="уют" tag="UNKU"/>
  </table>
  <table name="UNK54" rads=".*">
    <!-- 292 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="в" tag="UNKR"/>
  </table>
  <table name="UNK55" rads=".*">
    <!-- 288 members -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
  </table>
  <table name="UNK56" rads=".*">
    <!-- 281 members -->
    <form  suffix="ный" tag="UNK"/>
    <form  suffix="ная" tag="UNKA"/>
    <form  suffix="ного" tag="UNKA"/>
    <form  suffix="ное" tag="UNKA"/>
    <form  suffix="ной" tag="UNKA"/>
    <form  suffix="ном" tag="UNKA"/>
    <form  suffix="ному" tag="UNKA"/>
    <form  suffix="ною" tag="UNKA"/>
    <form  suffix="ную" tag="UNKA"/>
    <form  suffix="ные" tag="UNKA"/>
    <form  suffix="ным" tag="UNKA"/>
    <form  suffix="ными" tag="UNKA"/>
    <form  suffix="ных" tag="UNKA"/>
    <form  suffix="" tag="UNKS"/>
    <form  suffix="а" tag="UNKS"/>
    <form  suffix="о" tag="UNKS"/>
    <form  suffix="ы" tag="UNKS"/>
    <form  suffix="на" tag="UNKX"/>
    <form  suffix="но" tag="UNKX"/>
    <form  suffix="ны" tag="UNKX"/>
  </table>
  <table name="UNK57" rads=".*">
    <!-- 267 members -->
    <form  suffix="ый" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="ого" tag="UNKA"/>
    <form  suffix="ое" tag="UNKA"/>
    <form  suffix="ой" tag="UNKA"/>
    <form  suffix="ом" tag="UNKA"/>
    <form  suffix="ому" tag="UNKA"/>
    <form  suffix="ою" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
    <form  suffix="ые" tag="UNKA"/>
    <form  suffix="ым" tag="UNKA"/>
    <form  suffix="ыми" tag="UNKA"/>
    <form  suffix="ых" tag="UNKA"/>
    <form  suffix="а" tag="UNKX"/>
    <form  suffix="о" tag="UNKX"/>
    <form  suffix="ы" tag="UNKX"/>
  </table>
  <table name="UNK58" rads=".*">
    <!-- 241 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="и" tag="UNKB"/>
    <form  suffix="ите" tag="UNKB"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="ив" tag="UNKR"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="лю" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK59" rads=".*">
    <!-- 230 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="ив" tag="UNKR"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="ю" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK60" rads=".*">
    <!-- 224 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="и" tag="UNKB"/>
    <form  suffix="ите" tag="UNKB"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="ив" tag="UNKR"/>
    <form  suffix="ат" tag="UNKW"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="у" tag="UNKW"/>
  </table>
  <table name="UNK61" rads=".*">
    <!-- 223 members -->
    <form  suffix="уть" tag="UNK"/>
    <form  suffix="и" tag="UNKB"/>
    <form  suffix="ите" tag="UNKB"/>
    <form  suffix="ул" tag="UNKL"/>
    <form  suffix="ула" tag="UNKL"/>
    <form  suffix="ули" tag="UNKL"/>
    <form  suffix="уло" tag="UNKL"/>
    <form  suffix="ув" tag="UNKR"/>
    <form  suffix="ем" tag="UNKU"/>
    <form  suffix="ет" tag="UNKU"/>
    <form  suffix="ете" tag="UNKU"/>
    <form  suffix="ешь" tag="UNKU"/>
    <form  suffix="у" tag="UNKU"/>
    <form  suffix="ут" tag="UNKU"/>
    <form  suffix="ём" tag="UNKU"/>
    <form  suffix="ёт" tag="UNKU"/>
    <form  suffix="ёте" tag="UNKU"/>
    <form  suffix="ёшь" tag="UNKU"/>
  </table>
  <table name="UNK62" rads=".*">
    <!-- 217 members -->
    <form  suffix="оваться" tag="UNK"/>
    <form  suffix="овалась" tag="UNKL"/>
    <form  suffix="овались" tag="UNKL"/>
    <form  suffix="овалось" tag="UNKL"/>
    <form  suffix="овался" tag="UNKL"/>
    <form  suffix="уясь" tag="UNKQ"/>
  </table>
  <table name="UNK63" rads=".*">
    <!-- 213 members -->
    <form  suffix="й" tag="UNK"/>
    <form  suffix="ю" tag="UNKH"/>
  </table>
  <table name="UNK64" rads=".*">
    <!-- 200 members -->
    <form  suffix="а" tag="UNK"/>
    <form  suffix="" tag="UNKI"/>
    <form  suffix="ам" tag="UNKI"/>
    <form  suffix="ами" tag="UNKI"/>
    <form  suffix="ах" tag="UNKI"/>
    <form  suffix="е" tag="UNKI"/>
    <form  suffix="ей" tag="UNKI"/>
    <form  suffix="ею" tag="UNKI"/>
    <form  suffix="и" tag="UNKI"/>
    <form  suffix="у" tag="UNKI"/>
  </table>
  <table name="UNK65" rads=".*">
    <!-- 185 members -->
    <form  suffix="уть" tag="UNK"/>
    <form  suffix="и" tag="UNKB"/>
    <form  suffix="ите" tag="UNKB"/>
    <form  suffix="ул" tag="UNKL"/>
    <form  suffix="ула" tag="UNKL"/>
    <form  suffix="ули" tag="UNKL"/>
    <form  suffix="уло" tag="UNKL"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="у" tag="UNKM"/>
    <form  suffix="ут" tag="UNKM"/>
    <form  suffix="ув" tag="UNKR"/>
  </table>
  <table name="UNK66" rads=".*">
    <!-- 184 members -->
    <form  suffix="ий" tag="UNK"/>
    <form  suffix="его" tag="UNKA"/>
    <form  suffix="ее" tag="UNKA"/>
    <form  suffix="ей" tag="UNKA"/>
    <form  suffix="ем" tag="UNKA"/>
    <form  suffix="ему" tag="UNKA"/>
    <form  suffix="ею" tag="UNKA"/>
    <form  suffix="ие" tag="UNKA"/>
    <form  suffix="им" tag="UNKA"/>
    <form  suffix="ими" tag="UNKA"/>
    <form  suffix="их" tag="UNKA"/>
    <form  suffix="юю" tag="UNKA"/>
    <form  suffix="яя" tag="UNKA"/>
  </table>
  <table name="UNK67" rads=".*">
    <!-- 178 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="ится" tag="UNKD"/>
    <form  suffix="ятся" tag="UNKD"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ившись" tag="UNKR"/>
  </table>
  <table name="UNK68" rads=".*">
    <!-- 172 members -->
    <form  suffix="и" tag="UNK"/>
    <form  suffix="ам" tag="UNKO"/>
    <form  suffix="ами" tag="UNKO"/>
    <form  suffix="ах" tag="UNKO"/>
  </table>
  <table name="UNK69" rads=".*">
    <!-- 169 members -->
    <form  suffix="ий" tag="UNK"/>
    <form  suffix="ье" tag="UNKO"/>
    <form  suffix="ьего" tag="UNKO"/>
    <form  suffix="ьей" tag="UNKO"/>
    <form  suffix="ьем" tag="UNKO"/>
    <form  suffix="ьему" tag="UNKO"/>
    <form  suffix="ьи" tag="UNKO"/>
    <form  suffix="ьим" tag="UNKO"/>
    <form  suffix="ьими" tag="UNKO"/>
    <form  suffix="ьих" tag="UNKO"/>
    <form  suffix="ью" tag="UNKO"/>
    <form  suffix="ья" tag="UNKO"/>
  </table>
  <table name="UNK70" rads=".*">
    <!-- 169 members -->
    <form  suffix="йка" tag="UNK"/>
    <form  suffix="ек" tag="UNKI"/>
    <form  suffix="йкам" tag="UNKI"/>
    <form  suffix="йками" tag="UNKI"/>
    <form  suffix="йках" tag="UNKI"/>
    <form  suffix="йке" tag="UNKI"/>
    <form  suffix="йки" tag="UNKI"/>
    <form  suffix="йкой" tag="UNKI"/>
    <form  suffix="йкою" tag="UNKI"/>
    <form  suffix="йку" tag="UNKI"/>
  </table>
  <table name="UNK71" rads=".*">
    <!-- 169 members -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="йся" tag="UNKB"/>
    <form  suffix="йтесь" tag="UNKB"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="емся" tag="UNKM"/>
    <form  suffix="етесь" tag="UNKM"/>
    <form  suffix="ется" tag="UNKM"/>
    <form  suffix="ешься" tag="UNKM"/>
    <form  suffix="юсь" tag="UNKM"/>
    <form  suffix="ются" tag="UNKM"/>
    <form  suffix="вшись" tag="UNKR"/>
  </table>
  <table name="UNK72" rads=".*">
    <!-- 169 members -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="йся" tag="UNKB"/>
    <form  suffix="йтесь" tag="UNKB"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="ясь" tag="UNKP"/>
    <form  suffix="емся" tag="UNKU"/>
    <form  suffix="етесь" tag="UNKU"/>
    <form  suffix="ется" tag="UNKU"/>
    <form  suffix="ешься" tag="UNKU"/>
    <form  suffix="юсь" tag="UNKU"/>
    <form  suffix="ются" tag="UNKU"/>
  </table>
  <table name="UNK73" rads=".*">
    <!-- 166 members -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="вшись" tag="UNKR"/>
  </table>
  <table name="UNK74" rads=".*">
    <!-- 163 members -->
    <form  suffix="ый" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="ого" tag="UNKA"/>
    <form  suffix="ое" tag="UNKA"/>
    <form  suffix="ой" tag="UNKA"/>
    <form  suffix="ом" tag="UNKA"/>
    <form  suffix="ому" tag="UNKA"/>
    <form  suffix="ою" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
    <form  suffix="ые" tag="UNKA"/>
    <form  suffix="ым" tag="UNKA"/>
    <form  suffix="ыми" tag="UNKA"/>
    <form  suffix="ых" tag="UNKA"/>
    <form  suffix="ее" tag="UNKE"/>
    <form  suffix="ей" tag="UNKE"/>
    <form  suffix="" tag="UNKS"/>
    <form  suffix="а" tag="UNKS"/>
    <form  suffix="о" tag="UNKS"/>
    <form  suffix="ы" tag="UNKS"/>
  </table>
  <table name="UNK75" rads=".*">
    <!-- 162 members -->
    <form  suffix="" tag="UNK"/>
    <form  suffix="а" tag="UNKJ"/>
    <form  suffix="е" tag="UNKJ"/>
    <form  suffix="у" tag="UNKJ"/>
  </table>
  <table name="UNK76" rads=".*">
    <!-- 160 members -->
    <form  suffix="о" tag="UNK"/>
    <form  suffix="" tag="UNKK"/>
    <form  suffix="а" tag="UNKK"/>
    <form  suffix="ам" tag="UNKK"/>
    <form  suffix="ами" tag="UNKK"/>
    <form  suffix="ах" tag="UNKK"/>
    <form  suffix="е" tag="UNKK"/>
    <form  suffix="ом" tag="UNKK"/>
    <form  suffix="у" tag="UNKK"/>
  </table>
  <table name="UNK77" rads=".*">
    <!-- 157 members -->
    <form  suffix="ь" tag="UNK"/>
    <form  suffix="е" tag="UNKJ"/>
    <form  suffix="ем" tag="UNKJ"/>
    <form  suffix="ю" tag="UNKJ"/>
    <form  suffix="я" tag="UNKJ"/>
  </table>
  <table name="UNK78" rads=".*">
    <!-- 154 members -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="емся" tag="UNKM"/>
    <form  suffix="етесь" tag="UNKM"/>
    <form  suffix="ется" tag="UNKM"/>
    <form  suffix="ешься" tag="UNKM"/>
    <form  suffix="юсь" tag="UNKM"/>
    <form  suffix="ются" tag="UNKM"/>
    <form  suffix="вшись" tag="UNKR"/>
  </table>
  <table name="UNK79" rads=".*">
    <!-- 148 members -->
    <form  suffix="ек" tag="UNK"/>
    <form  suffix="ка" tag="UNKO"/>
    <form  suffix="кам" tag="UNKO"/>
    <form  suffix="ками" tag="UNKO"/>
    <form  suffix="ках" tag="UNKO"/>
    <form  suffix="ке" tag="UNKO"/>
    <form  suffix="ки" tag="UNKO"/>
    <form  suffix="ков" tag="UNKO"/>
    <form  suffix="ком" tag="UNKO"/>
    <form  suffix="ку" tag="UNKO"/>
  </table>
  <table name="UNK80" rads=".*">
    <!-- 147 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="ись" tag="UNKB"/>
    <form  suffix="итесь" tag="UNKB"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ившись" tag="UNKR"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="юсь" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK81" rads=".*">
    <!-- 146 members -->
    <form  suffix="ин" tag="UNK"/>
    <form  suffix="" tag="UNKO"/>
    <form  suffix="ам" tag="UNKO"/>
    <form  suffix="ами" tag="UNKO"/>
    <form  suffix="ах" tag="UNKO"/>
    <form  suffix="е" tag="UNKO"/>
    <form  suffix="ина" tag="UNKO"/>
    <form  suffix="ине" tag="UNKO"/>
    <form  suffix="ином" tag="UNKO"/>
    <form  suffix="ину" tag="UNKO"/>
  </table>
  <table name="UNK82" rads=".*">
    <!-- 145 members -->
    <form  suffix="е" tag="UNK"/>
    <form  suffix="й" tag="UNKE"/>
  </table>
  <table name="UNK83" rads=".*">
    <!-- 138 members -->
    <form  suffix="й" tag="UNK"/>
    <form  suffix="ем" tag="UNKJ"/>
    <form  suffix="и" tag="UNKJ"/>
    <form  suffix="ю" tag="UNKJ"/>
    <form  suffix="я" tag="UNKJ"/>
  </table>
  <table name="UNK84" rads=".*">
    <!-- 135 members -->
    <form  suffix="" tag="UNK"/>
    <form  suffix="м" tag="UNKO"/>
    <form  suffix="ми" tag="UNKO"/>
    <form  suffix="х" tag="UNKO"/>
  </table>
  <table name="UNK85" rads=".*">
    <!-- 135 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="ив" tag="UNKR"/>
    <form  suffix="ь" tag="UNKT"/>
    <form  suffix="ьте" tag="UNKT"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="ю" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK86" rads=".*">
    <!-- 133 members -->
    <form  suffix="ье" tag="UNK"/>
    <form  suffix="ий" tag="UNKK"/>
    <form  suffix="ьем" tag="UNKK"/>
    <form  suffix="ью" tag="UNKK"/>
    <form  suffix="ья" tag="UNKK"/>
    <form  suffix="ьям" tag="UNKK"/>
    <form  suffix="ьями" tag="UNKK"/>
    <form  suffix="ьях" tag="UNKK"/>
  </table>
  <table name="UNK87" rads=".*">
    <!-- 129 members -->
    <form  suffix="ьный" tag="UNK"/>
    <form  suffix="ьная" tag="UNKA"/>
    <form  suffix="ьного" tag="UNKA"/>
    <form  suffix="ьное" tag="UNKA"/>
    <form  suffix="ьной" tag="UNKA"/>
    <form  suffix="ьном" tag="UNKA"/>
    <form  suffix="ьному" tag="UNKA"/>
    <form  suffix="ьною" tag="UNKA"/>
    <form  suffix="ьную" tag="UNKA"/>
    <form  suffix="ьные" tag="UNKA"/>
    <form  suffix="ьным" tag="UNKA"/>
    <form  suffix="ьными" tag="UNKA"/>
    <form  suffix="ьных" tag="UNKA"/>
    <form  suffix="ьнее" tag="UNKE"/>
    <form  suffix="ьней" tag="UNKE"/>
    <form  suffix="ен" tag="UNKS"/>
    <form  suffix="ьна" tag="UNKS"/>
    <form  suffix="ьно" tag="UNKS"/>
    <form  suffix="ьны" tag="UNKS"/>
  </table>
  <table name="UNK88" rads=".*">
    <!-- 125 members -->
    <form  suffix="а" tag="UNK"/>
    <form  suffix="и" tag="UNKL"/>
    <form  suffix="о" tag="UNKL"/>
  </table>
  <table name="UNK89" rads=".*">
    <!-- 125 members -->
    <form  suffix="овать" tag="UNK"/>
    <form  suffix="овал" tag="UNKL"/>
    <form  suffix="овала" tag="UNKL"/>
    <form  suffix="овали" tag="UNKL"/>
    <form  suffix="овало" tag="UNKL"/>
    <form  suffix="овав" tag="UNKR"/>
    <form  suffix="уем" tag="UNKU"/>
    <form  suffix="ует" tag="UNKU"/>
    <form  suffix="уете" tag="UNKU"/>
    <form  suffix="уешь" tag="UNKU"/>
    <form  suffix="ую" tag="UNKU"/>
    <form  suffix="уют" tag="UNKU"/>
  </table>
  <table name="UNK90" rads=".*">
    <!-- 124 members -->
    <form  suffix="й" tag="UNK"/>
    <form  suffix="е" tag="UNKK"/>
    <form  suffix="ев" tag="UNKK"/>
    <form  suffix="ем" tag="UNKK"/>
    <form  suffix="и" tag="UNKK"/>
    <form  suffix="ю" tag="UNKK"/>
    <form  suffix="я" tag="UNKK"/>
    <form  suffix="ям" tag="UNKK"/>
    <form  suffix="ями" tag="UNKK"/>
    <form  suffix="ях" tag="UNKK"/>
  </table>
  <table name="UNK91" rads=".*">
    <!-- 122 members -->
    <form  suffix="я" tag="UNK"/>
    <form  suffix="е" tag="UNKI"/>
    <form  suffix="ей" tag="UNKI"/>
    <form  suffix="ею" tag="UNKI"/>
    <form  suffix="и" tag="UNKI"/>
    <form  suffix="ю" tag="UNKI"/>
    <form  suffix="ям" tag="UNKI"/>
    <form  suffix="ями" tag="UNKI"/>
    <form  suffix="ях" tag="UNKI"/>
  </table>
  <table name="UNK92" rads=".*">
    <!-- 118 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="я" tag="UNKP"/>
    <form  suffix="ем" tag="UNKU"/>
    <form  suffix="ет" tag="UNKU"/>
    <form  suffix="ете" tag="UNKU"/>
    <form  suffix="ешь" tag="UNKU"/>
    <form  suffix="ю" tag="UNKU"/>
    <form  suffix="ют" tag="UNKU"/>
  </table>
  <table name="UNK93" rads=".*">
    <!-- 117 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="и" tag="UNKB"/>
    <form  suffix="ите" tag="UNKB"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="ив" tag="UNKR"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK94" rads=".*">
    <!-- 114 members -->
    <form  suffix="сь" tag="UNK"/>
    <form  suffix="тесь" tag="UNKB"/>
  </table>
  <table name="UNK95" rads=".*">
    <!-- 113 members -->
    <form  suffix="а" tag="UNK"/>
    <form  suffix="е" tag="UNKH"/>
    <form  suffix="и" tag="UNKH"/>
    <form  suffix="у" tag="UNKH"/>
  </table>
  <table name="UNK96" rads=".*">
    <!-- 113 members -->
    <form  suffix="дить" tag="UNK"/>
    <form  suffix="ди" tag="UNKB"/>
    <form  suffix="дите" tag="UNKB"/>
    <form  suffix="дил" tag="UNKL"/>
    <form  suffix="дила" tag="UNKL"/>
    <form  suffix="дили" tag="UNKL"/>
    <form  suffix="дило" tag="UNKL"/>
    <form  suffix="див" tag="UNKR"/>
    <form  suffix="дим" tag="UNKW"/>
    <form  suffix="дит" tag="UNKW"/>
    <form  suffix="дите" tag="UNKW"/>
    <form  suffix="дишь" tag="UNKW"/>
    <form  suffix="дят" tag="UNKW"/>
    <form  suffix="жу" tag="UNKW"/>
  </table>
  <table name="UNK97" rads=".*">
    <!-- 113 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="ив" tag="UNKR"/>
    <form  suffix="ат" tag="UNKW"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="у" tag="UNKW"/>
  </table>
  <table name="UNK98" rads=".*">
    <!-- 113 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ившись" tag="UNKR"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="юсь" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK99" rads=".*">
    <!-- 112 members -->
    <form  suffix="ся" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
  </table>
  <table name="UNK100" rads=".*">
    <!-- 111 members -->
    <form  suffix="й" tag="UNK"/>
    <form  suffix="е" tag="UNKJ"/>
    <form  suffix="ем" tag="UNKJ"/>
    <form  suffix="ю" tag="UNKJ"/>
    <form  suffix="я" tag="UNKJ"/>
  </table>
  <table name="UNK101" rads=".*">
    <!-- 110 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="ится" tag="UNKD"/>
    <form  suffix="ятся" tag="UNKD"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
  </table>
  <table name="UNK102" rads=".*">
    <!-- 110 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
  </table>
  <table name="UNK103" rads=".*">
    <!-- 101 members -->
    <form  suffix="овать" tag="UNK"/>
    <form  suffix="овал" tag="UNKL"/>
    <form  suffix="овала" tag="UNKL"/>
    <form  suffix="овали" tag="UNKL"/>
    <form  suffix="овало" tag="UNKL"/>
    <form  suffix="уя" tag="UNKQ"/>
    <form  suffix="овав" tag="UNKR"/>
    <form  suffix="уй" tag="UNKT"/>
    <form  suffix="уйте" tag="UNKT"/>
    <form  suffix="уем" tag="UNKU"/>
    <form  suffix="ует" tag="UNKU"/>
    <form  suffix="уете" tag="UNKU"/>
    <form  suffix="уешь" tag="UNKU"/>
    <form  suffix="ую" tag="UNKU"/>
    <form  suffix="уют" tag="UNKU"/>
  </table>
  <table name="UNK104" rads=".*">
    <!-- 96 members -->
    <form  suffix="овать" tag="UNK"/>
    <form  suffix="овал" tag="UNKL"/>
    <form  suffix="овала" tag="UNKL"/>
    <form  suffix="овали" tag="UNKL"/>
    <form  suffix="овало" tag="UNKL"/>
    <form  suffix="уя" tag="UNKQ"/>
    <form  suffix="овав" tag="UNKR"/>
    <form  suffix="уем" tag="UNKU"/>
    <form  suffix="ует" tag="UNKU"/>
    <form  suffix="уете" tag="UNKU"/>
    <form  suffix="уешь" tag="UNKU"/>
    <form  suffix="ую" tag="UNKU"/>
    <form  suffix="уют" tag="UNKU"/>
  </table>
  <table name="UNK105" rads=".*">
    <!-- 95 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="ив" tag="UNKR"/>
    <form  suffix="ь" tag="UNKT"/>
    <form  suffix="ьте" tag="UNKT"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="лю" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK106" rads=".*">
    <!-- 91 members -->
    <form  suffix="й" tag="UNK"/>
    <form  suffix="ев" tag="UNKK"/>
    <form  suffix="ем" tag="UNKK"/>
    <form  suffix="и" tag="UNKK"/>
    <form  suffix="ю" tag="UNKK"/>
    <form  suffix="я" tag="UNKK"/>
    <form  suffix="ям" tag="UNKK"/>
    <form  suffix="ями" tag="UNKK"/>
    <form  suffix="ях" tag="UNKK"/>
  </table>
  <table name="UNK107" rads=".*">
    <!-- 90 members -->
    <form  suffix="ька" tag="UNK"/>
    <form  suffix="ек" tag="UNKI"/>
    <form  suffix="ькам" tag="UNKI"/>
    <form  suffix="ьками" tag="UNKI"/>
    <form  suffix="ьках" tag="UNKI"/>
    <form  suffix="ьке" tag="UNKI"/>
    <form  suffix="ьки" tag="UNKI"/>
    <form  suffix="ькой" tag="UNKI"/>
    <form  suffix="ькою" tag="UNKI"/>
    <form  suffix="ьку" tag="UNKI"/>
  </table>
  <table name="UNK108" rads=".*">
    <!-- 89 members -->
    <form  suffix="оваться" tag="UNK"/>
    <form  suffix="овалась" tag="UNKL"/>
    <form  suffix="овались" tag="UNKL"/>
    <form  suffix="овалось" tag="UNKL"/>
    <form  suffix="овался" tag="UNKL"/>
    <form  suffix="уясь" tag="UNKQ"/>
    <form  suffix="уйся" tag="UNKT"/>
    <form  suffix="уйтесь" tag="UNKT"/>
    <form  suffix="уемся" tag="UNKU"/>
    <form  suffix="уетесь" tag="UNKU"/>
    <form  suffix="уется" tag="UNKU"/>
    <form  suffix="уешься" tag="UNKU"/>
    <form  suffix="уюсь" tag="UNKU"/>
    <form  suffix="уются" tag="UNKU"/>
  </table>
  <table name="UNK109" rads=".*">
    <!-- 88 members -->
    <form  suffix="кий" tag="UNK"/>
    <form  suffix="кая" tag="UNKA"/>
    <form  suffix="кие" tag="UNKA"/>
    <form  suffix="ким" tag="UNKA"/>
    <form  suffix="кими" tag="UNKA"/>
    <form  suffix="ких" tag="UNKA"/>
    <form  suffix="кого" tag="UNKA"/>
    <form  suffix="кое" tag="UNKA"/>
    <form  suffix="кой" tag="UNKA"/>
    <form  suffix="ком" tag="UNKA"/>
    <form  suffix="кому" tag="UNKA"/>
    <form  suffix="кою" tag="UNKA"/>
    <form  suffix="кую" tag="UNKA"/>
    <form  suffix="ка" tag="UNKS"/>
    <form  suffix="ки" tag="UNKS"/>
    <form  suffix="ко" tag="UNKS"/>
    <form  suffix="ок" tag="UNKS"/>
  </table>
  <table name="UNK110" rads=".*">
    <!-- 88 members -->
    <form  suffix="я" tag="UNK"/>
    <form  suffix="е" tag="UNKH"/>
    <form  suffix="ей" tag="UNKH"/>
    <form  suffix="ею" tag="UNKH"/>
    <form  suffix="и" tag="UNKH"/>
    <form  suffix="ю" tag="UNKH"/>
  </table>
  <table name="UNK111" rads=".*">
    <!-- 87 members -->
    <form  suffix="а" tag="UNK"/>
    <form  suffix="е" tag="UNKH"/>
    <form  suffix="у" tag="UNKH"/>
    <form  suffix="ы" tag="UNKH"/>
  </table>
  <table name="UNK112" rads=".*">
    <!-- 87 members -->
    <form  suffix="е" tag="UNK"/>
    <form  suffix="" tag="UNKK"/>
    <form  suffix="а" tag="UNKK"/>
    <form  suffix="ам" tag="UNKK"/>
    <form  suffix="ами" tag="UNKK"/>
    <form  suffix="ах" tag="UNKK"/>
    <form  suffix="ем" tag="UNKK"/>
    <form  suffix="у" tag="UNKK"/>
  </table>
  <table name="UNK113" rads=".*">
    <!-- 87 members -->
    <form  suffix="и" tag="UNK"/>
    <form  suffix="ям" tag="UNKO"/>
    <form  suffix="ями" tag="UNKO"/>
    <form  suffix="ях" tag="UNKO"/>
  </table>
  <table name="UNK114" rads=".*">
    <!-- 87 members -->
    <form  suffix="ь" tag="UNK"/>
    <form  suffix="е" tag="UNKG"/>
    <form  suffix="ей" tag="UNKG"/>
    <form  suffix="ем" tag="UNKG"/>
    <form  suffix="и" tag="UNKG"/>
    <form  suffix="ю" tag="UNKG"/>
    <form  suffix="я" tag="UNKG"/>
    <form  suffix="ям" tag="UNKG"/>
    <form  suffix="ями" tag="UNKG"/>
    <form  suffix="ях" tag="UNKG"/>
    <form  suffix="ём" tag="UNKG"/>
  </table>
  <table name="UNK115" rads=".*">
    <!-- 86 members -->
    <form  suffix="уться" tag="UNK"/>
    <form  suffix="ись" tag="UNKB"/>
    <form  suffix="итесь" tag="UNKB"/>
    <form  suffix="улась" tag="UNKL"/>
    <form  suffix="улись" tag="UNKL"/>
    <form  suffix="улось" tag="UNKL"/>
    <form  suffix="улся" tag="UNKL"/>
    <form  suffix="увшись" tag="UNKR"/>
    <form  suffix="емся" tag="UNKU"/>
    <form  suffix="етесь" tag="UNKU"/>
    <form  suffix="ется" tag="UNKU"/>
    <form  suffix="ешься" tag="UNKU"/>
    <form  suffix="усь" tag="UNKU"/>
    <form  suffix="утся" tag="UNKU"/>
    <form  suffix="ёмся" tag="UNKU"/>
    <form  suffix="ётесь" tag="UNKU"/>
    <form  suffix="ётся" tag="UNKU"/>
    <form  suffix="ёшься" tag="UNKU"/>
  </table>
  <table name="UNK116" rads=".*">
    <!-- 81 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="ив" tag="UNKR"/>
    <form  suffix="ь" tag="UNKT"/>
    <form  suffix="ьте" tag="UNKT"/>
    <form  suffix="ат" tag="UNKW"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="у" tag="UNKW"/>
  </table>
  <table name="UNK117" rads=".*">
    <!-- 80 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="ись" tag="UNKB"/>
    <form  suffix="итесь" tag="UNKB"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ившись" tag="UNKR"/>
    <form  suffix="атся" tag="UNKW"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="усь" tag="UNKW"/>
  </table>
  <table name="UNK118" rads=".*">
    <!-- 79 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="я" tag="UNKP"/>
  </table>
  <table name="UNK119" rads=".*">
    <!-- 77 members -->
    <form  suffix="ий" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="его" tag="UNKA"/>
    <form  suffix="ее" tag="UNKA"/>
    <form  suffix="ей" tag="UNKA"/>
    <form  suffix="ем" tag="UNKA"/>
    <form  suffix="ему" tag="UNKA"/>
    <form  suffix="ею" tag="UNKA"/>
    <form  suffix="ие" tag="UNKA"/>
    <form  suffix="им" tag="UNKA"/>
    <form  suffix="ими" tag="UNKA"/>
    <form  suffix="их" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
    <form  suffix="е" tag="UNKZ"/>
  </table>
  <table name="UNK120" rads=".*">
    <!-- 75 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="и" tag="UNKB"/>
    <form  suffix="ите" tag="UNKB"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="я" tag="UNKP"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="ю" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK121" rads=".*">
    <!-- 75 members -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="ясь" tag="UNKP"/>
    <form  suffix="емся" tag="UNKU"/>
    <form  suffix="етесь" tag="UNKU"/>
    <form  suffix="ется" tag="UNKU"/>
    <form  suffix="ешься" tag="UNKU"/>
    <form  suffix="юсь" tag="UNKU"/>
    <form  suffix="ются" tag="UNKU"/>
  </table>
  <table name="UNK122" rads=".*">
    <!-- 75 members -->
    <form  suffix="ький" tag="UNK"/>
    <form  suffix="ькая" tag="UNKA"/>
    <form  suffix="ькие" tag="UNKA"/>
    <form  suffix="ьким" tag="UNKA"/>
    <form  suffix="ькими" tag="UNKA"/>
    <form  suffix="ьких" tag="UNKA"/>
    <form  suffix="ького" tag="UNKA"/>
    <form  suffix="ькое" tag="UNKA"/>
    <form  suffix="ькой" tag="UNKA"/>
    <form  suffix="ьком" tag="UNKA"/>
    <form  suffix="ькому" tag="UNKA"/>
    <form  suffix="ькою" tag="UNKA"/>
    <form  suffix="ькую" tag="UNKA"/>
    <form  suffix="ек" tag="UNKS"/>
    <form  suffix="ька" tag="UNKS"/>
    <form  suffix="ьки" tag="UNKS"/>
    <form  suffix="ько" tag="UNKS"/>
  </table>
  <table name="UNK123" rads=".*">
    <!-- 72 members -->
    <form  suffix="стить" tag="UNK"/>
    <form  suffix="сти" tag="UNKB"/>
    <form  suffix="стите" tag="UNKB"/>
    <form  suffix="стил" tag="UNKL"/>
    <form  suffix="стила" tag="UNKL"/>
    <form  suffix="стили" tag="UNKL"/>
    <form  suffix="стило" tag="UNKL"/>
    <form  suffix="стив" tag="UNKR"/>
    <form  suffix="стим" tag="UNKW"/>
    <form  suffix="стит" tag="UNKW"/>
    <form  suffix="стите" tag="UNKW"/>
    <form  suffix="стишь" tag="UNKW"/>
    <form  suffix="стят" tag="UNKW"/>
    <form  suffix="щу" tag="UNKW"/>
  </table>
  <table name="UNK124" rads=".*">
    <!-- 71 members -->
    <form  suffix="оваться" tag="UNK"/>
    <form  suffix="овалась" tag="UNKL"/>
    <form  suffix="овались" tag="UNKL"/>
    <form  suffix="овалось" tag="UNKL"/>
    <form  suffix="овался" tag="UNKL"/>
    <form  suffix="овавшись" tag="UNKR"/>
    <form  suffix="уйся" tag="UNKT"/>
    <form  suffix="уйтесь" tag="UNKT"/>
    <form  suffix="уемся" tag="UNKU"/>
    <form  suffix="уетесь" tag="UNKU"/>
    <form  suffix="уется" tag="UNKU"/>
    <form  suffix="уешься" tag="UNKU"/>
    <form  suffix="уюсь" tag="UNKU"/>
    <form  suffix="уются" tag="UNKU"/>
  </table>
  <table name="UNK125" rads=".*">
    <!-- 71 members -->
    <form  suffix="ённый" tag="UNK"/>
    <form  suffix="ённая" tag="UNKA"/>
    <form  suffix="ённого" tag="UNKA"/>
    <form  suffix="ённое" tag="UNKA"/>
    <form  suffix="ённой" tag="UNKA"/>
    <form  suffix="ённом" tag="UNKA"/>
    <form  suffix="ённому" tag="UNKA"/>
    <form  suffix="ённою" tag="UNKA"/>
    <form  suffix="ённую" tag="UNKA"/>
    <form  suffix="ённые" tag="UNKA"/>
    <form  suffix="ённым" tag="UNKA"/>
    <form  suffix="ёнными" tag="UNKA"/>
    <form  suffix="ённых" tag="UNKA"/>
    <form  suffix="ена" tag="UNKS"/>
    <form  suffix="ено" tag="UNKS"/>
    <form  suffix="ены" tag="UNKS"/>
    <form  suffix="ён" tag="UNKS"/>
    <form  suffix="ёна" tag="UNKS"/>
    <form  suffix="ёно" tag="UNKS"/>
    <form  suffix="ёны" tag="UNKS"/>
    <form  suffix="ённа" tag="UNKX"/>
    <form  suffix="ённо" tag="UNKX"/>
    <form  suffix="ённы" tag="UNKX"/>
  </table>
  <table name="UNK126" rads=".*">
    <!-- 70 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ившись" tag="UNKR"/>
    <form  suffix="атся" tag="UNKW"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="усь" tag="UNKW"/>
  </table>
  <table name="UNK127" rads=".*">
    <!-- 69 members -->
    <form  suffix="енок" tag="UNK"/>
    <form  suffix="енка" tag="UNKF"/>
    <form  suffix="енке" tag="UNKF"/>
    <form  suffix="енком" tag="UNKF"/>
    <form  suffix="енку" tag="UNKF"/>
    <form  suffix="ят" tag="UNKF"/>
    <form  suffix="ята" tag="UNKF"/>
    <form  suffix="ятам" tag="UNKF"/>
    <form  suffix="ятами" tag="UNKF"/>
    <form  suffix="ятах" tag="UNKF"/>
  </table>
  <table name="UNK128" rads=".*">
    <!-- 69 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="ись" tag="UNKB"/>
    <form  suffix="итесь" tag="UNKB"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ившись" tag="UNKR"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="люсь" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK129" rads=".*">
    <!-- 69 members -->
    <form  suffix="ёнок" tag="UNK"/>
    <form  suffix="ят" tag="UNKF"/>
    <form  suffix="ята" tag="UNKF"/>
    <form  suffix="ятам" tag="UNKF"/>
    <form  suffix="ятами" tag="UNKF"/>
    <form  suffix="ятах" tag="UNKF"/>
    <form  suffix="ёнка" tag="UNKF"/>
    <form  suffix="ёнке" tag="UNKF"/>
    <form  suffix="ёнком" tag="UNKF"/>
    <form  suffix="ёнку" tag="UNKF"/>
  </table>
  <table name="UNK130" rads=".*">
    <!-- 68 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="ив" tag="UNKR"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="лю" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK131" rads=".*">
    <!-- 68 members -->
    <form  suffix="ый" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="ого" tag="UNKA"/>
    <form  suffix="ое" tag="UNKA"/>
    <form  suffix="ой" tag="UNKA"/>
    <form  suffix="ом" tag="UNKA"/>
    <form  suffix="ому" tag="UNKA"/>
    <form  suffix="ою" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
    <form  suffix="ые" tag="UNKA"/>
    <form  suffix="ым" tag="UNKA"/>
    <form  suffix="ыми" tag="UNKA"/>
    <form  suffix="ых" tag="UNKA"/>
    <form  suffix="ее" tag="UNKE"/>
    <form  suffix="ей" tag="UNKE"/>
    <form  suffix="а" tag="UNKX"/>
    <form  suffix="о" tag="UNKX"/>
    <form  suffix="ы" tag="UNKX"/>
  </table>
  <table name="UNK132" rads=".*">
    <!-- 67 members -->
    <form  suffix="" tag="UNK"/>
    <form  suffix="а" tag="UNKA"/>
    <form  suffix="о" tag="UNKA"/>
    <form  suffix="ого" tag="UNKA"/>
    <form  suffix="ой" tag="UNKA"/>
    <form  suffix="ом" tag="UNKA"/>
    <form  suffix="ому" tag="UNKA"/>
    <form  suffix="у" tag="UNKA"/>
    <form  suffix="ы" tag="UNKA"/>
    <form  suffix="ым" tag="UNKA"/>
    <form  suffix="ыми" tag="UNKA"/>
    <form  suffix="ых" tag="UNKA"/>
  </table>
  <table name="UNK133" rads=".*">
    <!-- 65 members -->
    <form  suffix="о" tag="UNK"/>
    <form  suffix="а" tag="UNKK"/>
    <form  suffix="ам" tag="UNKK"/>
    <form  suffix="ами" tag="UNKK"/>
    <form  suffix="ах" tag="UNKK"/>
    <form  suffix="е" tag="UNKK"/>
    <form  suffix="ом" tag="UNKK"/>
    <form  suffix="у" tag="UNKK"/>
  </table>
  <table name="UNK134" rads=".*">
    <!-- 64 members -->
    <form  suffix="" tag="UNK"/>
    <form  suffix="а" tag="UNKN"/>
    <form  suffix="ам" tag="UNKN"/>
    <form  suffix="ами" tag="UNKN"/>
    <form  suffix="ах" tag="UNKN"/>
    <form  suffix="е" tag="UNKN"/>
    <form  suffix="ов" tag="UNKN"/>
    <form  suffix="ом" tag="UNKN"/>
    <form  suffix="у" tag="UNKN"/>
  </table>
  <table name="UNK135" rads=".*">
    <!-- 64 members -->
    <form  suffix="уть" tag="UNK"/>
    <form  suffix="и" tag="UNKB"/>
    <form  suffix="ите" tag="UNKB"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="у" tag="UNKM"/>
    <form  suffix="ут" tag="UNKM"/>
    <form  suffix="ув" tag="UNKR"/>
  </table>
  <table name="UNK136" rads=".*">
    <!-- 60 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="ится" tag="UNKD"/>
    <form  suffix="ятся" tag="UNKD"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ясь" tag="UNKP"/>
  </table>
  <table name="UNK137" rads=".*">
    <!-- 58 members -->
    <form  suffix="ец" tag="UNK"/>
    <form  suffix="йца" tag="UNKO"/>
    <form  suffix="йцам" tag="UNKO"/>
    <form  suffix="йцами" tag="UNKO"/>
    <form  suffix="йцах" tag="UNKO"/>
    <form  suffix="йце" tag="UNKO"/>
    <form  suffix="йцев" tag="UNKO"/>
    <form  suffix="йцем" tag="UNKO"/>
    <form  suffix="йцу" tag="UNKO"/>
    <form  suffix="йцы" tag="UNKO"/>
  </table>
  <table name="UNK138" rads=".*">
    <!-- 58 members -->
    <form  suffix="уть" tag="UNK"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="у" tag="UNKM"/>
    <form  suffix="ут" tag="UNKM"/>
    <form  suffix="ув" tag="UNKR"/>
  </table>
  <table name="UNK139" rads=".*">
    <!-- 57 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="я" tag="UNKP"/>
    <form  suffix="ь" tag="UNKT"/>
    <form  suffix="ьте" tag="UNKT"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="ю" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK140" rads=".*">
    <!-- 57 members -->
    <form  suffix="о" tag="UNK"/>
    <form  suffix="а" tag="UNKK"/>
    <form  suffix="ам" tag="UNKK"/>
    <form  suffix="ами" tag="UNKK"/>
    <form  suffix="ах" tag="UNKK"/>
    <form  suffix="е" tag="UNKK"/>
    <form  suffix="и" tag="UNKK"/>
    <form  suffix="ом" tag="UNKK"/>
    <form  suffix="у" tag="UNKK"/>
  </table>
  <table name="UNK141" rads=".*">
    <!-- 57 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="й" tag="UNKB"/>
    <form  suffix="йте" tag="UNKB"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="в" tag="UNKR"/>
    <form  suffix="ем" tag="UNKU"/>
    <form  suffix="ет" tag="UNKU"/>
    <form  suffix="ете" tag="UNKU"/>
    <form  suffix="ешь" tag="UNKU"/>
    <form  suffix="ю" tag="UNKU"/>
    <form  suffix="ют" tag="UNKU"/>
  </table>
  <table name="UNK142" rads=".*">
    <!-- 57 members -->
    <form  suffix="ё" tag="UNK"/>
    <form  suffix="е" tag="UNKJ"/>
    <form  suffix="ю" tag="UNKJ"/>
    <form  suffix="я" tag="UNKJ"/>
    <form  suffix="ёа" tag="UNKJ"/>
    <form  suffix="ёе" tag="UNKJ"/>
    <form  suffix="ём" tag="UNKJ"/>
    <form  suffix="ёом" tag="UNKJ"/>
    <form  suffix="ёу" tag="UNKJ"/>
  </table>
  <table name="UNK143" rads=".*">
    <!-- 56 members -->
    <form  suffix="зать" tag="UNK"/>
    <form  suffix="зал" tag="UNKL"/>
    <form  suffix="зала" tag="UNKL"/>
    <form  suffix="зали" tag="UNKL"/>
    <form  suffix="зало" tag="UNKL"/>
    <form  suffix="зав" tag="UNKR"/>
    <form  suffix="жем" tag="UNKY"/>
    <form  suffix="жет" tag="UNKY"/>
    <form  suffix="жете" tag="UNKY"/>
    <form  suffix="жешь" tag="UNKY"/>
    <form  suffix="жу" tag="UNKY"/>
    <form  suffix="жут" tag="UNKY"/>
  </table>
  <table name="UNK144" rads=".*">
    <!-- 54 members -->
    <form  suffix="дить" tag="UNK"/>
    <form  suffix="ди" tag="UNKB"/>
    <form  suffix="дите" tag="UNKB"/>
    <form  suffix="дил" tag="UNKL"/>
    <form  suffix="дила" tag="UNKL"/>
    <form  suffix="дили" tag="UNKL"/>
    <form  suffix="дило" tag="UNKL"/>
    <form  suffix="дя" tag="UNKP"/>
    <form  suffix="дим" tag="UNKW"/>
    <form  suffix="дит" tag="UNKW"/>
    <form  suffix="дите" tag="UNKW"/>
    <form  suffix="дишь" tag="UNKW"/>
    <form  suffix="дят" tag="UNKW"/>
    <form  suffix="жу" tag="UNKW"/>
  </table>
  <table name="UNK145" rads=".*">
    <!-- 54 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="я" tag="UNKP"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="ю" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK146" rads=".*">
    <!-- 54 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ившись" tag="UNKR"/>
    <form  suffix="ься" tag="UNKT"/>
    <form  suffix="ьтесь" tag="UNKT"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="юсь" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK147" rads=".*">
    <!-- 53 members -->
    <form  suffix="ась" tag="UNK"/>
    <form  suffix="ись" tag="UNKL"/>
    <form  suffix="ось" tag="UNKL"/>
  </table>
  <table name="UNK148" rads=".*">
    <!-- 53 members -->
    <form  suffix="я" tag="UNK"/>
    <form  suffix="е" tag="UNKE"/>
    <form  suffix="ей" tag="UNKE"/>
    <form  suffix="ею" tag="UNKE"/>
    <form  suffix="и" tag="UNKE"/>
    <form  suffix="ю" tag="UNKE"/>
    <form  suffix="ёй" tag="UNKE"/>
    <form  suffix="ёю" tag="UNKE"/>
  </table>
  <table name="UNK149" rads=".*">
    <!-- 52 members -->
    <form  suffix="тать" tag="UNK"/>
    <form  suffix="тал" tag="UNKL"/>
    <form  suffix="тала" tag="UNKL"/>
    <form  suffix="тали" tag="UNKL"/>
    <form  suffix="тало" tag="UNKL"/>
    <form  suffix="тав" tag="UNKR"/>
    <form  suffix="чем" tag="UNKY"/>
    <form  suffix="чет" tag="UNKY"/>
    <form  suffix="чете" tag="UNKY"/>
    <form  suffix="чешь" tag="UNKY"/>
    <form  suffix="чу" tag="UNKY"/>
    <form  suffix="чут" tag="UNKY"/>
  </table>
  <table name="UNK150" rads=".*">
    <!-- 52 members -->
    <form  suffix="уть" tag="UNK"/>
    <form  suffix="ул" tag="UNKL"/>
    <form  suffix="ула" tag="UNKL"/>
    <form  suffix="ули" tag="UNKL"/>
    <form  suffix="уло" tag="UNKL"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="у" tag="UNKM"/>
    <form  suffix="ут" tag="UNKM"/>
    <form  suffix="ув" tag="UNKR"/>
    <form  suffix="ь" tag="UNKT"/>
    <form  suffix="ьте" tag="UNKT"/>
  </table>
  <table name="UNK151" rads=".*">
    <!-- 51 members -->
    <form  suffix="сить" tag="UNK"/>
    <form  suffix="си" tag="UNKB"/>
    <form  suffix="сите" tag="UNKB"/>
    <form  suffix="сил" tag="UNKL"/>
    <form  suffix="сила" tag="UNKL"/>
    <form  suffix="сили" tag="UNKL"/>
    <form  suffix="сило" tag="UNKL"/>
    <form  suffix="сив" tag="UNKR"/>
    <form  suffix="сим" tag="UNKW"/>
    <form  suffix="сит" tag="UNKW"/>
    <form  suffix="сите" tag="UNKW"/>
    <form  suffix="сишь" tag="UNKW"/>
    <form  suffix="сят" tag="UNKW"/>
    <form  suffix="шу" tag="UNKW"/>
  </table>
  <table name="UNK152" rads=".*">
    <!-- 50 members -->
    <form  suffix="ий" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="его" tag="UNKA"/>
    <form  suffix="ее" tag="UNKA"/>
    <form  suffix="ей" tag="UNKA"/>
    <form  suffix="ем" tag="UNKA"/>
    <form  suffix="ему" tag="UNKA"/>
    <form  suffix="ею" tag="UNKA"/>
    <form  suffix="ие" tag="UNKA"/>
    <form  suffix="им" tag="UNKA"/>
    <form  suffix="ими" tag="UNKA"/>
    <form  suffix="их" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
    <form  suffix="" tag="UNKS"/>
    <form  suffix="а" tag="UNKS"/>
    <form  suffix="е" tag="UNKS"/>
    <form  suffix="и" tag="UNKS"/>
  </table>
  <table name="UNK153" rads=".*">
    <!-- 50 members -->
    <form  suffix="ой" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="ие" tag="UNKA"/>
    <form  suffix="им" tag="UNKA"/>
    <form  suffix="ими" tag="UNKA"/>
    <form  suffix="их" tag="UNKA"/>
    <form  suffix="ого" tag="UNKA"/>
    <form  suffix="ое" tag="UNKA"/>
    <form  suffix="ом" tag="UNKA"/>
    <form  suffix="ому" tag="UNKA"/>
    <form  suffix="ою" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
  </table>
  <table name="UNK154" rads=".*">
    <!-- 50 members -->
    <form  suffix="ок" tag="UNK"/>
    <form  suffix="ка" tag="UNKG"/>
    <form  suffix="ке" tag="UNKG"/>
    <form  suffix="ком" tag="UNKG"/>
    <form  suffix="ку" tag="UNKG"/>
  </table>
  <table name="UNK155" rads=".*">
    <!-- 49 members -->
    <form  suffix="ень" tag="UNK"/>
    <form  suffix="не" tag="UNKO"/>
    <form  suffix="ней" tag="UNKO"/>
    <form  suffix="нем" tag="UNKO"/>
    <form  suffix="ни" tag="UNKO"/>
    <form  suffix="ню" tag="UNKO"/>
    <form  suffix="ня" tag="UNKO"/>
    <form  suffix="ням" tag="UNKO"/>
    <form  suffix="нями" tag="UNKO"/>
    <form  suffix="нях" tag="UNKO"/>
  </table>
  <table name="UNK156" rads=".*">
    <!-- 49 members -->
    <form  suffix="ся" tag="UNK"/>
    <form  suffix="тесь" tag="UNKB"/>
  </table>
  <table name="UNK157" rads=".*">
    <!-- 48 members -->
    <form  suffix="диться" tag="UNK"/>
    <form  suffix="дись" tag="UNKB"/>
    <form  suffix="дитесь" tag="UNKB"/>
    <form  suffix="дилась" tag="UNKL"/>
    <form  suffix="дились" tag="UNKL"/>
    <form  suffix="дилось" tag="UNKL"/>
    <form  suffix="дился" tag="UNKL"/>
    <form  suffix="дившись" tag="UNKR"/>
    <form  suffix="димся" tag="UNKW"/>
    <form  suffix="дитесь" tag="UNKW"/>
    <form  suffix="дится" tag="UNKW"/>
    <form  suffix="дишься" tag="UNKW"/>
    <form  suffix="дятся" tag="UNKW"/>
    <form  suffix="жусь" tag="UNKW"/>
  </table>
  <table name="UNK158" rads=".*">
    <!-- 48 members -->
    <form  suffix="оваться" tag="UNK"/>
    <form  suffix="овалась" tag="UNKL"/>
    <form  suffix="овались" tag="UNKL"/>
    <form  suffix="овалось" tag="UNKL"/>
    <form  suffix="овался" tag="UNKL"/>
    <form  suffix="овавшись" tag="UNKR"/>
    <form  suffix="уемся" tag="UNKU"/>
    <form  suffix="уетесь" tag="UNKU"/>
    <form  suffix="уется" tag="UNKU"/>
    <form  suffix="уешься" tag="UNKU"/>
    <form  suffix="уюсь" tag="UNKU"/>
    <form  suffix="уются" tag="UNKU"/>
  </table>
  <table name="UNK159" rads=".*">
    <!-- 47 members -->
    <form  suffix="ец" tag="UNK"/>
    <form  suffix="ьца" tag="UNKO"/>
    <form  suffix="ьцам" tag="UNKO"/>
    <form  suffix="ьцами" tag="UNKO"/>
    <form  suffix="ьцах" tag="UNKO"/>
    <form  suffix="ьце" tag="UNKO"/>
    <form  suffix="ьцу" tag="UNKO"/>
    <form  suffix="ьцы" tag="UNKO"/>
  </table>
  <table name="UNK160" rads=".*">
    <!-- 47 members -->
    <form  suffix="ный" tag="UNK"/>
    <form  suffix="ная" tag="UNKA"/>
    <form  suffix="ного" tag="UNKA"/>
    <form  suffix="ное" tag="UNKA"/>
    <form  suffix="ной" tag="UNKA"/>
    <form  suffix="ном" tag="UNKA"/>
    <form  suffix="ному" tag="UNKA"/>
    <form  suffix="ною" tag="UNKA"/>
    <form  suffix="ную" tag="UNKA"/>
    <form  suffix="ные" tag="UNKA"/>
    <form  suffix="ным" tag="UNKA"/>
    <form  suffix="ными" tag="UNKA"/>
    <form  suffix="ных" tag="UNKA"/>
    <form  suffix="нее" tag="UNKE"/>
    <form  suffix="ней" tag="UNKE"/>
    <form  suffix="" tag="UNKS"/>
    <form  suffix="а" tag="UNKS"/>
    <form  suffix="о" tag="UNKS"/>
    <form  suffix="ы" tag="UNKS"/>
    <form  suffix="на" tag="UNKX"/>
    <form  suffix="но" tag="UNKX"/>
    <form  suffix="ны" tag="UNKX"/>
  </table>
  <table name="UNK161" rads=".*">
    <!-- 47 members -->
    <form  suffix="ня" tag="UNK"/>
    <form  suffix="ен" tag="UNKI"/>
    <form  suffix="не" tag="UNKI"/>
    <form  suffix="ней" tag="UNKI"/>
    <form  suffix="нею" tag="UNKI"/>
    <form  suffix="ни" tag="UNKI"/>
    <form  suffix="ню" tag="UNKI"/>
    <form  suffix="ням" tag="UNKI"/>
    <form  suffix="нями" tag="UNKI"/>
    <form  suffix="нях" tag="UNKI"/>
  </table>
  <table name="UNK162" rads=".*">
    <!-- 46 members -->
    <form  suffix="" tag="UNK"/>
    <form  suffix="а" tag="UNKA"/>
    <form  suffix="о" tag="UNKA"/>
    <form  suffix="ой" tag="UNKA"/>
    <form  suffix="ом" tag="UNKA"/>
    <form  suffix="у" tag="UNKA"/>
    <form  suffix="ы" tag="UNKA"/>
    <form  suffix="ым" tag="UNKA"/>
    <form  suffix="ыми" tag="UNKA"/>
    <form  suffix="ых" tag="UNKA"/>
  </table>
  <table name="UNK163" rads=".*">
    <!-- 46 members -->
    <form  suffix="сить" tag="UNK"/>
    <form  suffix="сил" tag="UNKL"/>
    <form  suffix="сила" tag="UNKL"/>
    <form  suffix="сили" tag="UNKL"/>
    <form  suffix="сило" tag="UNKL"/>
    <form  suffix="сив" tag="UNKR"/>
    <form  suffix="сь" tag="UNKT"/>
    <form  suffix="сьте" tag="UNKT"/>
    <form  suffix="сим" tag="UNKW"/>
    <form  suffix="сит" tag="UNKW"/>
    <form  suffix="сите" tag="UNKW"/>
    <form  suffix="сишь" tag="UNKW"/>
    <form  suffix="сят" tag="UNKW"/>
    <form  suffix="шу" tag="UNKW"/>
  </table>
  <table name="UNK164" rads=".*">
    <!-- 45 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="ись" tag="UNKB"/>
    <form  suffix="итесь" tag="UNKB"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ившись" tag="UNKR"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK165" rads=".*">
    <!-- 44 members -->
    <form  suffix="" tag="UNK"/>
    <form  suffix="а" tag="UNKK"/>
    <form  suffix="ам" tag="UNKK"/>
    <form  suffix="ами" tag="UNKK"/>
    <form  suffix="ах" tag="UNKK"/>
    <form  suffix="е" tag="UNKK"/>
    <form  suffix="у" tag="UNKK"/>
    <form  suffix="ы" tag="UNKK"/>
  </table>
  <table name="UNK166" rads=".*">
    <!-- 44 members -->
    <form  suffix="ать" tag="UNK"/>
    <form  suffix="ал" tag="UNKL"/>
    <form  suffix="ала" tag="UNKL"/>
    <form  suffix="али" tag="UNKL"/>
    <form  suffix="ало" tag="UNKL"/>
    <form  suffix="ав" tag="UNKR"/>
    <form  suffix="ат" tag="UNKU"/>
    <form  suffix="им" tag="UNKU"/>
    <form  suffix="ит" tag="UNKU"/>
    <form  suffix="ите" tag="UNKU"/>
    <form  suffix="ишь" tag="UNKU"/>
    <form  suffix="у" tag="UNKU"/>
  </table>
  <table name="UNK167" rads=".*">
    <!-- 44 members -->
    <form  suffix="дить" tag="UNK"/>
    <form  suffix="дил" tag="UNKL"/>
    <form  suffix="дила" tag="UNKL"/>
    <form  suffix="дили" tag="UNKL"/>
    <form  suffix="дило" tag="UNKL"/>
    <form  suffix="див" tag="UNKR"/>
    <form  suffix="дим" tag="UNKW"/>
    <form  suffix="дит" tag="UNKW"/>
    <form  suffix="дите" tag="UNKW"/>
    <form  suffix="дишь" tag="UNKW"/>
    <form  suffix="дят" tag="UNKW"/>
    <form  suffix="жу" tag="UNKW"/>
  </table>
  <table name="UNK168" rads=".*">
    <!-- 44 members -->
    <form  suffix="ек" tag="UNK"/>
    <form  suffix="ка" tag="UNKG"/>
    <form  suffix="ке" tag="UNKG"/>
    <form  suffix="ком" tag="UNKG"/>
    <form  suffix="ку" tag="UNKG"/>
  </table>
  <table name="UNK169" rads=".*">
    <!-- 44 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ившись" tag="UNKR"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="люсь" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK170" rads=".*">
    <!-- 43 members -->
    <form  suffix="уться" tag="UNK"/>
    <form  suffix="ись" tag="UNKB"/>
    <form  suffix="итесь" tag="UNKB"/>
    <form  suffix="улась" tag="UNKL"/>
    <form  suffix="улись" tag="UNKL"/>
    <form  suffix="улось" tag="UNKL"/>
    <form  suffix="улся" tag="UNKL"/>
    <form  suffix="емся" tag="UNKM"/>
    <form  suffix="етесь" tag="UNKM"/>
    <form  suffix="ется" tag="UNKM"/>
    <form  suffix="ешься" tag="UNKM"/>
    <form  suffix="усь" tag="UNKM"/>
    <form  suffix="утся" tag="UNKM"/>
    <form  suffix="увшись" tag="UNKR"/>
  </table>
  <table name="UNK171" rads=".*">
    <!-- 42 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ившись" tag="UNKR"/>
    <form  suffix="ься" tag="UNKT"/>
    <form  suffix="ьтесь" tag="UNKT"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="люсь" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK172" rads=".*">
    <!-- 42 members -->
    <form  suffix="ный" tag="UNK"/>
    <form  suffix="ная" tag="UNKA"/>
    <form  suffix="ного" tag="UNKA"/>
    <form  suffix="ное" tag="UNKA"/>
    <form  suffix="ной" tag="UNKA"/>
    <form  suffix="ном" tag="UNKA"/>
    <form  suffix="ному" tag="UNKA"/>
    <form  suffix="ною" tag="UNKA"/>
    <form  suffix="ную" tag="UNKA"/>
    <form  suffix="ные" tag="UNKA"/>
    <form  suffix="ным" tag="UNKA"/>
    <form  suffix="ными" tag="UNKA"/>
    <form  suffix="ных" tag="UNKA"/>
    <form  suffix="" tag="UNKS"/>
    <form  suffix="а" tag="UNKS"/>
    <form  suffix="о" tag="UNKS"/>
    <form  suffix="ы" tag="UNKS"/>
    <form  suffix="но" tag="UNKZ"/>
  </table>
  <table name="UNK173" rads=".*">
    <!-- 41 members -->
    <form  suffix="ек" tag="UNK"/>
    <form  suffix="ька" tag="UNKO"/>
    <form  suffix="ькам" tag="UNKO"/>
    <form  suffix="ьками" tag="UNKO"/>
    <form  suffix="ьках" tag="UNKO"/>
    <form  suffix="ьке" tag="UNKO"/>
    <form  suffix="ьки" tag="UNKO"/>
    <form  suffix="ьков" tag="UNKO"/>
    <form  suffix="ьком" tag="UNKO"/>
    <form  suffix="ьку" tag="UNKO"/>
  </table>
  <table name="UNK174" rads=".*">
    <!-- 41 members -->
    <form  suffix="ечь" tag="UNK"/>
    <form  suffix="ек" tag="UNKL"/>
    <form  suffix="екла" tag="UNKL"/>
    <form  suffix="екли" tag="UNKL"/>
    <form  suffix="екло" tag="UNKL"/>
    <form  suffix="ёк" tag="UNKL"/>
    <form  suffix="еку" tag="UNKM"/>
    <form  suffix="екут" tag="UNKM"/>
    <form  suffix="ечем" tag="UNKM"/>
    <form  suffix="ечет" tag="UNKM"/>
    <form  suffix="ечете" tag="UNKM"/>
    <form  suffix="ечешь" tag="UNKM"/>
    <form  suffix="ечём" tag="UNKM"/>
    <form  suffix="ечёт" tag="UNKM"/>
    <form  suffix="ечёте" tag="UNKM"/>
    <form  suffix="ечёшь" tag="UNKM"/>
  </table>
  <table name="UNK175" rads=".*">
    <!-- 41 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="ив" tag="UNKR"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK176" rads=".*">
    <!-- 41 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="атся" tag="UNKD"/>
    <form  suffix="ится" tag="UNKD"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ившись" tag="UNKR"/>
  </table>
  <table name="UNK177" rads=".*">
    <!-- 41 members -->
    <form  suffix="ти" tag="UNK"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="у" tag="UNKM"/>
    <form  suffix="ут" tag="UNKM"/>
    <form  suffix="ём" tag="UNKM"/>
    <form  suffix="ёт" tag="UNKM"/>
    <form  suffix="ёте" tag="UNKM"/>
    <form  suffix="ёшь" tag="UNKM"/>
  </table>
  <table name="UNK178" rads=".*">
    <!-- 41 members -->
    <form  suffix="ёк" tag="UNK"/>
    <form  suffix="ька" tag="UNKO"/>
    <form  suffix="ькам" tag="UNKO"/>
    <form  suffix="ьками" tag="UNKO"/>
    <form  suffix="ьках" tag="UNKO"/>
    <form  suffix="ьке" tag="UNKO"/>
    <form  suffix="ьки" tag="UNKO"/>
    <form  suffix="ьков" tag="UNKO"/>
    <form  suffix="ьком" tag="UNKO"/>
    <form  suffix="ьку" tag="UNKO"/>
  </table>
  <table name="UNK179" rads=".*">
    <!-- 40 members -->
    <form  suffix="ать" tag="UNK"/>
    <form  suffix="ал" tag="UNKL"/>
    <form  suffix="ала" tag="UNKL"/>
    <form  suffix="али" tag="UNKL"/>
    <form  suffix="ало" tag="UNKL"/>
    <form  suffix="ав" tag="UNKR"/>
    <form  suffix="и" tag="UNKT"/>
    <form  suffix="ат" tag="UNKU"/>
    <form  suffix="им" tag="UNKU"/>
    <form  suffix="ит" tag="UNKU"/>
    <form  suffix="ите" tag="UNKU"/>
    <form  suffix="ишь" tag="UNKU"/>
    <form  suffix="у" tag="UNKU"/>
  </table>
  <table name="UNK180" rads=".*">
    <!-- 40 members -->
    <form  suffix="уть" tag="UNK"/>
    <form  suffix="ул" tag="UNKL"/>
    <form  suffix="ула" tag="UNKL"/>
    <form  suffix="ули" tag="UNKL"/>
    <form  suffix="уло" tag="UNKL"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="у" tag="UNKM"/>
    <form  suffix="ут" tag="UNKM"/>
    <form  suffix="ув" tag="UNKR"/>
  </table>
  <table name="UNK181" rads=".*">
    <!-- 40 members -->
    <form  suffix="уть" tag="UNK"/>
    <form  suffix="ул" tag="UNKL"/>
    <form  suffix="ула" tag="UNKL"/>
    <form  suffix="ули" tag="UNKL"/>
    <form  suffix="уло" tag="UNKL"/>
    <form  suffix="ув" tag="UNKR"/>
    <form  suffix="ем" tag="UNKU"/>
    <form  suffix="ет" tag="UNKU"/>
    <form  suffix="ете" tag="UNKU"/>
    <form  suffix="ешь" tag="UNKU"/>
    <form  suffix="у" tag="UNKU"/>
    <form  suffix="ут" tag="UNKU"/>
    <form  suffix="ём" tag="UNKU"/>
    <form  suffix="ёт" tag="UNKU"/>
    <form  suffix="ёте" tag="UNKU"/>
    <form  suffix="ёшь" tag="UNKU"/>
  </table>
  <table name="UNK182" rads=".*">
    <!-- 40 members -->
    <form  suffix="ыть" tag="UNK"/>
    <form  suffix="ой" tag="UNKB"/>
    <form  suffix="ойте" tag="UNKB"/>
    <form  suffix="ыл" tag="UNKL"/>
    <form  suffix="ыла" tag="UNKL"/>
    <form  suffix="ыли" tag="UNKL"/>
    <form  suffix="ыло" tag="UNKL"/>
    <form  suffix="оем" tag="UNKM"/>
    <form  suffix="оет" tag="UNKM"/>
    <form  suffix="оете" tag="UNKM"/>
    <form  suffix="оешь" tag="UNKM"/>
    <form  suffix="ою" tag="UNKM"/>
    <form  suffix="оют" tag="UNKM"/>
    <form  suffix="ыв" tag="UNKR"/>
  </table>
  <table name="UNK183" rads=".*">
    <!-- 39 members -->
    <form  suffix="" tag="UNK"/>
    <form  suffix="а" tag="UNKG"/>
    <form  suffix="е" tag="UNKG"/>
    <form  suffix="у" tag="UNKG"/>
    <form  suffix="ым" tag="UNKG"/>
  </table>
  <table name="UNK184" rads=".*">
    <!-- 39 members -->
    <form  suffix="е" tag="UNK"/>
    <form  suffix="а" tag="UNKK"/>
    <form  suffix="ам" tag="UNKK"/>
    <form  suffix="ами" tag="UNKK"/>
    <form  suffix="ах" tag="UNKK"/>
    <form  suffix="ем" tag="UNKK"/>
    <form  suffix="у" tag="UNKK"/>
  </table>
  <table name="UNK185" rads=".*">
    <!-- 39 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="ив" tag="UNKR"/>
    <form  suffix="й" tag="UNKT"/>
    <form  suffix="йте" tag="UNKT"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="ю" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK186" rads=".*">
    <!-- 38 members -->
    <form  suffix="я" tag="UNK"/>
    <form  suffix="е" tag="UNKI"/>
    <form  suffix="ей" tag="UNKI"/>
    <form  suffix="ею" tag="UNKI"/>
    <form  suffix="и" tag="UNKI"/>
    <form  suffix="ь" tag="UNKI"/>
    <form  suffix="ю" tag="UNKI"/>
    <form  suffix="ям" tag="UNKI"/>
    <form  suffix="ями" tag="UNKI"/>
    <form  suffix="ях" tag="UNKI"/>
  </table>
  <table name="UNK187" rads=".*">
    <!-- 37 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="и" tag="UNKB"/>
    <form  suffix="ите" tag="UNKB"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="а" tag="UNKP"/>
    <form  suffix="ат" tag="UNKW"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="у" tag="UNKW"/>
  </table>
  <table name="UNK188" rads=".*">
    <!-- 37 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="и" tag="UNKB"/>
    <form  suffix="ите" tag="UNKB"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="я" tag="UNKP"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="лю" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK189" rads=".*">
    <!-- 36 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ившись" tag="UNKR"/>
    <form  suffix="ься" tag="UNKT"/>
    <form  suffix="ьтесь" tag="UNKT"/>
    <form  suffix="атся" tag="UNKW"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="усь" tag="UNKW"/>
  </table>
  <table name="UNK190" rads=".*">
    <!-- 35 members -->
    <form  suffix="еть" tag="UNK"/>
    <form  suffix="ел" tag="UNKL"/>
    <form  suffix="ела" tag="UNKL"/>
    <form  suffix="ели" tag="UNKL"/>
    <form  suffix="ело" tag="UNKL"/>
    <form  suffix="ев" tag="UNKR"/>
    <form  suffix="и" tag="UNKT"/>
    <form  suffix="ите" tag="UNKT"/>
    <form  suffix="им" tag="UNKY"/>
    <form  suffix="ит" tag="UNKY"/>
    <form  suffix="ите" tag="UNKY"/>
    <form  suffix="ишь" tag="UNKY"/>
    <form  suffix="ят" tag="UNKY"/>
  </table>
  <table name="UNK191" rads=".*">
    <!-- 35 members -->
    <form  suffix="ий" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="ие" tag="UNKA"/>
    <form  suffix="им" tag="UNKA"/>
    <form  suffix="ими" tag="UNKA"/>
    <form  suffix="их" tag="UNKA"/>
    <form  suffix="ого" tag="UNKA"/>
    <form  suffix="ое" tag="UNKA"/>
    <form  suffix="ой" tag="UNKA"/>
    <form  suffix="ом" tag="UNKA"/>
    <form  suffix="ому" tag="UNKA"/>
    <form  suffix="ою" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
    <form  suffix="" tag="UNKS"/>
    <form  suffix="а" tag="UNKS"/>
    <form  suffix="и" tag="UNKS"/>
    <form  suffix="о" tag="UNKS"/>
  </table>
  <table name="UNK192" rads=".*">
    <!-- 34 members -->
    <form  suffix="ать" tag="UNK"/>
    <form  suffix="ал" tag="UNKL"/>
    <form  suffix="ала" tag="UNKL"/>
    <form  suffix="али" tag="UNKL"/>
    <form  suffix="ало" tag="UNKL"/>
    <form  suffix="ав" tag="UNKR"/>
    <form  suffix="ем" tag="UNKY"/>
    <form  suffix="ет" tag="UNKY"/>
    <form  suffix="ете" tag="UNKY"/>
    <form  suffix="ешь" tag="UNKY"/>
    <form  suffix="у" tag="UNKY"/>
    <form  suffix="ут" tag="UNKY"/>
    <form  suffix="ём" tag="UNKY"/>
    <form  suffix="ёт" tag="UNKY"/>
    <form  suffix="ёте" tag="UNKY"/>
    <form  suffix="ёшь" tag="UNKY"/>
  </table>
  <table name="UNK193" rads=".*">
    <!-- 34 members -->
    <form  suffix="вать" tag="UNK"/>
    <form  suffix="вай" tag="UNKB"/>
    <form  suffix="вайте" tag="UNKB"/>
    <form  suffix="вал" tag="UNKL"/>
    <form  suffix="вала" tag="UNKL"/>
    <form  suffix="вали" tag="UNKL"/>
    <form  suffix="вало" tag="UNKL"/>
    <form  suffix="вая" tag="UNKP"/>
    <form  suffix="ем" tag="UNKU"/>
    <form  suffix="ет" tag="UNKU"/>
    <form  suffix="ете" tag="UNKU"/>
    <form  suffix="ешь" tag="UNKU"/>
    <form  suffix="ю" tag="UNKU"/>
    <form  suffix="ют" tag="UNKU"/>
    <form  suffix="ём" tag="UNKU"/>
    <form  suffix="ёт" tag="UNKU"/>
    <form  suffix="ёте" tag="UNKU"/>
    <form  suffix="ёшь" tag="UNKU"/>
  </table>
  <table name="UNK194" rads=".*">
    <!-- 34 members -->
    <form  suffix="ьня" tag="UNK"/>
    <form  suffix="ен" tag="UNKI"/>
    <form  suffix="ьне" tag="UNKI"/>
    <form  suffix="ьней" tag="UNKI"/>
    <form  suffix="ьнею" tag="UNKI"/>
    <form  suffix="ьни" tag="UNKI"/>
    <form  suffix="ьню" tag="UNKI"/>
    <form  suffix="ьням" tag="UNKI"/>
    <form  suffix="ьнями" tag="UNKI"/>
    <form  suffix="ьнях" tag="UNKI"/>
  </table>
  <table name="UNK195" rads=".*">
    <!-- 33 members -->
    <form  suffix="зить" tag="UNK"/>
    <form  suffix="зи" tag="UNKB"/>
    <form  suffix="зите" tag="UNKB"/>
    <form  suffix="зил" tag="UNKL"/>
    <form  suffix="зила" tag="UNKL"/>
    <form  suffix="зили" tag="UNKL"/>
    <form  suffix="зило" tag="UNKL"/>
    <form  suffix="зив" tag="UNKR"/>
    <form  suffix="жу" tag="UNKW"/>
    <form  suffix="зим" tag="UNKW"/>
    <form  suffix="зит" tag="UNKW"/>
    <form  suffix="зите" tag="UNKW"/>
    <form  suffix="зишь" tag="UNKW"/>
    <form  suffix="зят" tag="UNKW"/>
  </table>
  <table name="UNK196" rads=".*">
    <!-- 32 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="ись" tag="UNKB"/>
    <form  suffix="итесь" tag="UNKB"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ясь" tag="UNKP"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="юсь" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK197" rads=".*">
    <!-- 32 members -->
    <form  suffix="оваться" tag="UNK"/>
    <form  suffix="овалась" tag="UNKL"/>
    <form  suffix="овались" tag="UNKL"/>
    <form  suffix="овалось" tag="UNKL"/>
    <form  suffix="овался" tag="UNKL"/>
    <form  suffix="уясь" tag="UNKQ"/>
    <form  suffix="уемся" tag="UNKU"/>
    <form  suffix="уетесь" tag="UNKU"/>
    <form  suffix="уется" tag="UNKU"/>
    <form  suffix="уешься" tag="UNKU"/>
    <form  suffix="уюсь" tag="UNKU"/>
    <form  suffix="уются" tag="UNKU"/>
  </table>
  <table name="UNK198" rads=".*">
    <!-- 32 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="в" tag="UNKR"/>
    <form  suffix="ем" tag="UNKU"/>
    <form  suffix="ет" tag="UNKU"/>
    <form  suffix="ете" tag="UNKU"/>
    <form  suffix="ешь" tag="UNKU"/>
    <form  suffix="ю" tag="UNKU"/>
    <form  suffix="ют" tag="UNKU"/>
  </table>
  <table name="UNK199" rads=".*">
    <!-- 32 members -->
    <form  suffix="я" tag="UNK"/>
    <form  suffix="е" tag="UNKI"/>
    <form  suffix="ей" tag="UNKI"/>
    <form  suffix="ею" tag="UNKI"/>
    <form  suffix="и" tag="UNKI"/>
    <form  suffix="й" tag="UNKI"/>
    <form  suffix="ю" tag="UNKI"/>
    <form  suffix="ям" tag="UNKI"/>
    <form  suffix="ями" tag="UNKI"/>
    <form  suffix="ях" tag="UNKI"/>
  </table>
  <table name="UNK200" rads=".*">
    <!-- 31 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="а" tag="UNKP"/>
    <form  suffix="ь" tag="UNKT"/>
    <form  suffix="ьте" tag="UNKT"/>
    <form  suffix="ат" tag="UNKW"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="у" tag="UNKW"/>
  </table>
  <table name="UNK201" rads=".*">
    <!-- 30 members -->
    <form  suffix="е" tag="UNK"/>
    <form  suffix="а" tag="UNKJ"/>
    <form  suffix="ем" tag="UNKJ"/>
    <form  suffix="у" tag="UNKJ"/>
  </table>
  <table name="UNK202" rads=".*">
    <!-- 30 members -->
    <form  suffix="заться" tag="UNK"/>
    <form  suffix="залась" tag="UNKL"/>
    <form  suffix="зались" tag="UNKL"/>
    <form  suffix="залось" tag="UNKL"/>
    <form  suffix="зался" tag="UNKL"/>
    <form  suffix="завшись" tag="UNKR"/>
    <form  suffix="жемся" tag="UNKY"/>
    <form  suffix="жетесь" tag="UNKY"/>
    <form  suffix="жется" tag="UNKY"/>
    <form  suffix="жешься" tag="UNKY"/>
    <form  suffix="жусь" tag="UNKY"/>
    <form  suffix="жутся" tag="UNKY"/>
  </table>
  <table name="UNK203" rads=".*">
    <!-- 30 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="в" tag="UNKR"/>
  </table>
  <table name="UNK204" rads=".*">
    <!-- 30 members -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="ется" tag="UNKD"/>
    <form  suffix="ются" tag="UNKD"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="вшись" tag="UNKR"/>
  </table>
  <table name="UNK205" rads=".*" fast="-">
    <!-- 29 members -->
    <form  suffix="диться" tag="UNK"/>
    <form  suffix="дилась" tag="UNKL"/>
    <form  suffix="дились" tag="UNKL"/>
    <form  suffix="дилось" tag="UNKL"/>
    <form  suffix="дился" tag="UNKL"/>
    <form  suffix="дившись" tag="UNKR"/>
    <form  suffix="димся" tag="UNKW"/>
    <form  suffix="дитесь" tag="UNKW"/>
    <form  suffix="дится" tag="UNKW"/>
    <form  suffix="дишься" tag="UNKW"/>
    <form  suffix="дятся" tag="UNKW"/>
    <form  suffix="жусь" tag="UNKW"/>
  </table>
  <table name="UNK206" rads=".*" fast="-">
    <!-- 29 members -->
    <form  suffix="сить" tag="UNK"/>
    <form  suffix="си" tag="UNKB"/>
    <form  suffix="сите" tag="UNKB"/>
    <form  suffix="сил" tag="UNKL"/>
    <form  suffix="сила" tag="UNKL"/>
    <form  suffix="сили" tag="UNKL"/>
    <form  suffix="сило" tag="UNKL"/>
    <form  suffix="ся" tag="UNKP"/>
    <form  suffix="сим" tag="UNKW"/>
    <form  suffix="сит" tag="UNKW"/>
    <form  suffix="сите" tag="UNKW"/>
    <form  suffix="сишь" tag="UNKW"/>
    <form  suffix="сят" tag="UNKW"/>
    <form  suffix="шу" tag="UNKW"/>
  </table>
  <table name="UNK207" rads=".*" fast="-">
    <!-- 29 members -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="йся" tag="UNKB"/>
    <form  suffix="йтесь" tag="UNKB"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="вшись" tag="UNKR"/>
    <form  suffix="емся" tag="UNKU"/>
    <form  suffix="етесь" tag="UNKU"/>
    <form  suffix="ется" tag="UNKU"/>
    <form  suffix="ешься" tag="UNKU"/>
    <form  suffix="юсь" tag="UNKU"/>
    <form  suffix="ются" tag="UNKU"/>
  </table>
  <table name="UNK208" rads=".*" fast="-">
    <!-- 28 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ившись" tag="UNKR"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK209" rads=".*" fast="-">
    <!-- 28 members -->
    <form  suffix="ой" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="ого" tag="UNKA"/>
    <form  suffix="ое" tag="UNKA"/>
    <form  suffix="ом" tag="UNKA"/>
    <form  suffix="ому" tag="UNKA"/>
    <form  suffix="ою" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
    <form  suffix="ые" tag="UNKA"/>
    <form  suffix="ым" tag="UNKA"/>
    <form  suffix="ыми" tag="UNKA"/>
    <form  suffix="ых" tag="UNKA"/>
    <form  suffix="" tag="UNKS"/>
    <form  suffix="а" tag="UNKS"/>
    <form  suffix="о" tag="UNKS"/>
    <form  suffix="ы" tag="UNKS"/>
  </table>
  <table name="UNK210" rads=".*" fast="-">
    <!-- 28 members -->
    <form  suffix="чь" tag="UNK"/>
    <form  suffix="гу" tag="UNKU"/>
    <form  suffix="гут" tag="UNKU"/>
    <form  suffix="жем" tag="UNKU"/>
    <form  suffix="жет" tag="UNKU"/>
    <form  suffix="жете" tag="UNKU"/>
    <form  suffix="жешь" tag="UNKU"/>
    <form  suffix="жём" tag="UNKU"/>
    <form  suffix="жёт" tag="UNKU"/>
    <form  suffix="жёте" tag="UNKU"/>
    <form  suffix="жёшь" tag="UNKU"/>
  </table>
  <table name="UNK211" rads=".*" fast="-">
    <!-- 27 members -->
    <form  suffix="йный" tag="UNK"/>
    <form  suffix="йная" tag="UNKA"/>
    <form  suffix="йного" tag="UNKA"/>
    <form  suffix="йное" tag="UNKA"/>
    <form  suffix="йной" tag="UNKA"/>
    <form  suffix="йном" tag="UNKA"/>
    <form  suffix="йному" tag="UNKA"/>
    <form  suffix="йною" tag="UNKA"/>
    <form  suffix="йную" tag="UNKA"/>
    <form  suffix="йные" tag="UNKA"/>
    <form  suffix="йным" tag="UNKA"/>
    <form  suffix="йными" tag="UNKA"/>
    <form  suffix="йных" tag="UNKA"/>
    <form  suffix="ен" tag="UNKS"/>
    <form  suffix="йна" tag="UNKS"/>
    <form  suffix="йно" tag="UNKS"/>
    <form  suffix="йны" tag="UNKS"/>
  </table>
  <table name="UNK212" rads=".*" fast="-">
    <!-- 27 members -->
    <form  suffix="онок" tag="UNK"/>
    <form  suffix="ат" tag="UNKF"/>
    <form  suffix="ата" tag="UNKF"/>
    <form  suffix="атам" tag="UNKF"/>
    <form  suffix="атами" tag="UNKF"/>
    <form  suffix="атах" tag="UNKF"/>
    <form  suffix="онка" tag="UNKF"/>
    <form  suffix="онке" tag="UNKF"/>
    <form  suffix="онком" tag="UNKF"/>
    <form  suffix="онку" tag="UNKF"/>
  </table>
  <table name="UNK213" rads=".*" fast="-">
    <!-- 27 members -->
    <form  suffix="стить" tag="UNK"/>
    <form  suffix="стил" tag="UNKL"/>
    <form  suffix="стила" tag="UNKL"/>
    <form  suffix="стили" tag="UNKL"/>
    <form  suffix="стило" tag="UNKL"/>
    <form  suffix="стив" tag="UNKR"/>
    <form  suffix="стим" tag="UNKW"/>
    <form  suffix="стит" tag="UNKW"/>
    <form  suffix="стите" tag="UNKW"/>
    <form  suffix="стишь" tag="UNKW"/>
    <form  suffix="стят" tag="UNKW"/>
    <form  suffix="щу" tag="UNKW"/>
  </table>
  <table name="UNK214" rads=".*" fast="-">
    <!-- 27 members -->
    <form  suffix="уться" tag="UNK"/>
    <form  suffix="улась" tag="UNKL"/>
    <form  suffix="улись" tag="UNKL"/>
    <form  suffix="улось" tag="UNKL"/>
    <form  suffix="улся" tag="UNKL"/>
    <form  suffix="емся" tag="UNKM"/>
    <form  suffix="етесь" tag="UNKM"/>
    <form  suffix="ется" tag="UNKM"/>
    <form  suffix="ешься" tag="UNKM"/>
    <form  suffix="усь" tag="UNKM"/>
    <form  suffix="утся" tag="UNKM"/>
    <form  suffix="увшись" tag="UNKR"/>
    <form  suffix="ься" tag="UNKT"/>
    <form  suffix="ьтесь" tag="UNKT"/>
  </table>
  <table name="UNK215" rads=".*" fast="-">
    <!-- 26 members -->
    <form  suffix="а" tag="UNK"/>
    <form  suffix="" tag="UNKI"/>
    <form  suffix="ам" tag="UNKI"/>
    <form  suffix="ами" tag="UNKI"/>
    <form  suffix="ах" tag="UNKI"/>
    <form  suffix="е" tag="UNKI"/>
    <form  suffix="и" tag="UNKI"/>
    <form  suffix="у" tag="UNKI"/>
  </table>
  <table name="UNK216" rads=".*" fast="-">
    <!-- 26 members -->
    <form  suffix="зить" tag="UNK"/>
    <form  suffix="зил" tag="UNKL"/>
    <form  suffix="зила" tag="UNKL"/>
    <form  suffix="зили" tag="UNKL"/>
    <form  suffix="зило" tag="UNKL"/>
    <form  suffix="зив" tag="UNKR"/>
    <form  suffix="жу" tag="UNKW"/>
    <form  suffix="зим" tag="UNKW"/>
    <form  suffix="зит" tag="UNKW"/>
    <form  suffix="зите" tag="UNKW"/>
    <form  suffix="зишь" tag="UNKW"/>
    <form  suffix="зят" tag="UNKW"/>
  </table>
  <table name="UNK217" rads=".*" fast="-">
    <!-- 26 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="ив" tag="UNKR"/>
    <form  suffix="ь" tag="UNKT"/>
    <form  suffix="ьте" tag="UNKT"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK218" rads=".*" fast="-">
    <!-- 26 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ясь" tag="UNKP"/>
    <form  suffix="ься" tag="UNKT"/>
    <form  suffix="ьтесь" tag="UNKT"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="юсь" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK219" rads=".*" fast="-">
    <!-- 26 members -->
    <form  suffix="тить" tag="UNK"/>
    <form  suffix="ти" tag="UNKB"/>
    <form  suffix="тите" tag="UNKB"/>
    <form  suffix="тил" tag="UNKL"/>
    <form  suffix="тила" tag="UNKL"/>
    <form  suffix="тили" tag="UNKL"/>
    <form  suffix="тило" tag="UNKL"/>
    <form  suffix="тив" tag="UNKR"/>
    <form  suffix="тим" tag="UNKW"/>
    <form  suffix="тит" tag="UNKW"/>
    <form  suffix="тите" tag="UNKW"/>
    <form  suffix="тишь" tag="UNKW"/>
    <form  suffix="тят" tag="UNKW"/>
    <form  suffix="чу" tag="UNKW"/>
  </table>
  <table name="UNK220" rads=".*" fast="-">
    <!-- 25 members -->
    <form  suffix="дить" tag="UNK"/>
    <form  suffix="дил" tag="UNKL"/>
    <form  suffix="дила" tag="UNKL"/>
    <form  suffix="дили" tag="UNKL"/>
    <form  suffix="дило" tag="UNKL"/>
    <form  suffix="див" tag="UNKR"/>
    <form  suffix="дь" tag="UNKT"/>
    <form  suffix="дьте" tag="UNKT"/>
    <form  suffix="дим" tag="UNKW"/>
    <form  suffix="дит" tag="UNKW"/>
    <form  suffix="дите" tag="UNKW"/>
    <form  suffix="дишь" tag="UNKW"/>
    <form  suffix="дят" tag="UNKW"/>
    <form  suffix="жу" tag="UNKW"/>
  </table>
  <table name="UNK221" rads=".*" fast="-">
    <!-- 25 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="ив" tag="UNKR"/>
    <form  suffix="ьем" tag="UNKV"/>
    <form  suffix="ьет" tag="UNKV"/>
    <form  suffix="ьете" tag="UNKV"/>
    <form  suffix="ьешь" tag="UNKV"/>
    <form  suffix="ью" tag="UNKV"/>
    <form  suffix="ьют" tag="UNKV"/>
    <form  suffix="ьём" tag="UNKV"/>
    <form  suffix="ьёт" tag="UNKV"/>
    <form  suffix="ьёте" tag="UNKV"/>
    <form  suffix="ьёшь" tag="UNKV"/>
  </table>
  <table name="UNK222" rads=".*" fast="-">
    <!-- 25 members -->
    <form  suffix="сить" tag="UNK"/>
    <form  suffix="сил" tag="UNKL"/>
    <form  suffix="сила" tag="UNKL"/>
    <form  suffix="сили" tag="UNKL"/>
    <form  suffix="сило" tag="UNKL"/>
    <form  suffix="сив" tag="UNKR"/>
    <form  suffix="сим" tag="UNKW"/>
    <form  suffix="сит" tag="UNKW"/>
    <form  suffix="сите" tag="UNKW"/>
    <form  suffix="сишь" tag="UNKW"/>
    <form  suffix="сят" tag="UNKW"/>
    <form  suffix="шу" tag="UNKW"/>
  </table>
  <table name="UNK223" rads=".*" fast="-">
    <!-- 24 members -->
    <form  suffix="аться" tag="UNK"/>
    <form  suffix="алась" tag="UNKL"/>
    <form  suffix="ались" tag="UNKL"/>
    <form  suffix="алось" tag="UNKL"/>
    <form  suffix="ался" tag="UNKL"/>
    <form  suffix="авшись" tag="UNKR"/>
    <form  suffix="емся" tag="UNKY"/>
    <form  suffix="етесь" tag="UNKY"/>
    <form  suffix="ется" tag="UNKY"/>
    <form  suffix="ешься" tag="UNKY"/>
    <form  suffix="усь" tag="UNKY"/>
    <form  suffix="утся" tag="UNKY"/>
    <form  suffix="ёмся" tag="UNKY"/>
    <form  suffix="ётесь" tag="UNKY"/>
    <form  suffix="ётся" tag="UNKY"/>
    <form  suffix="ёшься" tag="UNKY"/>
  </table>
  <table name="UNK224" rads=".*" fast="-">
    <!-- 24 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="ись" tag="UNKB"/>
    <form  suffix="итесь" tag="UNKB"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ась" tag="UNKP"/>
    <form  suffix="атся" tag="UNKW"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="усь" tag="UNKW"/>
  </table>
  <table name="UNK225" rads=".*" fast="-">
    <!-- 24 members -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="емся" tag="UNKM"/>
    <form  suffix="етесь" tag="UNKM"/>
    <form  suffix="ется" tag="UNKM"/>
    <form  suffix="ешься" tag="UNKM"/>
    <form  suffix="юсь" tag="UNKM"/>
    <form  suffix="ются" tag="UNKM"/>
  </table>
  <table name="UNK226" rads=".*" fast="-">
    <!-- 23 members -->
    <form  suffix="ститься" tag="UNK"/>
    <form  suffix="стись" tag="UNKB"/>
    <form  suffix="ститесь" tag="UNKB"/>
    <form  suffix="стилась" tag="UNKL"/>
    <form  suffix="стились" tag="UNKL"/>
    <form  suffix="стилось" tag="UNKL"/>
    <form  suffix="стился" tag="UNKL"/>
    <form  suffix="стившись" tag="UNKR"/>
    <form  suffix="стимся" tag="UNKW"/>
    <form  suffix="ститесь" tag="UNKW"/>
    <form  suffix="стится" tag="UNKW"/>
    <form  suffix="стишься" tag="UNKW"/>
    <form  suffix="стятся" tag="UNKW"/>
    <form  suffix="щусь" tag="UNKW"/>
  </table>
  <table name="UNK227" rads=".*" fast="-">
    <!-- 23 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="й" tag="UNKB"/>
    <form  suffix="йте" tag="UNKB"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="в" tag="UNKR"/>
    <form  suffix="дим" tag="UNKY"/>
    <form  suffix="дите" tag="UNKY"/>
    <form  suffix="дут" tag="UNKY"/>
    <form  suffix="м" tag="UNKY"/>
    <form  suffix="ст" tag="UNKY"/>
    <form  suffix="шь" tag="UNKY"/>
  </table>
  <table name="UNK228" rads=".*" fast="-">
    <!-- 22 members -->
    <form  suffix="ий" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="ие" tag="UNKA"/>
    <form  suffix="им" tag="UNKA"/>
    <form  suffix="ими" tag="UNKA"/>
    <form  suffix="их" tag="UNKA"/>
    <form  suffix="ого" tag="UNKA"/>
    <form  suffix="ое" tag="UNKA"/>
    <form  suffix="ой" tag="UNKA"/>
    <form  suffix="ом" tag="UNKA"/>
    <form  suffix="ому" tag="UNKA"/>
    <form  suffix="ою" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
    <form  suffix="о" tag="UNKZ"/>
  </table>
  <table name="UNK229" rads=".*" fast="-">
    <!-- 22 members -->
    <form  suffix="сать" tag="UNK"/>
    <form  suffix="сал" tag="UNKL"/>
    <form  suffix="сала" tag="UNKL"/>
    <form  suffix="сали" tag="UNKL"/>
    <form  suffix="сало" tag="UNKL"/>
    <form  suffix="сав" tag="UNKR"/>
    <form  suffix="ши" tag="UNKT"/>
    <form  suffix="шите" tag="UNKT"/>
    <form  suffix="шем" tag="UNKY"/>
    <form  suffix="шет" tag="UNKY"/>
    <form  suffix="шете" tag="UNKY"/>
    <form  suffix="шешь" tag="UNKY"/>
    <form  suffix="шу" tag="UNKY"/>
    <form  suffix="шут" tag="UNKY"/>
  </table>
  <table name="UNK230" rads=".*" fast="-">
    <!-- 22 members -->
    <form  suffix="сть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="в" tag="UNKR"/>
  </table>
  <table name="UNK231" rads=".*" fast="-">
    <!-- 22 members -->
    <form  suffix="тись" tag="UNK"/>
    <form  suffix="емся" tag="UNKM"/>
    <form  suffix="етесь" tag="UNKM"/>
    <form  suffix="ется" tag="UNKM"/>
    <form  suffix="ешься" tag="UNKM"/>
    <form  suffix="усь" tag="UNKM"/>
    <form  suffix="утся" tag="UNKM"/>
    <form  suffix="ёмся" tag="UNKM"/>
    <form  suffix="ётесь" tag="UNKM"/>
    <form  suffix="ётся" tag="UNKM"/>
    <form  suffix="ёшься" tag="UNKM"/>
  </table>
  <table name="UNK232" rads=".*" fast="-">
    <!-- 22 members -->
    <form  suffix="ый" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="ого" tag="UNKA"/>
    <form  suffix="ое" tag="UNKA"/>
    <form  suffix="ой" tag="UNKA"/>
    <form  suffix="ом" tag="UNKA"/>
    <form  suffix="ому" tag="UNKA"/>
    <form  suffix="ою" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
    <form  suffix="ые" tag="UNKA"/>
    <form  suffix="ым" tag="UNKA"/>
    <form  suffix="ыми" tag="UNKA"/>
    <form  suffix="ых" tag="UNKA"/>
    <form  suffix="ее" tag="UNKE"/>
    <form  suffix="ей" tag="UNKE"/>
  </table>
  <table name="UNK233" rads=".*" fast="-">
    <!-- 22 members -->
    <form  suffix="ый" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="ого" tag="UNKA"/>
    <form  suffix="ое" tag="UNKA"/>
    <form  suffix="ой" tag="UNKA"/>
    <form  suffix="ом" tag="UNKA"/>
    <form  suffix="ому" tag="UNKA"/>
    <form  suffix="ою" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
    <form  suffix="ые" tag="UNKA"/>
    <form  suffix="ым" tag="UNKA"/>
    <form  suffix="ыми" tag="UNKA"/>
    <form  suffix="ых" tag="UNKA"/>
    <form  suffix="ее" tag="UNKE"/>
    <form  suffix="ей" tag="UNKE"/>
    <form  suffix="о" tag="UNKZ"/>
  </table>
  <table name="UNK234" rads=".*" fast="-">
    <!-- 21 members -->
    <form  suffix="диться" tag="UNK"/>
    <form  suffix="дись" tag="UNKB"/>
    <form  suffix="дитесь" tag="UNKB"/>
    <form  suffix="дилась" tag="UNKL"/>
    <form  suffix="дились" tag="UNKL"/>
    <form  suffix="дилось" tag="UNKL"/>
    <form  suffix="дился" tag="UNKL"/>
    <form  suffix="дясь" tag="UNKP"/>
    <form  suffix="димся" tag="UNKW"/>
    <form  suffix="дитесь" tag="UNKW"/>
    <form  suffix="дится" tag="UNKW"/>
    <form  suffix="дишься" tag="UNKW"/>
    <form  suffix="дятся" tag="UNKW"/>
    <form  suffix="жусь" tag="UNKW"/>
  </table>
  <table name="UNK235" rads=".*" fast="-">
    <!-- 21 members -->
    <form  suffix="ести" tag="UNK"/>
    <form  suffix="ел" tag="UNKL"/>
    <form  suffix="ела" tag="UNKL"/>
    <form  suffix="ели" tag="UNKL"/>
    <form  suffix="ело" tag="UNKL"/>
    <form  suffix="ёл" tag="UNKL"/>
  </table>
  <table name="UNK236" rads=".*" fast="-">
    <!-- 21 members -->
    <form  suffix="ести" tag="UNK"/>
    <form  suffix="ел" tag="UNKL"/>
    <form  suffix="ела" tag="UNKL"/>
    <form  suffix="ели" tag="UNKL"/>
    <form  suffix="ело" tag="UNKL"/>
    <form  suffix="ёл" tag="UNKL"/>
    <form  suffix="едем" tag="UNKY"/>
    <form  suffix="едет" tag="UNKY"/>
    <form  suffix="едете" tag="UNKY"/>
    <form  suffix="едешь" tag="UNKY"/>
    <form  suffix="еду" tag="UNKY"/>
    <form  suffix="едут" tag="UNKY"/>
    <form  suffix="едём" tag="UNKY"/>
    <form  suffix="едёт" tag="UNKY"/>
    <form  suffix="едёте" tag="UNKY"/>
    <form  suffix="едёшь" tag="UNKY"/>
  </table>
  <table name="UNK237" rads=".*" fast="-">
    <!-- 21 members -->
    <form  suffix="овать" tag="UNK"/>
    <form  suffix="овал" tag="UNKL"/>
    <form  suffix="овала" tag="UNKL"/>
    <form  suffix="овали" tag="UNKL"/>
    <form  suffix="овало" tag="UNKL"/>
    <form  suffix="уя" tag="UNKQ"/>
  </table>
  <table name="UNK238" rads=".*" fast="-">
    <!-- 21 members -->
    <form  suffix="ситься" tag="UNK"/>
    <form  suffix="силась" tag="UNKL"/>
    <form  suffix="сились" tag="UNKL"/>
    <form  suffix="силось" tag="UNKL"/>
    <form  suffix="сился" tag="UNKL"/>
    <form  suffix="сившись" tag="UNKR"/>
    <form  suffix="симся" tag="UNKW"/>
    <form  suffix="ситесь" tag="UNKW"/>
    <form  suffix="сится" tag="UNKW"/>
    <form  suffix="сишься" tag="UNKW"/>
    <form  suffix="сятся" tag="UNKW"/>
    <form  suffix="шусь" tag="UNKW"/>
  </table>
  <table name="UNK239" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="ел" tag="UNK"/>
    <form  suffix="ла" tag="UNKO"/>
    <form  suffix="лам" tag="UNKO"/>
    <form  suffix="лами" tag="UNKO"/>
    <form  suffix="лах" tag="UNKO"/>
    <form  suffix="ле" tag="UNKO"/>
    <form  suffix="лов" tag="UNKO"/>
    <form  suffix="лом" tag="UNKO"/>
    <form  suffix="лу" tag="UNKO"/>
    <form  suffix="лы" tag="UNKO"/>
  </table>
  <table name="UNK240" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="я" tag="UNKP"/>
    <form  suffix="ь" tag="UNKT"/>
    <form  suffix="ьте" tag="UNKT"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="лю" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK241" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ясь" tag="UNKP"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="юсь" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK242" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="овать" tag="UNK"/>
    <form  suffix="овал" tag="UNKL"/>
    <form  suffix="овала" tag="UNKL"/>
    <form  suffix="овали" tag="UNKL"/>
    <form  suffix="овало" tag="UNKL"/>
    <form  suffix="уем" tag="UNKU"/>
    <form  suffix="ует" tag="UNKU"/>
    <form  suffix="уете" tag="UNKU"/>
    <form  suffix="уешь" tag="UNKU"/>
    <form  suffix="ую" tag="UNKU"/>
    <form  suffix="уют" tag="UNKU"/>
  </table>
  <table name="UNK243" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="рать" tag="UNK"/>
    <form  suffix="рал" tag="UNKL"/>
    <form  suffix="рала" tag="UNKL"/>
    <form  suffix="рали" tag="UNKL"/>
    <form  suffix="рало" tag="UNKL"/>
    <form  suffix="рав" tag="UNKR"/>
    <form  suffix="ерем" tag="UNKY"/>
    <form  suffix="ерет" tag="UNKY"/>
    <form  suffix="ерете" tag="UNKY"/>
    <form  suffix="ерешь" tag="UNKY"/>
    <form  suffix="еру" tag="UNKY"/>
    <form  suffix="ерут" tag="UNKY"/>
    <form  suffix="ерём" tag="UNKY"/>
    <form  suffix="ерёт" tag="UNKY"/>
    <form  suffix="ерёте" tag="UNKY"/>
    <form  suffix="ерёшь" tag="UNKY"/>
  </table>
  <table name="UNK244" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="ю" tag="UNKM"/>
    <form  suffix="ют" tag="UNKM"/>
  </table>
  <table name="UNK245" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="я" tag="UNK"/>
    <form  suffix="е" tag="UNKG"/>
    <form  suffix="ей" tag="UNKG"/>
    <form  suffix="ею" tag="UNKG"/>
    <form  suffix="и" tag="UNKG"/>
    <form  suffix="ю" tag="UNKG"/>
    <form  suffix="ям" tag="UNKG"/>
    <form  suffix="ями" tag="UNKG"/>
    <form  suffix="ях" tag="UNKG"/>
    <form  suffix="ёй" tag="UNKG"/>
    <form  suffix="ёю" tag="UNKG"/>
  </table>
  <table name="UNK246" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="еть" tag="UNK"/>
    <form  suffix="ел" tag="UNKL"/>
    <form  suffix="ела" tag="UNKL"/>
    <form  suffix="ели" tag="UNKL"/>
    <form  suffix="ело" tag="UNKL"/>
    <form  suffix="ев" tag="UNKR"/>
    <form  suffix="и" tag="UNKT"/>
    <form  suffix="ите" tag="UNKT"/>
    <form  suffix="им" tag="UNKY"/>
    <form  suffix="ит" tag="UNKY"/>
    <form  suffix="ите" tag="UNKY"/>
    <form  suffix="ишь" tag="UNKY"/>
    <form  suffix="ю" tag="UNKY"/>
    <form  suffix="ят" tag="UNKY"/>
  </table>
  <table name="UNK247" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="зить" tag="UNK"/>
    <form  suffix="зи" tag="UNKB"/>
    <form  suffix="зите" tag="UNKB"/>
    <form  suffix="зил" tag="UNKL"/>
    <form  suffix="зила" tag="UNKL"/>
    <form  suffix="зили" tag="UNKL"/>
    <form  suffix="зило" tag="UNKL"/>
    <form  suffix="зя" tag="UNKP"/>
    <form  suffix="жу" tag="UNKW"/>
    <form  suffix="зим" tag="UNKW"/>
    <form  suffix="зит" tag="UNKW"/>
    <form  suffix="зите" tag="UNKW"/>
    <form  suffix="зишь" tag="UNKW"/>
    <form  suffix="зят" tag="UNKW"/>
  </table>
  <table name="UNK248" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="кать" tag="UNK"/>
    <form  suffix="кал" tag="UNKL"/>
    <form  suffix="кала" tag="UNKL"/>
    <form  suffix="кали" tag="UNKL"/>
    <form  suffix="кало" tag="UNKL"/>
    <form  suffix="кав" tag="UNKR"/>
    <form  suffix="чем" tag="UNKY"/>
    <form  suffix="чет" tag="UNKY"/>
    <form  suffix="чете" tag="UNKY"/>
    <form  suffix="чешь" tag="UNKY"/>
    <form  suffix="чу" tag="UNKY"/>
    <form  suffix="чут" tag="UNKY"/>
  </table>
  <table name="UNK249" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="раться" tag="UNK"/>
    <form  suffix="ралась" tag="UNKL"/>
    <form  suffix="рались" tag="UNKL"/>
    <form  suffix="ралось" tag="UNKL"/>
    <form  suffix="рался" tag="UNKL"/>
    <form  suffix="равшись" tag="UNKR"/>
    <form  suffix="еремся" tag="UNKY"/>
    <form  suffix="еретесь" tag="UNKY"/>
    <form  suffix="ерется" tag="UNKY"/>
    <form  suffix="ерешься" tag="UNKY"/>
    <form  suffix="ерусь" tag="UNKY"/>
    <form  suffix="ерутся" tag="UNKY"/>
    <form  suffix="ерёмся" tag="UNKY"/>
    <form  suffix="ерётесь" tag="UNKY"/>
    <form  suffix="ерётся" tag="UNKY"/>
    <form  suffix="ерёшься" tag="UNKY"/>
  </table>
  <table name="UNK250" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="сти" tag="UNK"/>
    <form  suffix="бем" tag="UNKY"/>
    <form  suffix="бет" tag="UNKY"/>
    <form  suffix="бете" tag="UNKY"/>
    <form  suffix="бешь" tag="UNKY"/>
    <form  suffix="бу" tag="UNKY"/>
    <form  suffix="бут" tag="UNKY"/>
    <form  suffix="бём" tag="UNKY"/>
    <form  suffix="бёт" tag="UNKY"/>
    <form  suffix="бёте" tag="UNKY"/>
    <form  suffix="бёшь" tag="UNKY"/>
  </table>
  <table name="UNK251" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="тать" tag="UNK"/>
    <form  suffix="тал" tag="UNKL"/>
    <form  suffix="тала" tag="UNKL"/>
    <form  suffix="тали" tag="UNKL"/>
    <form  suffix="тало" tag="UNKL"/>
    <form  suffix="чем" tag="UNKY"/>
    <form  suffix="чет" tag="UNKY"/>
    <form  suffix="чете" tag="UNKY"/>
    <form  suffix="чешь" tag="UNKY"/>
    <form  suffix="чу" tag="UNKY"/>
    <form  suffix="чут" tag="UNKY"/>
  </table>
  <table name="UNK252" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="й" tag="UNKB"/>
    <form  suffix="йте" tag="UNKB"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="ю" tag="UNKM"/>
    <form  suffix="ют" tag="UNKM"/>
    <form  suffix="я" tag="UNKP"/>
    <form  suffix="в" tag="UNKR"/>
  </table>
  <table name="UNK253" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="уться" tag="UNK"/>
    <form  suffix="улась" tag="UNKL"/>
    <form  suffix="улись" tag="UNKL"/>
    <form  suffix="улось" tag="UNKL"/>
    <form  suffix="улся" tag="UNKL"/>
    <form  suffix="емся" tag="UNKM"/>
    <form  suffix="етесь" tag="UNKM"/>
    <form  suffix="ется" tag="UNKM"/>
    <form  suffix="ешься" tag="UNKM"/>
    <form  suffix="усь" tag="UNKM"/>
    <form  suffix="утся" tag="UNKM"/>
    <form  suffix="увшись" tag="UNKR"/>
  </table>
  <table name="UNK254" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="чься" tag="UNK"/>
    <form  suffix="гусь" tag="UNKU"/>
    <form  suffix="гутся" tag="UNKU"/>
    <form  suffix="жемся" tag="UNKU"/>
    <form  suffix="жетесь" tag="UNKU"/>
    <form  suffix="жется" tag="UNKU"/>
    <form  suffix="жешься" tag="UNKU"/>
    <form  suffix="жёмся" tag="UNKU"/>
    <form  suffix="жётесь" tag="UNKU"/>
    <form  suffix="жётся" tag="UNKU"/>
    <form  suffix="жёшься" tag="UNKU"/>
  </table>
  <table name="UNK255" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="ыться" tag="UNK"/>
    <form  suffix="ойся" tag="UNKB"/>
    <form  suffix="ойтесь" tag="UNKB"/>
    <form  suffix="ылась" tag="UNKL"/>
    <form  suffix="ылись" tag="UNKL"/>
    <form  suffix="ылось" tag="UNKL"/>
    <form  suffix="ылся" tag="UNKL"/>
    <form  suffix="оемся" tag="UNKM"/>
    <form  suffix="оетесь" tag="UNKM"/>
    <form  suffix="оется" tag="UNKM"/>
    <form  suffix="оешься" tag="UNKM"/>
    <form  suffix="оюсь" tag="UNKM"/>
    <form  suffix="оются" tag="UNKM"/>
    <form  suffix="ывшись" tag="UNKR"/>
  </table>
  <table name="UNK256" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="ённый" tag="UNK"/>
    <form  suffix="ённая" tag="UNKA"/>
    <form  suffix="ённого" tag="UNKA"/>
    <form  suffix="ённое" tag="UNKA"/>
    <form  suffix="ённой" tag="UNKA"/>
    <form  suffix="ённом" tag="UNKA"/>
    <form  suffix="ённому" tag="UNKA"/>
    <form  suffix="ённою" tag="UNKA"/>
    <form  suffix="ённую" tag="UNKA"/>
    <form  suffix="ённые" tag="UNKA"/>
    <form  suffix="ённым" tag="UNKA"/>
    <form  suffix="ёнными" tag="UNKA"/>
    <form  suffix="ённых" tag="UNKA"/>
    <form  suffix="ена" tag="UNKS"/>
    <form  suffix="ено" tag="UNKS"/>
    <form  suffix="ены" tag="UNKS"/>
    <form  suffix="ён" tag="UNKS"/>
    <form  suffix="ёна" tag="UNKS"/>
    <form  suffix="ёно" tag="UNKS"/>
    <form  suffix="ёны" tag="UNKS"/>
    <form  suffix="ённо" tag="UNKZ"/>
  </table>
  <table name="UNK257" rads=".*" fast="-">
    <!-- 18 members -->
    <form  suffix="деть" tag="UNK"/>
    <form  suffix="дел" tag="UNKL"/>
    <form  suffix="дела" tag="UNKL"/>
    <form  suffix="дели" tag="UNKL"/>
    <form  suffix="дело" tag="UNKL"/>
    <form  suffix="дев" tag="UNKR"/>
    <form  suffix="ди" tag="UNKT"/>
    <form  suffix="дите" tag="UNKT"/>
    <form  suffix="дим" tag="UNKY"/>
    <form  suffix="дит" tag="UNKY"/>
    <form  suffix="дите" tag="UNKY"/>
    <form  suffix="дишь" tag="UNKY"/>
    <form  suffix="дят" tag="UNKY"/>
    <form  suffix="жу" tag="UNKY"/>
  </table>
  <table name="UNK258" rads=".*" fast="-">
    <!-- 18 members -->
    <form  suffix="еть" tag="UNK"/>
    <form  suffix="ел" tag="UNKL"/>
    <form  suffix="ела" tag="UNKL"/>
    <form  suffix="ели" tag="UNKL"/>
    <form  suffix="ело" tag="UNKL"/>
    <form  suffix="ев" tag="UNKR"/>
    <form  suffix="и" tag="UNKT"/>
    <form  suffix="ите" tag="UNKT"/>
    <form  suffix="им" tag="UNKY"/>
    <form  suffix="ит" tag="UNKY"/>
    <form  suffix="ите" tag="UNKY"/>
    <form  suffix="ишь" tag="UNKY"/>
    <form  suffix="лю" tag="UNKY"/>
    <form  suffix="ят" tag="UNKY"/>
  </table>
  <table name="UNK259" rads=".*" fast="-">
    <!-- 18 members -->
    <form  suffix="зиться" tag="UNK"/>
    <form  suffix="зись" tag="UNKB"/>
    <form  suffix="зитесь" tag="UNKB"/>
    <form  suffix="зилась" tag="UNKL"/>
    <form  suffix="зились" tag="UNKL"/>
    <form  suffix="зилось" tag="UNKL"/>
    <form  suffix="зился" tag="UNKL"/>
    <form  suffix="зившись" tag="UNKR"/>
    <form  suffix="жусь" tag="UNKW"/>
    <form  suffix="зимся" tag="UNKW"/>
    <form  suffix="зитесь" tag="UNKW"/>
    <form  suffix="зится" tag="UNKW"/>
    <form  suffix="зишься" tag="UNKW"/>
    <form  suffix="зятся" tag="UNKW"/>
  </table>
  <table name="UNK260" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="а" tag="UNK"/>
    <form  suffix="е" tag="UNKH"/>
    <form  suffix="ей" tag="UNKH"/>
    <form  suffix="ею" tag="UNKH"/>
    <form  suffix="и" tag="UNKH"/>
    <form  suffix="у" tag="UNKH"/>
  </table>
  <table name="UNK261" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="ать" tag="UNK"/>
    <form  suffix="ал" tag="UNKL"/>
    <form  suffix="ала" tag="UNKL"/>
    <form  suffix="али" tag="UNKL"/>
    <form  suffix="ало" tag="UNKL"/>
    <form  suffix="и" tag="UNKT"/>
    <form  suffix="ат" tag="UNKU"/>
    <form  suffix="им" tag="UNKU"/>
    <form  suffix="ит" tag="UNKU"/>
    <form  suffix="ите" tag="UNKU"/>
    <form  suffix="ишь" tag="UNKU"/>
    <form  suffix="у" tag="UNKU"/>
  </table>
  <table name="UNK262" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="евать" tag="UNK"/>
    <form  suffix="евал" tag="UNKL"/>
    <form  suffix="евала" tag="UNKL"/>
    <form  suffix="евали" tag="UNKL"/>
    <form  suffix="евало" tag="UNKL"/>
    <form  suffix="евав" tag="UNKR"/>
    <form  suffix="уй" tag="UNKT"/>
    <form  suffix="уйте" tag="UNKT"/>
    <form  suffix="уем" tag="UNKU"/>
    <form  suffix="ует" tag="UNKU"/>
    <form  suffix="уете" tag="UNKU"/>
    <form  suffix="уешь" tag="UNKU"/>
    <form  suffix="ую" tag="UNKU"/>
    <form  suffix="уют" tag="UNKU"/>
  </table>
  <table name="UNK263" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="жать" tag="UNK"/>
    <form  suffix="жал" tag="UNKL"/>
    <form  suffix="жала" tag="UNKL"/>
    <form  suffix="жали" tag="UNKL"/>
    <form  suffix="жало" tag="UNKL"/>
    <form  suffix="жав" tag="UNKR"/>
    <form  suffix="гу" tag="UNKY"/>
    <form  suffix="гут" tag="UNKY"/>
    <form  suffix="жим" tag="UNKY"/>
    <form  suffix="жит" tag="UNKY"/>
    <form  suffix="жите" tag="UNKY"/>
    <form  suffix="жишь" tag="UNKY"/>
  </table>
  <table name="UNK264" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="ись" tag="UNKB"/>
    <form  suffix="итесь" tag="UNKB"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ясь" tag="UNKP"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="люсь" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK265" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="оть" tag="UNK"/>
    <form  suffix="и" tag="UNKB"/>
    <form  suffix="ите" tag="UNKB"/>
    <form  suffix="ол" tag="UNKL"/>
    <form  suffix="ола" tag="UNKL"/>
    <form  suffix="оли" tag="UNKL"/>
    <form  suffix="оло" tag="UNKL"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="ю" tag="UNKM"/>
    <form  suffix="ют" tag="UNKM"/>
    <form  suffix="ов" tag="UNKR"/>
  </table>
  <table name="UNK266" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="ти" tag="UNK"/>
    <form  suffix="дем" tag="UNKM"/>
    <form  suffix="дет" tag="UNKM"/>
    <form  suffix="дете" tag="UNKM"/>
    <form  suffix="дешь" tag="UNKM"/>
    <form  suffix="ду" tag="UNKM"/>
    <form  suffix="дут" tag="UNKM"/>
    <form  suffix="дём" tag="UNKM"/>
    <form  suffix="дёт" tag="UNKM"/>
    <form  suffix="дёте" tag="UNKM"/>
    <form  suffix="дёшь" tag="UNKM"/>
  </table>
  <table name="UNK267" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="ь" tag="UNK"/>
    <form  suffix="ам" tag="UNKN"/>
    <form  suffix="ами" tag="UNKN"/>
    <form  suffix="ах" tag="UNKN"/>
    <form  suffix="ей" tag="UNKN"/>
    <form  suffix="и" tag="UNKN"/>
    <form  suffix="ью" tag="UNKN"/>
  </table>
  <table name="UNK268" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="ать" tag="UNK"/>
    <form  suffix="ал" tag="UNKL"/>
    <form  suffix="ала" tag="UNKL"/>
    <form  suffix="али" tag="UNKL"/>
    <form  suffix="ало" tag="UNKL"/>
    <form  suffix="ат" tag="UNKU"/>
    <form  suffix="им" tag="UNKU"/>
    <form  suffix="ит" tag="UNKU"/>
    <form  suffix="ите" tag="UNKU"/>
    <form  suffix="ишь" tag="UNKU"/>
    <form  suffix="у" tag="UNKU"/>
  </table>
  <table name="UNK269" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="аться" tag="UNK"/>
    <form  suffix="алась" tag="UNKL"/>
    <form  suffix="ались" tag="UNKL"/>
    <form  suffix="алось" tag="UNKL"/>
    <form  suffix="ался" tag="UNKL"/>
    <form  suffix="авшись" tag="UNKR"/>
    <form  suffix="ись" tag="UNKT"/>
    <form  suffix="атся" tag="UNKU"/>
    <form  suffix="имся" tag="UNKU"/>
    <form  suffix="итесь" tag="UNKU"/>
    <form  suffix="ится" tag="UNKU"/>
    <form  suffix="ишься" tag="UNKU"/>
    <form  suffix="усь" tag="UNKU"/>
  </table>
  <table name="UNK270" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="ваться" tag="UNK"/>
    <form  suffix="вайся" tag="UNKB"/>
    <form  suffix="вайтесь" tag="UNKB"/>
    <form  suffix="валась" tag="UNKL"/>
    <form  suffix="вались" tag="UNKL"/>
    <form  suffix="валось" tag="UNKL"/>
    <form  suffix="вался" tag="UNKL"/>
    <form  suffix="ваясь" tag="UNKP"/>
    <form  suffix="емся" tag="UNKU"/>
    <form  suffix="етесь" tag="UNKU"/>
    <form  suffix="ется" tag="UNKU"/>
    <form  suffix="ешься" tag="UNKU"/>
    <form  suffix="юсь" tag="UNKU"/>
    <form  suffix="ются" tag="UNKU"/>
    <form  suffix="ёмся" tag="UNKU"/>
    <form  suffix="ётесь" tag="UNKU"/>
    <form  suffix="ётся" tag="UNKU"/>
    <form  suffix="ёшься" tag="UNKU"/>
  </table>
  <table name="UNK271" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="ечься" tag="UNK"/>
    <form  suffix="еклась" tag="UNKL"/>
    <form  suffix="еклись" tag="UNKL"/>
    <form  suffix="еклось" tag="UNKL"/>
    <form  suffix="екся" tag="UNKL"/>
    <form  suffix="ёкся" tag="UNKL"/>
    <form  suffix="екусь" tag="UNKM"/>
    <form  suffix="екутся" tag="UNKM"/>
    <form  suffix="ечемся" tag="UNKM"/>
    <form  suffix="ечетесь" tag="UNKM"/>
    <form  suffix="ечется" tag="UNKM"/>
    <form  suffix="ечешься" tag="UNKM"/>
    <form  suffix="ечёмся" tag="UNKM"/>
    <form  suffix="ечётесь" tag="UNKM"/>
    <form  suffix="ечётся" tag="UNKM"/>
    <form  suffix="ечёшься" tag="UNKM"/>
  </table>
  <table name="UNK272" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ившись" tag="UNKR"/>
    <form  suffix="йся" tag="UNKT"/>
    <form  suffix="йтесь" tag="UNKT"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="юсь" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK273" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ившись" tag="UNKR"/>
    <form  suffix="ьемся" tag="UNKV"/>
    <form  suffix="ьетесь" tag="UNKV"/>
    <form  suffix="ьется" tag="UNKV"/>
    <form  suffix="ьешься" tag="UNKV"/>
    <form  suffix="ьюсь" tag="UNKV"/>
    <form  suffix="ьются" tag="UNKV"/>
    <form  suffix="ьёмся" tag="UNKV"/>
    <form  suffix="ьётесь" tag="UNKV"/>
    <form  suffix="ьётся" tag="UNKV"/>
    <form  suffix="ьёшься" tag="UNKV"/>
  </table>
  <table name="UNK274" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="ситься" tag="UNK"/>
    <form  suffix="силась" tag="UNKL"/>
    <form  suffix="сились" tag="UNKL"/>
    <form  suffix="силось" tag="UNKL"/>
    <form  suffix="сился" tag="UNKL"/>
    <form  suffix="сившись" tag="UNKR"/>
    <form  suffix="сься" tag="UNKT"/>
    <form  suffix="сьтесь" tag="UNKT"/>
    <form  suffix="симся" tag="UNKW"/>
    <form  suffix="ситесь" tag="UNKW"/>
    <form  suffix="сится" tag="UNKW"/>
    <form  suffix="сишься" tag="UNKW"/>
    <form  suffix="сятся" tag="UNKW"/>
    <form  suffix="шусь" tag="UNKW"/>
  </table>
  <table name="UNK275" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="скать" tag="UNK"/>
    <form  suffix="скал" tag="UNKL"/>
    <form  suffix="скала" tag="UNKL"/>
    <form  suffix="скали" tag="UNKL"/>
    <form  suffix="скало" tag="UNKL"/>
    <form  suffix="скав" tag="UNKR"/>
    <form  suffix="щем" tag="UNKY"/>
    <form  suffix="щет" tag="UNKY"/>
    <form  suffix="щете" tag="UNKY"/>
    <form  suffix="щешь" tag="UNKY"/>
    <form  suffix="щу" tag="UNKY"/>
    <form  suffix="щут" tag="UNKY"/>
  </table>
  <table name="UNK276" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="уть" tag="UNK"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="у" tag="UNKM"/>
    <form  suffix="ут" tag="UNKM"/>
  </table>
  <table name="UNK277" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="уться" tag="UNK"/>
    <form  suffix="улась" tag="UNKL"/>
    <form  suffix="улись" tag="UNKL"/>
    <form  suffix="улось" tag="UNKL"/>
    <form  suffix="улся" tag="UNKL"/>
    <form  suffix="увшись" tag="UNKR"/>
    <form  suffix="емся" tag="UNKU"/>
    <form  suffix="етесь" tag="UNKU"/>
    <form  suffix="ется" tag="UNKU"/>
    <form  suffix="ешься" tag="UNKU"/>
    <form  suffix="усь" tag="UNKU"/>
    <form  suffix="утся" tag="UNKU"/>
    <form  suffix="ёмся" tag="UNKU"/>
    <form  suffix="ётесь" tag="UNKU"/>
    <form  suffix="ётся" tag="UNKU"/>
    <form  suffix="ёшься" tag="UNKU"/>
  </table>
  <table name="UNK278" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="хать" tag="UNK"/>
    <form  suffix="хал" tag="UNKL"/>
    <form  suffix="хала" tag="UNKL"/>
    <form  suffix="хали" tag="UNKL"/>
    <form  suffix="хало" tag="UNKL"/>
    <form  suffix="хав" tag="UNKR"/>
    <form  suffix="дем" tag="UNKV"/>
    <form  suffix="дет" tag="UNKV"/>
    <form  suffix="дете" tag="UNKV"/>
    <form  suffix="дешь" tag="UNKV"/>
    <form  suffix="ду" tag="UNKV"/>
    <form  suffix="дут" tag="UNKV"/>
  </table>
  <table name="UNK279" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="ять" tag="UNK"/>
    <form  suffix="ял" tag="UNKL"/>
    <form  suffix="яла" tag="UNKL"/>
    <form  suffix="яли" tag="UNKL"/>
    <form  suffix="яло" tag="UNKL"/>
    <form  suffix="яв" tag="UNKR"/>
    <form  suffix="й" tag="UNKT"/>
    <form  suffix="йте" tag="UNKT"/>
    <form  suffix="ем" tag="UNKU"/>
    <form  suffix="ет" tag="UNKU"/>
    <form  suffix="ете" tag="UNKU"/>
    <form  suffix="ешь" tag="UNKU"/>
    <form  suffix="ю" tag="UNKU"/>
    <form  suffix="ют" tag="UNKU"/>
  </table>
  <table name="UNK280" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="езти" tag="UNK"/>
    <form  suffix="ез" tag="UNKL"/>
    <form  suffix="езла" tag="UNKL"/>
    <form  suffix="езли" tag="UNKL"/>
    <form  suffix="езло" tag="UNKL"/>
    <form  suffix="ёз" tag="UNKL"/>
    <form  suffix="езем" tag="UNKM"/>
    <form  suffix="езет" tag="UNKM"/>
    <form  suffix="езете" tag="UNKM"/>
    <form  suffix="езешь" tag="UNKM"/>
    <form  suffix="езу" tag="UNKM"/>
    <form  suffix="езут" tag="UNKM"/>
    <form  suffix="езём" tag="UNKM"/>
    <form  suffix="езёт" tag="UNKM"/>
    <form  suffix="езёте" tag="UNKM"/>
    <form  suffix="езёшь" tag="UNKM"/>
  </table>
  <table name="UNK281" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="ечь" tag="UNK"/>
    <form  suffix="ек" tag="UNKL"/>
    <form  suffix="екла" tag="UNKL"/>
    <form  suffix="екли" tag="UNKL"/>
    <form  suffix="екло" tag="UNKL"/>
    <form  suffix="ёк" tag="UNKL"/>
  </table>
  <table name="UNK282" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="зить" tag="UNK"/>
    <form  suffix="зил" tag="UNKL"/>
    <form  suffix="зила" tag="UNKL"/>
    <form  suffix="зили" tag="UNKL"/>
    <form  suffix="зило" tag="UNKL"/>
    <form  suffix="зив" tag="UNKR"/>
    <form  suffix="зь" tag="UNKT"/>
    <form  suffix="зьте" tag="UNKT"/>
    <form  suffix="жу" tag="UNKW"/>
    <form  suffix="зим" tag="UNKW"/>
    <form  suffix="зит" tag="UNKW"/>
    <form  suffix="зите" tag="UNKW"/>
    <form  suffix="зишь" tag="UNKW"/>
    <form  suffix="зят" tag="UNKW"/>
  </table>
  <table name="UNK283" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="а" tag="UNKP"/>
    <form  suffix="ат" tag="UNKW"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="у" tag="UNKW"/>
  </table>
  <table name="UNK284" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="атся" tag="UNKD"/>
    <form  suffix="ится" tag="UNKD"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
  </table>
  <table name="UNK285" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ась" tag="UNKP"/>
    <form  suffix="ься" tag="UNKT"/>
    <form  suffix="ьтесь" tag="UNKT"/>
    <form  suffix="атся" tag="UNKW"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="усь" tag="UNKW"/>
  </table>
  <table name="UNK286" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="оваться" tag="UNK"/>
    <form  suffix="овалась" tag="UNKL"/>
    <form  suffix="овались" tag="UNKL"/>
    <form  suffix="овалось" tag="UNKL"/>
    <form  suffix="овался" tag="UNKL"/>
    <form  suffix="уясь" tag="UNKQ"/>
    <form  suffix="овавшись" tag="UNKR"/>
    <form  suffix="уйся" tag="UNKT"/>
    <form  suffix="уйтесь" tag="UNKT"/>
    <form  suffix="уемся" tag="UNKU"/>
    <form  suffix="уетесь" tag="UNKU"/>
    <form  suffix="уется" tag="UNKU"/>
    <form  suffix="уешься" tag="UNKU"/>
    <form  suffix="уюсь" tag="UNKU"/>
    <form  suffix="уются" tag="UNKU"/>
  </table>
  <table name="UNK287" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="сать" tag="UNK"/>
    <form  suffix="сал" tag="UNKL"/>
    <form  suffix="сала" tag="UNKL"/>
    <form  suffix="сали" tag="UNKL"/>
    <form  suffix="сало" tag="UNKL"/>
    <form  suffix="сав" tag="UNKR"/>
    <form  suffix="шем" tag="UNKY"/>
    <form  suffix="шет" tag="UNKY"/>
    <form  suffix="шете" tag="UNKY"/>
    <form  suffix="шешь" tag="UNKY"/>
    <form  suffix="шу" tag="UNKY"/>
    <form  suffix="шут" tag="UNKY"/>
  </table>
  <table name="UNK288" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="ти" tag="UNK"/>
    <form  suffix="" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="у" tag="UNKM"/>
    <form  suffix="ут" tag="UNKM"/>
    <form  suffix="ём" tag="UNKM"/>
    <form  suffix="ёт" tag="UNKM"/>
    <form  suffix="ёте" tag="UNKM"/>
    <form  suffix="ёшь" tag="UNKM"/>
  </table>
  <table name="UNK289" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="ясь" tag="UNKP"/>
  </table>
  <table name="UNK290" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="ый" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="его" tag="UNKA"/>
    <form  suffix="ее" tag="UNKA"/>
    <form  suffix="ей" tag="UNKA"/>
    <form  suffix="ем" tag="UNKA"/>
    <form  suffix="ему" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
    <form  suffix="ые" tag="UNKA"/>
    <form  suffix="ым" tag="UNKA"/>
    <form  suffix="ыми" tag="UNKA"/>
    <form  suffix="ых" tag="UNKA"/>
  </table>
  <table name="UNK291" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="ённый" tag="UNK"/>
    <form  suffix="ённая" tag="UNKA"/>
    <form  suffix="ённого" tag="UNKA"/>
    <form  suffix="ённое" tag="UNKA"/>
    <form  suffix="ённой" tag="UNKA"/>
    <form  suffix="ённом" tag="UNKA"/>
    <form  suffix="ённому" tag="UNKA"/>
    <form  suffix="ённою" tag="UNKA"/>
    <form  suffix="ённую" tag="UNKA"/>
    <form  suffix="ённые" tag="UNKA"/>
    <form  suffix="ённым" tag="UNKA"/>
    <form  suffix="ёнными" tag="UNKA"/>
    <form  suffix="ённых" tag="UNKA"/>
    <form  suffix="ённее" tag="UNKE"/>
    <form  suffix="ённей" tag="UNKE"/>
    <form  suffix="ена" tag="UNKS"/>
    <form  suffix="ено" tag="UNKS"/>
    <form  suffix="ены" tag="UNKS"/>
    <form  suffix="ён" tag="UNKS"/>
    <form  suffix="ёна" tag="UNKS"/>
    <form  suffix="ёно" tag="UNKS"/>
    <form  suffix="ёны" tag="UNKS"/>
    <form  suffix="ённа" tag="UNKX"/>
    <form  suffix="ённо" tag="UNKX"/>
    <form  suffix="ённы" tag="UNKX"/>
  </table>
  <table name="UNK292" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="аться" tag="UNK"/>
    <form  suffix="алась" tag="UNKL"/>
    <form  suffix="ались" tag="UNKL"/>
    <form  suffix="алось" tag="UNKL"/>
    <form  suffix="ался" tag="UNKL"/>
    <form  suffix="авшись" tag="UNKR"/>
    <form  suffix="атся" tag="UNKU"/>
    <form  suffix="имся" tag="UNKU"/>
    <form  suffix="итесь" tag="UNKU"/>
    <form  suffix="ится" tag="UNKU"/>
    <form  suffix="ишься" tag="UNKU"/>
    <form  suffix="усь" tag="UNKU"/>
  </table>
  <table name="UNK293" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="дить" tag="UNK"/>
    <form  suffix="дил" tag="UNKL"/>
    <form  suffix="дила" tag="UNKL"/>
    <form  suffix="дили" tag="UNKL"/>
    <form  suffix="дило" tag="UNKL"/>
    <form  suffix="дя" tag="UNKP"/>
    <form  suffix="дим" tag="UNKW"/>
    <form  suffix="дит" tag="UNKW"/>
    <form  suffix="дите" tag="UNKW"/>
    <form  suffix="дишь" tag="UNKW"/>
    <form  suffix="дят" tag="UNKW"/>
    <form  suffix="жу" tag="UNKW"/>
  </table>
  <table name="UNK294" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
  </table>
  <table name="UNK295" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="в" tag="UNKR"/>
    <form  suffix="вем" tag="UNKY"/>
    <form  suffix="вет" tag="UNKY"/>
    <form  suffix="вете" tag="UNKY"/>
    <form  suffix="вешь" tag="UNKY"/>
    <form  suffix="ву" tag="UNKY"/>
    <form  suffix="вут" tag="UNKY"/>
    <form  suffix="вём" tag="UNKY"/>
    <form  suffix="вёт" tag="UNKY"/>
    <form  suffix="вёте" tag="UNKY"/>
    <form  suffix="вёшь" tag="UNKY"/>
  </table>
  <table name="UNK296" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="уть" tag="UNK"/>
    <form  suffix="и" tag="UNKB"/>
    <form  suffix="ите" tag="UNKB"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="у" tag="UNKM"/>
    <form  suffix="ут" tag="UNKM"/>
  </table>
  <table name="UNK297" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="ыть" tag="UNK"/>
    <form  suffix="ыл" tag="UNKL"/>
    <form  suffix="ыла" tag="UNKL"/>
    <form  suffix="ыли" tag="UNKL"/>
    <form  suffix="ыло" tag="UNKL"/>
    <form  suffix="ыв" tag="UNKR"/>
    <form  suffix="удем" tag="UNKY"/>
    <form  suffix="удет" tag="UNKY"/>
    <form  suffix="удете" tag="UNKY"/>
    <form  suffix="удешь" tag="UNKY"/>
    <form  suffix="уду" tag="UNKY"/>
    <form  suffix="удут" tag="UNKY"/>
  </table>
  <table name="UNK298" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="ять" tag="UNK"/>
    <form  suffix="ял" tag="UNKL"/>
    <form  suffix="яла" tag="UNKL"/>
    <form  suffix="яли" tag="UNKL"/>
    <form  suffix="яло" tag="UNKL"/>
    <form  suffix="яв" tag="UNKR"/>
    <form  suffix="ем" tag="UNKU"/>
    <form  suffix="ет" tag="UNKU"/>
    <form  suffix="ете" tag="UNKU"/>
    <form  suffix="ешь" tag="UNKU"/>
    <form  suffix="ю" tag="UNKU"/>
    <form  suffix="ют" tag="UNKU"/>
  </table>
  <table name="UNK299" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="ёж" tag="UNK"/>
    <form  suffix="ежа" tag="UNKJ"/>
    <form  suffix="еже" tag="UNKJ"/>
    <form  suffix="ежу" tag="UNKJ"/>
    <form  suffix="ёжа" tag="UNKJ"/>
    <form  suffix="ёже" tag="UNKJ"/>
    <form  suffix="ёжу" tag="UNKJ"/>
  </table>
  <table name="UNK300" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="еть" tag="UNK"/>
    <form  suffix="ел" tag="UNKL"/>
    <form  suffix="ела" tag="UNKL"/>
    <form  suffix="ели" tag="UNKL"/>
    <form  suffix="ело" tag="UNKL"/>
    <form  suffix="ев" tag="UNKR"/>
    <form  suffix="им" tag="UNKY"/>
    <form  suffix="ит" tag="UNKY"/>
    <form  suffix="ите" tag="UNKY"/>
    <form  suffix="ишь" tag="UNKY"/>
    <form  suffix="ят" tag="UNKY"/>
  </table>
  <table name="UNK301" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="я" tag="UNKP"/>
  </table>
  <table name="UNK302" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="я" tag="UNKP"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="лю" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK303" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ась" tag="UNKP"/>
    <form  suffix="атся" tag="UNKW"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="усь" tag="UNKW"/>
  </table>
  <table name="UNK304" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="ем" tag="UNKY"/>
    <form  suffix="ет" tag="UNKY"/>
    <form  suffix="ете" tag="UNKY"/>
    <form  suffix="ешь" tag="UNKY"/>
    <form  suffix="у" tag="UNKY"/>
    <form  suffix="ут" tag="UNKY"/>
  </table>
  <table name="UNK305" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="в" tag="UNKR"/>
    <form  suffix="нем" tag="UNKY"/>
    <form  suffix="нет" tag="UNKY"/>
    <form  suffix="нете" tag="UNKY"/>
    <form  suffix="нешь" tag="UNKY"/>
    <form  suffix="ну" tag="UNKY"/>
    <form  suffix="нут" tag="UNKY"/>
  </table>
  <table name="UNK306" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="чь" tag="UNK"/>
    <form  suffix="к" tag="UNKL"/>
    <form  suffix="кла" tag="UNKL"/>
    <form  suffix="кли" tag="UNKL"/>
    <form  suffix="кло" tag="UNKL"/>
    <form  suffix="ку" tag="UNKM"/>
    <form  suffix="кут" tag="UNKM"/>
    <form  suffix="чем" tag="UNKM"/>
    <form  suffix="чет" tag="UNKM"/>
    <form  suffix="чете" tag="UNKM"/>
    <form  suffix="чешь" tag="UNKM"/>
    <form  suffix="чём" tag="UNKM"/>
    <form  suffix="чёт" tag="UNKM"/>
    <form  suffix="чёте" tag="UNKM"/>
    <form  suffix="чёшь" tag="UNKM"/>
  </table>
  <table name="UNK307" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="еть" tag="UNK"/>
    <form  suffix="ел" tag="UNKL"/>
    <form  suffix="ела" tag="UNKL"/>
    <form  suffix="ели" tag="UNKL"/>
    <form  suffix="ело" tag="UNKL"/>
    <form  suffix="и" tag="UNKT"/>
    <form  suffix="ите" tag="UNKT"/>
    <form  suffix="им" tag="UNKY"/>
    <form  suffix="ит" tag="UNKY"/>
    <form  suffix="ите" tag="UNKY"/>
    <form  suffix="ишь" tag="UNKY"/>
    <form  suffix="ят" tag="UNKY"/>
  </table>
  <table name="UNK308" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="зать" tag="UNK"/>
    <form  suffix="зай" tag="UNKB"/>
    <form  suffix="зайте" tag="UNKB"/>
    <form  suffix="зал" tag="UNKL"/>
    <form  suffix="зала" tag="UNKL"/>
    <form  suffix="зали" tag="UNKL"/>
    <form  suffix="зало" tag="UNKL"/>
    <form  suffix="заем" tag="UNKM"/>
    <form  suffix="зает" tag="UNKM"/>
    <form  suffix="заете" tag="UNKM"/>
    <form  suffix="заешь" tag="UNKM"/>
    <form  suffix="заю" tag="UNKM"/>
    <form  suffix="зают" tag="UNKM"/>
    <form  suffix="зая" tag="UNKP"/>
    <form  suffix="зав" tag="UNKR"/>
    <form  suffix="жем" tag="UNKY"/>
    <form  suffix="жет" tag="UNKY"/>
    <form  suffix="жете" tag="UNKY"/>
    <form  suffix="жешь" tag="UNKY"/>
    <form  suffix="жу" tag="UNKY"/>
    <form  suffix="жут" tag="UNKY"/>
  </table>
  <table name="UNK309" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="и" tag="UNK"/>
    <form  suffix="ем" tag="UNKY"/>
    <form  suffix="ет" tag="UNKY"/>
    <form  suffix="ете" tag="UNKY"/>
    <form  suffix="ешь" tag="UNKY"/>
    <form  suffix="у" tag="UNKY"/>
    <form  suffix="ут" tag="UNKY"/>
    <form  suffix="ём" tag="UNKY"/>
    <form  suffix="ёт" tag="UNKY"/>
    <form  suffix="ёте" tag="UNKY"/>
    <form  suffix="ёшь" tag="UNKY"/>
  </table>
  <table name="UNK310" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="саться" tag="UNK"/>
    <form  suffix="салась" tag="UNKL"/>
    <form  suffix="сались" tag="UNKL"/>
    <form  suffix="салось" tag="UNKL"/>
    <form  suffix="сался" tag="UNKL"/>
    <form  suffix="савшись" tag="UNKR"/>
    <form  suffix="шемся" tag="UNKY"/>
    <form  suffix="шетесь" tag="UNKY"/>
    <form  suffix="шется" tag="UNKY"/>
    <form  suffix="шешься" tag="UNKY"/>
    <form  suffix="шусь" tag="UNKY"/>
    <form  suffix="шутся" tag="UNKY"/>
  </table>
  <table name="UNK311" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="саться" tag="UNK"/>
    <form  suffix="салась" tag="UNKL"/>
    <form  suffix="сались" tag="UNKL"/>
    <form  suffix="салось" tag="UNKL"/>
    <form  suffix="сался" tag="UNKL"/>
    <form  suffix="савшись" tag="UNKR"/>
    <form  suffix="шись" tag="UNKT"/>
    <form  suffix="шитесь" tag="UNKT"/>
    <form  suffix="шемся" tag="UNKY"/>
    <form  suffix="шетесь" tag="UNKY"/>
    <form  suffix="шется" tag="UNKY"/>
    <form  suffix="шешься" tag="UNKY"/>
    <form  suffix="шусь" tag="UNKY"/>
    <form  suffix="шутся" tag="UNKY"/>
  </table>
  <table name="UNK312" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="ситься" tag="UNK"/>
    <form  suffix="сись" tag="UNKB"/>
    <form  suffix="ситесь" tag="UNKB"/>
    <form  suffix="силась" tag="UNKL"/>
    <form  suffix="сились" tag="UNKL"/>
    <form  suffix="силось" tag="UNKL"/>
    <form  suffix="сился" tag="UNKL"/>
    <form  suffix="сясь" tag="UNKP"/>
    <form  suffix="симся" tag="UNKW"/>
    <form  suffix="ситесь" tag="UNKW"/>
    <form  suffix="сится" tag="UNKW"/>
    <form  suffix="сишься" tag="UNKW"/>
    <form  suffix="сятся" tag="UNKW"/>
    <form  suffix="шусь" tag="UNKW"/>
  </table>
  <table name="UNK313" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="слать" tag="UNK"/>
    <form  suffix="слал" tag="UNKL"/>
    <form  suffix="слала" tag="UNKL"/>
    <form  suffix="слали" tag="UNKL"/>
    <form  suffix="слало" tag="UNKL"/>
    <form  suffix="слав" tag="UNKR"/>
    <form  suffix="шлем" tag="UNKY"/>
    <form  suffix="шлет" tag="UNKY"/>
    <form  suffix="шлете" tag="UNKY"/>
    <form  suffix="шлешь" tag="UNKY"/>
    <form  suffix="шлю" tag="UNKY"/>
    <form  suffix="шлют" tag="UNKY"/>
    <form  suffix="шлём" tag="UNKY"/>
    <form  suffix="шлёт" tag="UNKY"/>
    <form  suffix="шлёте" tag="UNKY"/>
    <form  suffix="шлёшь" tag="UNKY"/>
  </table>
  <table name="UNK314" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="сться" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="вшись" tag="UNKR"/>
  </table>
  <table name="UNK315" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="в" tag="UNKR"/>
    <form  suffix="нем" tag="UNKV"/>
    <form  suffix="нет" tag="UNKV"/>
    <form  suffix="нете" tag="UNKV"/>
    <form  suffix="нешь" tag="UNKV"/>
    <form  suffix="ну" tag="UNKV"/>
    <form  suffix="нут" tag="UNKV"/>
  </table>
  <table name="UNK316" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="ь" tag="UNK"/>
    <form  suffix="е" tag="UNKE"/>
    <form  suffix="ем" tag="UNKE"/>
    <form  suffix="ю" tag="UNKE"/>
    <form  suffix="я" tag="UNKE"/>
    <form  suffix="ём" tag="UNKE"/>
  </table>
  <table name="UNK317" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="" tag="UNK"/>
    <form  suffix="а" tag="UNKK"/>
    <form  suffix="ам" tag="UNKK"/>
    <form  suffix="ами" tag="UNKK"/>
    <form  suffix="ах" tag="UNKK"/>
    <form  suffix="е" tag="UNKK"/>
    <form  suffix="ов" tag="UNKK"/>
    <form  suffix="ом" tag="UNKK"/>
    <form  suffix="у" tag="UNKK"/>
    <form  suffix="ы" tag="UNKK"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
  </table>
  <table name="UNK318" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="евать" tag="UNK"/>
    <form  suffix="евал" tag="UNKL"/>
    <form  suffix="евала" tag="UNKL"/>
    <form  suffix="евали" tag="UNKL"/>
    <form  suffix="евало" tag="UNKL"/>
    <form  suffix="уя" tag="UNKQ"/>
    <form  suffix="уй" tag="UNKT"/>
    <form  suffix="уйте" tag="UNKT"/>
    <form  suffix="уем" tag="UNKU"/>
    <form  suffix="ует" tag="UNKU"/>
    <form  suffix="уете" tag="UNKU"/>
    <form  suffix="уешь" tag="UNKU"/>
    <form  suffix="ую" tag="UNKU"/>
    <form  suffix="уют" tag="UNKU"/>
  </table>
  <table name="UNK319" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="ец" tag="UNK"/>
    <form  suffix="ца" tag="UNKG"/>
    <form  suffix="це" tag="UNKG"/>
    <form  suffix="цу" tag="UNKG"/>
  </table>
  <table name="UNK320" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="ечь" tag="UNK"/>
    <form  suffix="ягу" tag="UNKV"/>
    <form  suffix="ягут" tag="UNKV"/>
    <form  suffix="яжем" tag="UNKV"/>
    <form  suffix="яжет" tag="UNKV"/>
    <form  suffix="яжете" tag="UNKV"/>
    <form  suffix="яжешь" tag="UNKV"/>
  </table>
  <table name="UNK321" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="и" tag="UNKB"/>
    <form  suffix="ите" tag="UNKB"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="я" tag="UNKP"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK322" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="сть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="в" tag="UNKR"/>
    <form  suffix="дем" tag="UNKY"/>
    <form  suffix="дет" tag="UNKY"/>
    <form  suffix="дете" tag="UNKY"/>
    <form  suffix="дешь" tag="UNKY"/>
    <form  suffix="ду" tag="UNKY"/>
    <form  suffix="дут" tag="UNKY"/>
    <form  suffix="дём" tag="UNKY"/>
    <form  suffix="дёт" tag="UNKY"/>
    <form  suffix="дёте" tag="UNKY"/>
    <form  suffix="дёшь" tag="UNKY"/>
  </table>
  <table name="UNK323" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="й" tag="UNKB"/>
    <form  suffix="йте" tag="UNKB"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="в" tag="UNKR"/>
  </table>
  <table name="UNK324" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="в" tag="UNKR"/>
    <form  suffix="вем" tag="UNKV"/>
    <form  suffix="вет" tag="UNKV"/>
    <form  suffix="вете" tag="UNKV"/>
    <form  suffix="вешь" tag="UNKV"/>
    <form  suffix="ву" tag="UNKV"/>
    <form  suffix="вут" tag="UNKV"/>
    <form  suffix="вём" tag="UNKV"/>
    <form  suffix="вёт" tag="UNKV"/>
    <form  suffix="вёте" tag="UNKV"/>
    <form  suffix="вёшь" tag="UNKV"/>
  </table>
  <table name="UNK325" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="вшись" tag="UNKR"/>
    <form  suffix="емся" tag="UNKU"/>
    <form  suffix="етесь" tag="UNKU"/>
    <form  suffix="ется" tag="UNKU"/>
    <form  suffix="ешься" tag="UNKU"/>
    <form  suffix="юсь" tag="UNKU"/>
    <form  suffix="ются" tag="UNKU"/>
  </table>
  <table name="UNK326" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="хать" tag="UNK"/>
    <form  suffix="хал" tag="UNKL"/>
    <form  suffix="хала" tag="UNKL"/>
    <form  suffix="хали" tag="UNKL"/>
    <form  suffix="хало" tag="UNKL"/>
    <form  suffix="хав" tag="UNKR"/>
    <form  suffix="шем" tag="UNKY"/>
    <form  suffix="шет" tag="UNKY"/>
    <form  suffix="шете" tag="UNKY"/>
    <form  suffix="шешь" tag="UNKY"/>
    <form  suffix="шу" tag="UNKY"/>
    <form  suffix="шут" tag="UNKY"/>
  </table>
  <table name="UNK327" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="ать" tag="UNK"/>
    <form  suffix="ай" tag="UNKB"/>
    <form  suffix="айте" tag="UNKB"/>
    <form  suffix="ал" tag="UNKL"/>
    <form  suffix="ала" tag="UNKL"/>
    <form  suffix="али" tag="UNKL"/>
    <form  suffix="ало" tag="UNKL"/>
    <form  suffix="аем" tag="UNKM"/>
    <form  suffix="ает" tag="UNKM"/>
    <form  suffix="аете" tag="UNKM"/>
    <form  suffix="аешь" tag="UNKM"/>
    <form  suffix="аю" tag="UNKM"/>
    <form  suffix="ают" tag="UNKM"/>
    <form  suffix="ая" tag="UNKP"/>
    <form  suffix="ав" tag="UNKR"/>
    <form  suffix="лем" tag="UNKV"/>
    <form  suffix="лет" tag="UNKV"/>
    <form  suffix="лете" tag="UNKV"/>
    <form  suffix="лешь" tag="UNKV"/>
    <form  suffix="лю" tag="UNKV"/>
    <form  suffix="лют" tag="UNKV"/>
  </table>
  <table name="UNK328" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="деть" tag="UNK"/>
    <form  suffix="дел" tag="UNKL"/>
    <form  suffix="дела" tag="UNKL"/>
    <form  suffix="дели" tag="UNKL"/>
    <form  suffix="дело" tag="UNKL"/>
    <form  suffix="дев" tag="UNKR"/>
    <form  suffix="дим" tag="UNKY"/>
    <form  suffix="дит" tag="UNKY"/>
    <form  suffix="дите" tag="UNKY"/>
    <form  suffix="дишь" tag="UNKY"/>
    <form  suffix="дят" tag="UNKY"/>
    <form  suffix="жу" tag="UNKY"/>
  </table>
  <table name="UNK329" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="евать" tag="UNK"/>
    <form  suffix="евал" tag="UNKL"/>
    <form  suffix="евала" tag="UNKL"/>
    <form  suffix="евали" tag="UNKL"/>
    <form  suffix="евало" tag="UNKL"/>
    <form  suffix="евав" tag="UNKR"/>
    <form  suffix="юем" tag="UNKU"/>
    <form  suffix="юет" tag="UNKU"/>
    <form  suffix="юете" tag="UNKU"/>
    <form  suffix="юешь" tag="UNKU"/>
    <form  suffix="юю" tag="UNKU"/>
    <form  suffix="юют" tag="UNKU"/>
  </table>
  <table name="UNK330" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="евать" tag="UNK"/>
    <form  suffix="евал" tag="UNKL"/>
    <form  suffix="евала" tag="UNKL"/>
    <form  suffix="евали" tag="UNKL"/>
    <form  suffix="евало" tag="UNKL"/>
    <form  suffix="евав" tag="UNKR"/>
    <form  suffix="юем" tag="UNKV"/>
    <form  suffix="юет" tag="UNKV"/>
    <form  suffix="юете" tag="UNKV"/>
    <form  suffix="юешь" tag="UNKV"/>
    <form  suffix="юю" tag="UNKV"/>
    <form  suffix="юют" tag="UNKV"/>
    <form  suffix="юём" tag="UNKV"/>
    <form  suffix="юёт" tag="UNKV"/>
    <form  suffix="юёте" tag="UNKV"/>
    <form  suffix="юёшь" tag="UNKV"/>
  </table>
  <table name="UNK331" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="ести" tag="UNK"/>
    <form  suffix="ел" tag="UNKL"/>
    <form  suffix="ела" tag="UNKL"/>
    <form  suffix="ели" tag="UNKL"/>
    <form  suffix="ело" tag="UNKL"/>
    <form  suffix="ёл" tag="UNKL"/>
    <form  suffix="етем" tag="UNKY"/>
    <form  suffix="етет" tag="UNKY"/>
    <form  suffix="етете" tag="UNKY"/>
    <form  suffix="етешь" tag="UNKY"/>
    <form  suffix="ету" tag="UNKY"/>
    <form  suffix="етут" tag="UNKY"/>
    <form  suffix="етём" tag="UNKY"/>
    <form  suffix="етёт" tag="UNKY"/>
    <form  suffix="етёте" tag="UNKY"/>
    <form  suffix="етёшь" tag="UNKY"/>
  </table>
  <table name="UNK332" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="естись" tag="UNK"/>
    <form  suffix="елась" tag="UNKL"/>
    <form  suffix="елись" tag="UNKL"/>
    <form  suffix="елось" tag="UNKL"/>
    <form  suffix="елся" tag="UNKL"/>
    <form  suffix="ёлся" tag="UNKL"/>
  </table>
  <table name="UNK333" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="ю" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK334" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="атся" tag="UNKD"/>
    <form  suffix="ится" tag="UNKD"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ась" tag="UNKP"/>
  </table>
  <table name="UNK335" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ившись" tag="UNKR"/>
    <form  suffix="ься" tag="UNKT"/>
    <form  suffix="ьтесь" tag="UNKT"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK336" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="йкий" tag="UNK"/>
    <form  suffix="йкая" tag="UNKA"/>
    <form  suffix="йкие" tag="UNKA"/>
    <form  suffix="йким" tag="UNKA"/>
    <form  suffix="йкими" tag="UNKA"/>
    <form  suffix="йких" tag="UNKA"/>
    <form  suffix="йкого" tag="UNKA"/>
    <form  suffix="йкое" tag="UNKA"/>
    <form  suffix="йкой" tag="UNKA"/>
    <form  suffix="йком" tag="UNKA"/>
    <form  suffix="йкому" tag="UNKA"/>
    <form  suffix="йкою" tag="UNKA"/>
    <form  suffix="йкую" tag="UNKA"/>
    <form  suffix="ек" tag="UNKS"/>
    <form  suffix="йка" tag="UNKS"/>
    <form  suffix="йки" tag="UNKS"/>
    <form  suffix="йко" tag="UNKS"/>
  </table>
  <table name="UNK337" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="нать" tag="UNK"/>
    <form  suffix="нал" tag="UNKL"/>
    <form  suffix="нала" tag="UNKL"/>
    <form  suffix="нали" tag="UNKL"/>
    <form  suffix="нало" tag="UNKL"/>
    <form  suffix="нав" tag="UNKR"/>
    <form  suffix="оним" tag="UNKY"/>
    <form  suffix="онит" tag="UNKY"/>
    <form  suffix="оните" tag="UNKY"/>
    <form  suffix="онишь" tag="UNKY"/>
    <form  suffix="оню" tag="UNKY"/>
    <form  suffix="онят" tag="UNKY"/>
  </table>
  <table name="UNK338" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="оваться" tag="UNK"/>
    <form  suffix="овалась" tag="UNKL"/>
    <form  suffix="овались" tag="UNKL"/>
    <form  suffix="овалось" tag="UNKL"/>
    <form  suffix="овался" tag="UNKL"/>
    <form  suffix="уемся" tag="UNKU"/>
    <form  suffix="уетесь" tag="UNKU"/>
    <form  suffix="уется" tag="UNKU"/>
    <form  suffix="уешься" tag="UNKU"/>
    <form  suffix="уюсь" tag="UNKU"/>
    <form  suffix="уются" tag="UNKU"/>
  </table>
  <table name="UNK339" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="ситься" tag="UNK"/>
    <form  suffix="сись" tag="UNKB"/>
    <form  suffix="ситесь" tag="UNKB"/>
    <form  suffix="силась" tag="UNKL"/>
    <form  suffix="сились" tag="UNKL"/>
    <form  suffix="силось" tag="UNKL"/>
    <form  suffix="сился" tag="UNKL"/>
    <form  suffix="сившись" tag="UNKR"/>
    <form  suffix="симся" tag="UNKW"/>
    <form  suffix="ситесь" tag="UNKW"/>
    <form  suffix="сится" tag="UNKW"/>
    <form  suffix="сишься" tag="UNKW"/>
    <form  suffix="сятся" tag="UNKW"/>
    <form  suffix="шусь" tag="UNKW"/>
  </table>
  <table name="UNK340" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="ститься" tag="UNK"/>
    <form  suffix="стилась" tag="UNKL"/>
    <form  suffix="стились" tag="UNKL"/>
    <form  suffix="стилось" tag="UNKL"/>
    <form  suffix="стился" tag="UNKL"/>
    <form  suffix="стившись" tag="UNKR"/>
    <form  suffix="стимся" tag="UNKW"/>
    <form  suffix="ститесь" tag="UNKW"/>
    <form  suffix="стится" tag="UNKW"/>
    <form  suffix="стишься" tag="UNKW"/>
    <form  suffix="стятся" tag="UNKW"/>
    <form  suffix="щусь" tag="UNKW"/>
  </table>
  <table name="UNK341" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="таться" tag="UNK"/>
    <form  suffix="талась" tag="UNKL"/>
    <form  suffix="тались" tag="UNKL"/>
    <form  suffix="талось" tag="UNKL"/>
    <form  suffix="тался" tag="UNKL"/>
    <form  suffix="тавшись" tag="UNKR"/>
    <form  suffix="чемся" tag="UNKY"/>
    <form  suffix="четесь" tag="UNKY"/>
    <form  suffix="чется" tag="UNKY"/>
    <form  suffix="чешься" tag="UNKY"/>
    <form  suffix="чусь" tag="UNKY"/>
    <form  suffix="чутся" tag="UNKY"/>
  </table>
  <table name="UNK342" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="в" tag="UNKR"/>
    <form  suffix="ем" tag="UNKV"/>
    <form  suffix="ет" tag="UNKV"/>
    <form  suffix="ете" tag="UNKV"/>
    <form  suffix="ешь" tag="UNKV"/>
    <form  suffix="ю" tag="UNKV"/>
    <form  suffix="ют" tag="UNKV"/>
  </table>
  <table name="UNK343" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="йся" tag="UNKB"/>
    <form  suffix="йтесь" tag="UNKB"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="вшись" tag="UNKR"/>
    <form  suffix="димся" tag="UNKY"/>
    <form  suffix="дитесь" tag="UNKY"/>
    <form  suffix="дутся" tag="UNKY"/>
    <form  suffix="мся" tag="UNKY"/>
    <form  suffix="стся" tag="UNKY"/>
    <form  suffix="шься" tag="UNKY"/>
  </table>
  <table name="UNK344" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="яться" tag="UNK"/>
    <form  suffix="ялась" tag="UNKL"/>
    <form  suffix="ялись" tag="UNKL"/>
    <form  suffix="ялось" tag="UNKL"/>
    <form  suffix="ялся" tag="UNKL"/>
    <form  suffix="явшись" tag="UNKR"/>
    <form  suffix="емся" tag="UNKU"/>
    <form  suffix="етесь" tag="UNKU"/>
    <form  suffix="ется" tag="UNKU"/>
    <form  suffix="ешься" tag="UNKU"/>
    <form  suffix="юсь" tag="UNKU"/>
    <form  suffix="ются" tag="UNKU"/>
  </table>
  <table name="UNK345" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="вать" tag="UNK"/>
    <form  suffix="вал" tag="UNKL"/>
    <form  suffix="вала" tag="UNKL"/>
    <form  suffix="вали" tag="UNKL"/>
    <form  suffix="вало" tag="UNKL"/>
    <form  suffix="вая" tag="UNKP"/>
    <form  suffix="ем" tag="UNKU"/>
    <form  suffix="ет" tag="UNKU"/>
    <form  suffix="ете" tag="UNKU"/>
    <form  suffix="ешь" tag="UNKU"/>
    <form  suffix="ю" tag="UNKU"/>
    <form  suffix="ют" tag="UNKU"/>
    <form  suffix="ём" tag="UNKU"/>
    <form  suffix="ёт" tag="UNKU"/>
    <form  suffix="ёте" tag="UNKU"/>
    <form  suffix="ёшь" tag="UNKU"/>
  </table>
  <table name="UNK346" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="дить" tag="UNK"/>
    <form  suffix="ди" tag="UNKB"/>
    <form  suffix="дите" tag="UNKB"/>
    <form  suffix="дил" tag="UNKL"/>
    <form  suffix="дила" tag="UNKL"/>
    <form  suffix="дили" tag="UNKL"/>
    <form  suffix="дило" tag="UNKL"/>
    <form  suffix="дя" tag="UNKP"/>
    <form  suffix="див" tag="UNKR"/>
    <form  suffix="дим" tag="UNKW"/>
    <form  suffix="дит" tag="UNKW"/>
    <form  suffix="дите" tag="UNKW"/>
    <form  suffix="дишь" tag="UNKW"/>
    <form  suffix="дят" tag="UNKW"/>
    <form  suffix="жу" tag="UNKW"/>
  </table>
  <table name="UNK347" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="евать" tag="UNK"/>
    <form  suffix="евал" tag="UNKL"/>
    <form  suffix="евала" tag="UNKL"/>
    <form  suffix="евали" tag="UNKL"/>
    <form  suffix="евало" tag="UNKL"/>
    <form  suffix="евав" tag="UNKR"/>
    <form  suffix="уем" tag="UNKU"/>
    <form  suffix="ует" tag="UNKU"/>
    <form  suffix="уете" tag="UNKU"/>
    <form  suffix="уешь" tag="UNKU"/>
    <form  suffix="ую" tag="UNKU"/>
    <form  suffix="уют" tag="UNKU"/>
  </table>
  <table name="UNK348" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="евать" tag="UNK"/>
    <form  suffix="евал" tag="UNKL"/>
    <form  suffix="евала" tag="UNKL"/>
    <form  suffix="евали" tag="UNKL"/>
    <form  suffix="евало" tag="UNKL"/>
    <form  suffix="евав" tag="UNKR"/>
    <form  suffix="юй" tag="UNKT"/>
    <form  suffix="юйте" tag="UNKT"/>
    <form  suffix="юем" tag="UNKU"/>
    <form  suffix="юет" tag="UNKU"/>
    <form  suffix="юете" tag="UNKU"/>
    <form  suffix="юешь" tag="UNKU"/>
    <form  suffix="юю" tag="UNKU"/>
    <form  suffix="юют" tag="UNKU"/>
  </table>
  <table name="UNK349" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="евать" tag="UNK"/>
    <form  suffix="евал" tag="UNKL"/>
    <form  suffix="евала" tag="UNKL"/>
    <form  suffix="евали" tag="UNKL"/>
    <form  suffix="евало" tag="UNKL"/>
    <form  suffix="уя" tag="UNKQ"/>
    <form  suffix="уем" tag="UNKU"/>
    <form  suffix="ует" tag="UNKU"/>
    <form  suffix="уете" tag="UNKU"/>
    <form  suffix="уешь" tag="UNKU"/>
    <form  suffix="ую" tag="UNKU"/>
    <form  suffix="уют" tag="UNKU"/>
  </table>
  <table name="UNK350" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="есть" tag="UNK"/>
    <form  suffix="ел" tag="UNKL"/>
    <form  suffix="ела" tag="UNKL"/>
    <form  suffix="ели" tag="UNKL"/>
    <form  suffix="ело" tag="UNKL"/>
    <form  suffix="ев" tag="UNKR"/>
    <form  suffix="ядем" tag="UNKY"/>
    <form  suffix="ядет" tag="UNKY"/>
    <form  suffix="ядете" tag="UNKY"/>
    <form  suffix="ядешь" tag="UNKY"/>
    <form  suffix="яду" tag="UNKY"/>
    <form  suffix="ядут" tag="UNKY"/>
  </table>
  <table name="UNK351" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="йный" tag="UNK"/>
    <form  suffix="йная" tag="UNKA"/>
    <form  suffix="йного" tag="UNKA"/>
    <form  suffix="йное" tag="UNKA"/>
    <form  suffix="йной" tag="UNKA"/>
    <form  suffix="йном" tag="UNKA"/>
    <form  suffix="йному" tag="UNKA"/>
    <form  suffix="йною" tag="UNKA"/>
    <form  suffix="йную" tag="UNKA"/>
    <form  suffix="йные" tag="UNKA"/>
    <form  suffix="йным" tag="UNKA"/>
    <form  suffix="йными" tag="UNKA"/>
    <form  suffix="йных" tag="UNKA"/>
    <form  suffix="йнее" tag="UNKE"/>
    <form  suffix="йней" tag="UNKE"/>
    <form  suffix="ен" tag="UNKS"/>
    <form  suffix="йна" tag="UNKS"/>
    <form  suffix="йно" tag="UNKS"/>
    <form  suffix="йны" tag="UNKS"/>
  </table>
  <table name="UNK352" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="ный" tag="UNK"/>
    <form  suffix="ная" tag="UNKA"/>
    <form  suffix="ного" tag="UNKA"/>
    <form  suffix="ное" tag="UNKA"/>
    <form  suffix="ной" tag="UNKA"/>
    <form  suffix="ном" tag="UNKA"/>
    <form  suffix="ному" tag="UNKA"/>
    <form  suffix="ною" tag="UNKA"/>
    <form  suffix="ную" tag="UNKA"/>
    <form  suffix="ные" tag="UNKA"/>
    <form  suffix="ным" tag="UNKA"/>
    <form  suffix="ными" tag="UNKA"/>
    <form  suffix="ных" tag="UNKA"/>
    <form  suffix="нее" tag="UNKE"/>
    <form  suffix="ней" tag="UNKE"/>
    <form  suffix="" tag="UNKS"/>
    <form  suffix="а" tag="UNKS"/>
    <form  suffix="о" tag="UNKS"/>
    <form  suffix="ы" tag="UNKS"/>
  </table>
  <table name="UNK353" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="ой" tag="UNK"/>
    <form  suffix="ая" tag="UNKA"/>
    <form  suffix="ие" tag="UNKA"/>
    <form  suffix="им" tag="UNKA"/>
    <form  suffix="ими" tag="UNKA"/>
    <form  suffix="их" tag="UNKA"/>
    <form  suffix="ого" tag="UNKA"/>
    <form  suffix="ое" tag="UNKA"/>
    <form  suffix="ом" tag="UNKA"/>
    <form  suffix="ому" tag="UNKA"/>
    <form  suffix="ою" tag="UNKA"/>
    <form  suffix="ую" tag="UNKA"/>
    <form  suffix="" tag="UNKS"/>
    <form  suffix="а" tag="UNKS"/>
    <form  suffix="и" tag="UNKS"/>
    <form  suffix="о" tag="UNKS"/>
  </table>
  <table name="UNK354" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="вать" tag="UNK"/>
    <form  suffix="вал" tag="UNKL"/>
    <form  suffix="вала" tag="UNKL"/>
    <form  suffix="вали" tag="UNKL"/>
    <form  suffix="вало" tag="UNKL"/>
    <form  suffix="вав" tag="UNKR"/>
    <form  suffix="овем" tag="UNKY"/>
    <form  suffix="овет" tag="UNKY"/>
    <form  suffix="овете" tag="UNKY"/>
    <form  suffix="овешь" tag="UNKY"/>
    <form  suffix="ову" tag="UNKY"/>
    <form  suffix="овут" tag="UNKY"/>
    <form  suffix="овём" tag="UNKY"/>
    <form  suffix="овёт" tag="UNKY"/>
    <form  suffix="овёте" tag="UNKY"/>
    <form  suffix="овёшь" tag="UNKY"/>
  </table>
  <table name="UNK355" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="деться" tag="UNK"/>
    <form  suffix="делась" tag="UNKL"/>
    <form  suffix="делись" tag="UNKL"/>
    <form  suffix="делось" tag="UNKL"/>
    <form  suffix="делся" tag="UNKL"/>
    <form  suffix="девшись" tag="UNKR"/>
    <form  suffix="дись" tag="UNKT"/>
    <form  suffix="дитесь" tag="UNKT"/>
    <form  suffix="димся" tag="UNKY"/>
    <form  suffix="дитесь" tag="UNKY"/>
    <form  suffix="дится" tag="UNKY"/>
    <form  suffix="дишься" tag="UNKY"/>
    <form  suffix="дятся" tag="UNKY"/>
    <form  suffix="жусь" tag="UNKY"/>
  </table>
  <table name="UNK356" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="диться" tag="UNK"/>
    <form  suffix="дилась" tag="UNKL"/>
    <form  suffix="дились" tag="UNKL"/>
    <form  suffix="дилось" tag="UNKL"/>
    <form  suffix="дился" tag="UNKL"/>
    <form  suffix="дясь" tag="UNKP"/>
    <form  suffix="димся" tag="UNKW"/>
    <form  suffix="дитесь" tag="UNKW"/>
    <form  suffix="дится" tag="UNKW"/>
    <form  suffix="дишься" tag="UNKW"/>
    <form  suffix="дятся" tag="UNKW"/>
    <form  suffix="жусь" tag="UNKW"/>
  </table>
  <table name="UNK357" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="евать" tag="UNK"/>
    <form  suffix="евал" tag="UNKL"/>
    <form  suffix="евала" tag="UNKL"/>
    <form  suffix="евали" tag="UNKL"/>
    <form  suffix="евало" tag="UNKL"/>
    <form  suffix="евав" tag="UNKR"/>
    <form  suffix="уй" tag="UNKT"/>
    <form  suffix="уйте" tag="UNKT"/>
    <form  suffix="уем" tag="UNKV"/>
    <form  suffix="ует" tag="UNKV"/>
    <form  suffix="уете" tag="UNKV"/>
    <form  suffix="уешь" tag="UNKV"/>
    <form  suffix="ую" tag="UNKV"/>
    <form  suffix="уют" tag="UNKV"/>
    <form  suffix="уём" tag="UNKV"/>
    <form  suffix="уёт" tag="UNKV"/>
    <form  suffix="уёте" tag="UNKV"/>
    <form  suffix="уёшь" tag="UNKV"/>
  </table>
  <table name="UNK358" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="еть" tag="UNK"/>
    <form  suffix="ел" tag="UNKL"/>
    <form  suffix="ела" tag="UNKL"/>
    <form  suffix="ели" tag="UNKL"/>
    <form  suffix="ело" tag="UNKL"/>
    <form  suffix="и" tag="UNKT"/>
    <form  suffix="ите" tag="UNKT"/>
    <form  suffix="им" tag="UNKY"/>
    <form  suffix="ит" tag="UNKY"/>
    <form  suffix="ите" tag="UNKY"/>
    <form  suffix="ишь" tag="UNKY"/>
    <form  suffix="лю" tag="UNKY"/>
    <form  suffix="ят" tag="UNKY"/>
  </table>
  <table name="UNK359" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="ечься" tag="UNK"/>
    <form  suffix="еклась" tag="UNKL"/>
    <form  suffix="еклись" tag="UNKL"/>
    <form  suffix="еклось" tag="UNKL"/>
    <form  suffix="екся" tag="UNKL"/>
    <form  suffix="ёкся" tag="UNKL"/>
  </table>
  <table name="UNK360" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="зиться" tag="UNK"/>
    <form  suffix="зилась" tag="UNKL"/>
    <form  suffix="зились" tag="UNKL"/>
    <form  suffix="зилось" tag="UNKL"/>
    <form  suffix="зился" tag="UNKL"/>
    <form  suffix="зившись" tag="UNKR"/>
    <form  suffix="жусь" tag="UNKW"/>
    <form  suffix="зимся" tag="UNKW"/>
    <form  suffix="зитесь" tag="UNKW"/>
    <form  suffix="зится" tag="UNKW"/>
    <form  suffix="зишься" tag="UNKW"/>
    <form  suffix="зятся" tag="UNKW"/>
  </table>
  <table name="UNK361" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="ат" tag="UNKW"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="у" tag="UNKW"/>
  </table>
  <table name="UNK362" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="ись" tag="UNKB"/>
    <form  suffix="итесь" tag="UNKB"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ясь" tag="UNKP"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK363" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="овать" tag="UNK"/>
    <form  suffix="овал" tag="UNKL"/>
    <form  suffix="овала" tag="UNKL"/>
    <form  suffix="овали" tag="UNKL"/>
    <form  suffix="овало" tag="UNKL"/>
    <form  suffix="овав" tag="UNKR"/>
    <form  suffix="уй" tag="UNKT"/>
    <form  suffix="уйте" tag="UNKT"/>
    <form  suffix="уем" tag="UNKV"/>
    <form  suffix="ует" tag="UNKV"/>
    <form  suffix="уете" tag="UNKV"/>
    <form  suffix="уешь" tag="UNKV"/>
    <form  suffix="ую" tag="UNKV"/>
    <form  suffix="уют" tag="UNKV"/>
    <form  suffix="уём" tag="UNKV"/>
    <form  suffix="уёт" tag="UNKV"/>
    <form  suffix="уёте" tag="UNKV"/>
    <form  suffix="уёшь" tag="UNKV"/>
  </table>
  <table name="UNK364" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="скать" tag="UNK"/>
    <form  suffix="скай" tag="UNKB"/>
    <form  suffix="скайте" tag="UNKB"/>
    <form  suffix="скал" tag="UNKL"/>
    <form  suffix="скала" tag="UNKL"/>
    <form  suffix="скали" tag="UNKL"/>
    <form  suffix="скало" tag="UNKL"/>
    <form  suffix="скаем" tag="UNKM"/>
    <form  suffix="скает" tag="UNKM"/>
    <form  suffix="скаете" tag="UNKM"/>
    <form  suffix="скаешь" tag="UNKM"/>
    <form  suffix="скаю" tag="UNKM"/>
    <form  suffix="скают" tag="UNKM"/>
    <form  suffix="скав" tag="UNKR"/>
    <form  suffix="щем" tag="UNKY"/>
    <form  suffix="щет" tag="UNKY"/>
    <form  suffix="щете" tag="UNKY"/>
    <form  suffix="щешь" tag="UNKY"/>
    <form  suffix="щу" tag="UNKY"/>
    <form  suffix="щут" tag="UNKY"/>
  </table>
  <table name="UNK365" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="стить" tag="UNK"/>
    <form  suffix="сти" tag="UNKB"/>
    <form  suffix="стите" tag="UNKB"/>
    <form  suffix="стил" tag="UNKL"/>
    <form  suffix="стила" tag="UNKL"/>
    <form  suffix="стили" tag="UNKL"/>
    <form  suffix="стило" tag="UNKL"/>
    <form  suffix="стя" tag="UNKP"/>
    <form  suffix="стим" tag="UNKW"/>
    <form  suffix="стит" tag="UNKW"/>
    <form  suffix="стите" tag="UNKW"/>
    <form  suffix="стишь" tag="UNKW"/>
    <form  suffix="стят" tag="UNKW"/>
    <form  suffix="щу" tag="UNKW"/>
  </table>
  <table name="UNK366" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="вшись" tag="UNKR"/>
    <form  suffix="вемся" tag="UNKV"/>
    <form  suffix="ветесь" tag="UNKV"/>
    <form  suffix="вется" tag="UNKV"/>
    <form  suffix="вешься" tag="UNKV"/>
    <form  suffix="вусь" tag="UNKV"/>
    <form  suffix="вутся" tag="UNKV"/>
    <form  suffix="вёмся" tag="UNKV"/>
    <form  suffix="вётесь" tag="UNKV"/>
    <form  suffix="вётся" tag="UNKV"/>
    <form  suffix="вёшься" tag="UNKV"/>
  </table>
  <table name="UNK367" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="ыть" tag="UNK"/>
    <form  suffix="ыл" tag="UNKL"/>
    <form  suffix="ыла" tag="UNKL"/>
    <form  suffix="ыли" tag="UNKL"/>
    <form  suffix="ыло" tag="UNKL"/>
    <form  suffix="оем" tag="UNKM"/>
    <form  suffix="оет" tag="UNKM"/>
    <form  suffix="оете" tag="UNKM"/>
    <form  suffix="оешь" tag="UNKM"/>
    <form  suffix="ою" tag="UNKM"/>
    <form  suffix="оют" tag="UNKM"/>
    <form  suffix="ыв" tag="UNKR"/>
  </table>
  <table name="UNK368" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="ваться" tag="UNK"/>
    <form  suffix="валась" tag="UNKL"/>
    <form  suffix="вались" tag="UNKL"/>
    <form  suffix="валось" tag="UNKL"/>
    <form  suffix="вался" tag="UNKL"/>
    <form  suffix="ваясь" tag="UNKP"/>
    <form  suffix="емся" tag="UNKU"/>
    <form  suffix="етесь" tag="UNKU"/>
    <form  suffix="ется" tag="UNKU"/>
    <form  suffix="ешься" tag="UNKU"/>
    <form  suffix="юсь" tag="UNKU"/>
    <form  suffix="ются" tag="UNKU"/>
    <form  suffix="ёмся" tag="UNKU"/>
    <form  suffix="ётесь" tag="UNKU"/>
    <form  suffix="ётся" tag="UNKU"/>
    <form  suffix="ёшься" tag="UNKU"/>
  </table>
  <table name="UNK369" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="деть" tag="UNK"/>
    <form  suffix="дел" tag="UNKL"/>
    <form  suffix="дела" tag="UNKL"/>
    <form  suffix="дели" tag="UNKL"/>
    <form  suffix="дело" tag="UNKL"/>
    <form  suffix="ди" tag="UNKT"/>
    <form  suffix="дите" tag="UNKT"/>
    <form  suffix="дим" tag="UNKY"/>
    <form  suffix="дит" tag="UNKY"/>
    <form  suffix="дите" tag="UNKY"/>
    <form  suffix="дишь" tag="UNKY"/>
    <form  suffix="дят" tag="UNKY"/>
    <form  suffix="жу" tag="UNKY"/>
  </table>
  <table name="UNK370" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="деться" tag="UNK"/>
    <form  suffix="делась" tag="UNKL"/>
    <form  suffix="делись" tag="UNKL"/>
    <form  suffix="делось" tag="UNKL"/>
    <form  suffix="делся" tag="UNKL"/>
    <form  suffix="девшись" tag="UNKR"/>
    <form  suffix="димся" tag="UNKY"/>
    <form  suffix="дитесь" tag="UNKY"/>
    <form  suffix="дится" tag="UNKY"/>
    <form  suffix="дишься" tag="UNKY"/>
    <form  suffix="дятся" tag="UNKY"/>
    <form  suffix="жусь" tag="UNKY"/>
  </table>
  <table name="UNK371" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="еться" tag="UNK"/>
    <form  suffix="елась" tag="UNKL"/>
    <form  suffix="елись" tag="UNKL"/>
    <form  suffix="елось" tag="UNKL"/>
    <form  suffix="елся" tag="UNKL"/>
    <form  suffix="евшись" tag="UNKR"/>
    <form  suffix="ись" tag="UNKT"/>
    <form  suffix="итесь" tag="UNKT"/>
    <form  suffix="имся" tag="UNKY"/>
    <form  suffix="итесь" tag="UNKY"/>
    <form  suffix="ится" tag="UNKY"/>
    <form  suffix="ишься" tag="UNKY"/>
    <form  suffix="юсь" tag="UNKY"/>
    <form  suffix="ятся" tag="UNKY"/>
  </table>
  <table name="UNK372" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="зать" tag="UNK"/>
    <form  suffix="зал" tag="UNKL"/>
    <form  suffix="зала" tag="UNKL"/>
    <form  suffix="зали" tag="UNKL"/>
    <form  suffix="зало" tag="UNKL"/>
    <form  suffix="жем" tag="UNKY"/>
    <form  suffix="жет" tag="UNKY"/>
    <form  suffix="жете" tag="UNKY"/>
    <form  suffix="жешь" tag="UNKY"/>
    <form  suffix="жу" tag="UNKY"/>
    <form  suffix="жут" tag="UNKY"/>
  </table>
  <table name="UNK373" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="зиться" tag="UNK"/>
    <form  suffix="зилась" tag="UNKL"/>
    <form  suffix="зились" tag="UNKL"/>
    <form  suffix="зилось" tag="UNKL"/>
    <form  suffix="зился" tag="UNKL"/>
    <form  suffix="зившись" tag="UNKR"/>
    <form  suffix="зься" tag="UNKT"/>
    <form  suffix="зьтесь" tag="UNKT"/>
    <form  suffix="жусь" tag="UNKW"/>
    <form  suffix="зимся" tag="UNKW"/>
    <form  suffix="зитесь" tag="UNKW"/>
    <form  suffix="зится" tag="UNKW"/>
    <form  suffix="зишься" tag="UNKW"/>
    <form  suffix="зятся" tag="UNKW"/>
  </table>
  <table name="UNK374" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="лю" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK375" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ясь" tag="UNKP"/>
    <form  suffix="ься" tag="UNKT"/>
    <form  suffix="ьтесь" tag="UNKT"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="люсь" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK376" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="оваться" tag="UNK"/>
    <form  suffix="овалась" tag="UNKL"/>
    <form  suffix="овались" tag="UNKL"/>
    <form  suffix="овалось" tag="UNKL"/>
    <form  suffix="овался" tag="UNKL"/>
    <form  suffix="уясь" tag="UNKQ"/>
    <form  suffix="овавшись" tag="UNKR"/>
    <form  suffix="уемся" tag="UNKU"/>
    <form  suffix="уетесь" tag="UNKU"/>
    <form  suffix="уется" tag="UNKU"/>
    <form  suffix="уешься" tag="UNKU"/>
    <form  suffix="уюсь" tag="UNKU"/>
    <form  suffix="уются" tag="UNKU"/>
  </table>
  <table name="UNK377" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="оть" tag="UNK"/>
    <form  suffix="те" tag="UNKO"/>
    <form  suffix="тей" tag="UNKO"/>
    <form  suffix="тем" tag="UNKO"/>
    <form  suffix="ти" tag="UNKO"/>
    <form  suffix="тю" tag="UNKO"/>
    <form  suffix="тя" tag="UNKO"/>
    <form  suffix="тям" tag="UNKO"/>
    <form  suffix="тями" tag="UNKO"/>
    <form  suffix="тях" tag="UNKO"/>
  </table>
  <table name="UNK378" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="сть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
  </table>
  <table name="UNK379" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="уться" tag="UNK"/>
    <form  suffix="емся" tag="UNKM"/>
    <form  suffix="етесь" tag="UNKM"/>
    <form  suffix="ется" tag="UNKM"/>
    <form  suffix="ешься" tag="UNKM"/>
    <form  suffix="усь" tag="UNKM"/>
    <form  suffix="утся" tag="UNKM"/>
    <form  suffix="увшись" tag="UNKR"/>
  </table>
  <table name="UNK380" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="чь" tag="UNK"/>
    <form  suffix="гу" tag="UNKY"/>
    <form  suffix="гут" tag="UNKY"/>
    <form  suffix="жем" tag="UNKY"/>
    <form  suffix="жет" tag="UNKY"/>
    <form  suffix="жете" tag="UNKY"/>
    <form  suffix="жешь" tag="UNKY"/>
  </table>
  <table name="UNK381" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="ённый" tag="UNK"/>
    <form  suffix="ённая" tag="UNKA"/>
    <form  suffix="ённого" tag="UNKA"/>
    <form  suffix="ённое" tag="UNKA"/>
    <form  suffix="ённой" tag="UNKA"/>
    <form  suffix="ённом" tag="UNKA"/>
    <form  suffix="ённому" tag="UNKA"/>
    <form  suffix="ённою" tag="UNKA"/>
    <form  suffix="ённую" tag="UNKA"/>
    <form  suffix="ённые" tag="UNKA"/>
    <form  suffix="ённым" tag="UNKA"/>
    <form  suffix="ёнными" tag="UNKA"/>
    <form  suffix="ённых" tag="UNKA"/>
    <form  suffix="ённее" tag="UNKE"/>
    <form  suffix="ённей" tag="UNKE"/>
    <form  suffix="ена" tag="UNKS"/>
    <form  suffix="ено" tag="UNKS"/>
    <form  suffix="ены" tag="UNKS"/>
    <form  suffix="ён" tag="UNKS"/>
    <form  suffix="ёна" tag="UNKS"/>
    <form  suffix="ёно" tag="UNKS"/>
    <form  suffix="ёны" tag="UNKS"/>
  </table>
  <table name="UNK382" rads=".*(пух|пах|спас|смог|провис|настриг)" fast="-">
    <!-- 6 members: спас ; смог ; пух ; пах ; провис ; настриг -->
    <form  suffix="" tag="UNK"/>
    <form  suffix="а" tag="UNKJ"/>
    <form  suffix="е" tag="UNKJ"/>
    <form  suffix="ом" tag="UNKJ"/>
    <form  suffix="у" tag="UNKJ"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
  </table>
  <table name="UNK383" rads=".*(общи|отре|истре|растре|полузасы)п" fast="-">
    <!-- 6 members: потрепать ; общипать ; полузасыпать ; истрепать ; отрепать ; растрепать -->
    <form  suffix="ать" tag="UNK"/>
    <form  suffix="ал" tag="UNKL"/>
    <form  suffix="ала" tag="UNKL"/>
    <form  suffix="али" tag="UNKL"/>
    <form  suffix="ало" tag="UNKL"/>
    <form  suffix="ав" tag="UNKR"/>
    <form  suffix="лем" tag="UNKV"/>
    <form  suffix="лет" tag="UNKV"/>
    <form  suffix="лете" tag="UNKV"/>
    <form  suffix="лешь" tag="UNKV"/>
    <form  suffix="лю" tag="UNKV"/>
    <form  suffix="лют" tag="UNKV"/>
  </table>
  <table name="UNK384" rads=".*(у|о|от|при)сып" fast="-">
    <!-- 6 members: усыпать ; отсыпать ; недосыпать ; осыпать ; просыпать ; присыпать -->
    <form  suffix="ать" tag="UNK"/>
    <form  suffix="ал" tag="UNKL"/>
    <form  suffix="ала" tag="UNKL"/>
    <form  suffix="али" tag="UNKL"/>
    <form  suffix="ало" tag="UNKL"/>
    <form  suffix="аем" tag="UNKM"/>
    <form  suffix="ает" tag="UNKM"/>
    <form  suffix="аете" tag="UNKM"/>
    <form  suffix="аешь" tag="UNKM"/>
    <form  suffix="аю" tag="UNKM"/>
    <form  suffix="ают" tag="UNKM"/>
    <form  suffix="ая" tag="UNKP"/>
    <form  suffix="ав" tag="UNKR"/>
    <form  suffix="лем" tag="UNKV"/>
    <form  suffix="лет" tag="UNKV"/>
    <form  suffix="лете" tag="UNKV"/>
    <form  suffix="лешь" tag="UNKV"/>
    <form  suffix="лю" tag="UNKV"/>
    <form  suffix="лют" tag="UNKV"/>
  </table>
  <table name="UNK385" rads=".*(у|о|с|за|об)сып" fast="-">
    <!-- 6 members: усыпаться ; засыпаться ; осыпаться ; посыпаться ; ссыпаться ; обсыпаться -->
    <form  suffix="аться" tag="UNK"/>
    <form  suffix="алась" tag="UNKL"/>
    <form  suffix="ались" tag="UNKL"/>
    <form  suffix="алось" tag="UNKL"/>
    <form  suffix="ался" tag="UNKL"/>
    <form  suffix="аемся" tag="UNKM"/>
    <form  suffix="аетесь" tag="UNKM"/>
    <form  suffix="ается" tag="UNKM"/>
    <form  suffix="аешься" tag="UNKM"/>
    <form  suffix="аюсь" tag="UNKM"/>
    <form  suffix="аются" tag="UNKM"/>
    <form  suffix="аясь" tag="UNKP"/>
    <form  suffix="авшись" tag="UNKR"/>
    <form  suffix="лемся" tag="UNKV"/>
    <form  suffix="летесь" tag="UNKV"/>
    <form  suffix="лется" tag="UNKV"/>
    <form  suffix="лешься" tag="UNKV"/>
    <form  suffix="люсь" tag="UNKV"/>
    <form  suffix="лются" tag="UNKV"/>
  </table>
  <table name="UNK386" rads=".*(ви|смер|полуси)" fast="-">
    <!-- 6 members: предвидеть ; смердеть ; полусидеть ; невзвидеть ; ненавидеть ; видеть -->
    <form  suffix="деть" tag="UNK"/>
    <form  suffix="дел" tag="UNKL"/>
    <form  suffix="дела" tag="UNKL"/>
    <form  suffix="дели" tag="UNKL"/>
    <form  suffix="дело" tag="UNKL"/>
    <form  suffix="дим" tag="UNKY"/>
    <form  suffix="дит" tag="UNKY"/>
    <form  suffix="дите" tag="UNKY"/>
    <form  suffix="дишь" tag="UNKY"/>
    <form  suffix="дят" tag="UNKY"/>
    <form  suffix="жу" tag="UNKY"/>
  </table>
  <table name="UNK387" rads=".*(за|из|по|раз|пере)в" fast="-">
    <!-- 6 members: завестись ; перевестись ; известись ; повестись ; развестись ; обзавестись -->
    <form  suffix="естись" tag="UNK"/>
    <form  suffix="елась" tag="UNKL"/>
    <form  suffix="елись" tag="UNKL"/>
    <form  suffix="елось" tag="UNKL"/>
    <form  suffix="елся" tag="UNKL"/>
    <form  suffix="ёлся" tag="UNKL"/>
    <form  suffix="едемся" tag="UNKY"/>
    <form  suffix="едетесь" tag="UNKY"/>
    <form  suffix="едется" tag="UNKY"/>
    <form  suffix="едешься" tag="UNKY"/>
    <form  suffix="едусь" tag="UNKY"/>
    <form  suffix="едутся" tag="UNKY"/>
    <form  suffix="едёмся" tag="UNKY"/>
    <form  suffix="едётесь" tag="UNKY"/>
    <form  suffix="едётся" tag="UNKY"/>
    <form  suffix="едёшься" tag="UNKY"/>
  </table>
  <table name="UNK388" rads=".*пл" fast="-">
    <!-- 6 members: сплестись ; переплестись ; плестись ; доплестись ; поплестись ; приплестись -->
    <form  suffix="естись" tag="UNK"/>
    <form  suffix="елась" tag="UNKL"/>
    <form  suffix="елись" tag="UNKL"/>
    <form  suffix="елось" tag="UNKL"/>
    <form  suffix="елся" tag="UNKL"/>
    <form  suffix="ёлся" tag="UNKL"/>
    <form  suffix="етемся" tag="UNKY"/>
    <form  suffix="ететесь" tag="UNKY"/>
    <form  suffix="етется" tag="UNKY"/>
    <form  suffix="етешься" tag="UNKY"/>
    <form  suffix="етусь" tag="UNKY"/>
    <form  suffix="етутся" tag="UNKY"/>
    <form  suffix="етёмся" tag="UNKY"/>
    <form  suffix="етётесь" tag="UNKY"/>
    <form  suffix="етётся" tag="UNKY"/>
    <form  suffix="етёшься" tag="UNKY"/>
  </table>
  <table name="UNK389" rads=".*(у|за|про|пере|предпо)ч" fast="-">
    <!-- 6 members: предпочесть ; перечесть ; переучесть ; зачесть ; учесть ; прочесть -->
    <form  suffix="есть" tag="UNK"/>
    <form  suffix="ел" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="ёл" tag="UNKL"/>
    <form  suffix="тем" tag="UNKY"/>
    <form  suffix="тет" tag="UNKY"/>
    <form  suffix="тете" tag="UNKY"/>
    <form  suffix="тешь" tag="UNKY"/>
    <form  suffix="ту" tag="UNKY"/>
    <form  suffix="тут" tag="UNKY"/>
    <form  suffix="тём" tag="UNKY"/>
    <form  suffix="тёт" tag="UNKY"/>
    <form  suffix="тёте" tag="UNKY"/>
    <form  suffix="тёшь" tag="UNKY"/>
  </table>
  <table name="UNK390" rads=".*(вел|взрев|порев|зарев|прорев)" fast="-">
    <!-- 6 members: прореветь ; взреветь ; повелеть ; велеть ; пореветь ; зареветь -->
    <form  suffix="еть" tag="UNK"/>
    <form  suffix="ел" tag="UNKL"/>
    <form  suffix="ела" tag="UNKL"/>
    <form  suffix="ели" tag="UNKL"/>
    <form  suffix="ело" tag="UNKL"/>
    <form  suffix="ев" tag="UNKR"/>
    <form  suffix="и" tag="UNKT"/>
    <form  suffix="ите" tag="UNKT"/>
  </table>
  <table name="UNK391" rads=".*(обоз|пого|през|прого|подзаго|недосмот)р" fast="-">
    <!-- 6 members: обозреть ; подзагореть ; недосмотреть ; прогореть ; погореть ; презреть -->
    <form  suffix="еть" tag="UNK"/>
    <form  suffix="ел" tag="UNKL"/>
    <form  suffix="ела" tag="UNKL"/>
    <form  suffix="ели" tag="UNKL"/>
    <form  suffix="ело" tag="UNKL"/>
    <form  suffix="ев" tag="UNKR"/>
    <form  suffix="им" tag="UNKY"/>
    <form  suffix="ит" tag="UNKY"/>
    <form  suffix="ите" tag="UNKY"/>
    <form  suffix="ишь" tag="UNKY"/>
    <form  suffix="ю" tag="UNKY"/>
    <form  suffix="ят" tag="UNKY"/>
  </table>
  <table name="UNK392" rads=".*(ка|ре|ли|ма|развя)" fast="-">
    <!-- 6 members: казаться ; резаться ; смазаться ; развязаться ; лизаться ; мазаться -->
    <form  suffix="заться" tag="UNK"/>
    <form  suffix="залась" tag="UNKL"/>
    <form  suffix="зались" tag="UNKL"/>
    <form  suffix="залось" tag="UNKL"/>
    <form  suffix="зался" tag="UNKL"/>
    <form  suffix="жемся" tag="UNKY"/>
    <form  suffix="жетесь" tag="UNKY"/>
    <form  suffix="жется" tag="UNKY"/>
    <form  suffix="жешься" tag="UNKY"/>
    <form  suffix="жусь" tag="UNKY"/>
    <form  suffix="жутся" tag="UNKY"/>
  </table>
  <table name="UNK393" rads=".*(ла|гре|ело|моро|конфу)" fast="-">
    <!-- 6 members: грезить ; елозить ; лазить ; морозить ; влазить ; конфузить -->
    <form  suffix="зить" tag="UNK"/>
    <form  suffix="зил" tag="UNKL"/>
    <form  suffix="зила" tag="UNKL"/>
    <form  suffix="зили" tag="UNKL"/>
    <form  suffix="зило" tag="UNKL"/>
    <form  suffix="зя" tag="UNKP"/>
    <form  suffix="зь" tag="UNKT"/>
    <form  suffix="зьте" tag="UNKT"/>
    <form  suffix="жу" tag="UNKW"/>
    <form  suffix="зим" tag="UNKW"/>
    <form  suffix="зит" tag="UNKW"/>
    <form  suffix="зите" tag="UNKW"/>
    <form  suffix="зишь" tag="UNKW"/>
    <form  suffix="зят" tag="UNKW"/>
  </table>
  <table name="UNK394" rads=".*(Жуков|Вернад|Чайков|Достоев|Лобачев|Остроград)ск" fast="-">
    <!-- 6 members: Вернадский ; Чайковский ; Достоевский ; Лобачевский ; Жуковский ; Остроградский -->
    <form  suffix="ий" tag="UNK"/>
    <form  suffix="им" tag="UNKG"/>
    <form  suffix="ого" tag="UNKG"/>
    <form  suffix="ом" tag="UNKG"/>
    <form  suffix="ому" tag="UNKG"/>
  </table>
  <table name="UNK395" rads=".*(мор|умал|смер|стесн|забран|запоздн)" fast="-">
    <!-- 6 members: умалиться ; смериться ; запоздниться ; мориться ; забраниться ; стесниться -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="юсь" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK396" rads=".*(бо|тро|дво|кофе)" fast="-">
    <!-- 6 members: маслобойня ; тройня ; кофейня ; двойня ; скотобойня ; бойня -->
    <form  suffix="йня" tag="UNK"/>
    <form  suffix="ен" tag="UNKI"/>
    <form  suffix="йне" tag="UNKI"/>
    <form  suffix="йней" tag="UNKI"/>
    <form  suffix="йнею" tag="UNKI"/>
    <form  suffix="йни" tag="UNKI"/>
    <form  suffix="йню" tag="UNKI"/>
    <form  suffix="йням" tag="UNKI"/>
    <form  suffix="йнями" tag="UNKI"/>
    <form  suffix="йнях" tag="UNKI"/>
  </table>
  <table name="UNK397" rads=".*(напла|испла|попла|выпла|распла|расхны)" fast="-">
    <!-- 6 members: расплакаться ; расхныкаться ; наплакаться ; исплакаться ; поплакаться ; выплакаться -->
    <form  suffix="каться" tag="UNK"/>
    <form  suffix="калась" tag="UNKL"/>
    <form  suffix="кались" tag="UNKL"/>
    <form  suffix="калось" tag="UNKL"/>
    <form  suffix="кался" tag="UNKL"/>
    <form  suffix="кавшись" tag="UNKR"/>
    <form  suffix="чемся" tag="UNKY"/>
    <form  suffix="четесь" tag="UNKY"/>
    <form  suffix="чется" tag="UNKY"/>
    <form  suffix="чешься" tag="UNKY"/>
    <form  suffix="чусь" tag="UNKY"/>
    <form  suffix="чутся" tag="UNKY"/>
  </table>
  <table name="UNK398" rads=".*(обосн|обрис|исполос|изнасил|цивилиз|инвертир)" fast="-">
    <!-- 6 members: обосновать ; инвертировать ; исполосовать ; изнасиловать ; цивилизовать ; обрисовать -->
    <form  suffix="овать" tag="UNK"/>
    <form  suffix="овал" tag="UNKL"/>
    <form  suffix="овала" tag="UNKL"/>
    <form  suffix="овали" tag="UNKL"/>
    <form  suffix="овало" tag="UNKL"/>
    <form  suffix="уй" tag="UNKT"/>
    <form  suffix="уйте" tag="UNKT"/>
    <form  suffix="уем" tag="UNKU"/>
    <form  suffix="ует" tag="UNKU"/>
    <form  suffix="уете" tag="UNKU"/>
    <form  suffix="уешь" tag="UNKU"/>
    <form  suffix="ую" tag="UNKU"/>
    <form  suffix="уют" tag="UNKU"/>
  </table>
  <table name="UNK399" rads=".*(пор|вкол|накол|выкол|прокол)" fast="-">
    <!-- 6 members: выпороть ; проколоть ; наколоть ; вколоть ; выколоть ; пороть -->
    <form  suffix="оть" tag="UNK"/>
    <form  suffix="и" tag="UNKB"/>
    <form  suffix="ите" tag="UNKB"/>
    <form  suffix="ол" tag="UNKL"/>
    <form  suffix="ола" tag="UNKL"/>
    <form  suffix="оли" tag="UNKL"/>
    <form  suffix="оло" tag="UNKL"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="ю" tag="UNKM"/>
    <form  suffix="ют" tag="UNKM"/>
  </table>
  <table name="UNK400" rads=".*(укол|побор|откол|закол|напор|раскол)" fast="-">
    <!-- 6 members: уколоться ; побороться ; расколоться ; отколоться ; заколоться ; напороться -->
    <form  suffix="оться" tag="UNK"/>
    <form  suffix="ись" tag="UNKB"/>
    <form  suffix="итесь" tag="UNKB"/>
    <form  suffix="олась" tag="UNKL"/>
    <form  suffix="олись" tag="UNKL"/>
    <form  suffix="олось" tag="UNKL"/>
    <form  suffix="олся" tag="UNKL"/>
    <form  suffix="емся" tag="UNKM"/>
    <form  suffix="етесь" tag="UNKM"/>
    <form  suffix="ется" tag="UNKM"/>
    <form  suffix="ешься" tag="UNKM"/>
    <form  suffix="юсь" tag="UNKM"/>
    <form  suffix="ются" tag="UNKM"/>
    <form  suffix="овшись" tag="UNKR"/>
  </table>
  <table name="UNK401" rads=".*(ве|ква|вор|вак|голо|купоро)" fast="-">
    <!-- 6 members: квасить ; голосить ; купоросить ; весить ; ворсить ; ваксить -->
    <form  suffix="сить" tag="UNK"/>
    <form  suffix="сил" tag="UNKL"/>
    <form  suffix="сила" tag="UNKL"/>
    <form  suffix="сили" tag="UNKL"/>
    <form  suffix="сило" tag="UNKL"/>
    <form  suffix="ся" tag="UNKP"/>
    <form  suffix="сим" tag="UNKW"/>
    <form  suffix="сит" tag="UNKW"/>
    <form  suffix="сите" tag="UNKW"/>
    <form  suffix="сишь" tag="UNKW"/>
    <form  suffix="сят" tag="UNKW"/>
    <form  suffix="шу" tag="UNKW"/>
  </table>
  <table name="UNK402" rads=".*(кра|тру|дуба|гундо|пылесо|куроле)" fast="-">
    <!-- 6 members: красить ; пылесосить ; трусить ; куролесить ; гундосить ; дубасить -->
    <form  suffix="сить" tag="UNK"/>
    <form  suffix="сил" tag="UNKL"/>
    <form  suffix="сила" tag="UNKL"/>
    <form  suffix="сили" tag="UNKL"/>
    <form  suffix="сило" tag="UNKL"/>
    <form  suffix="ся" tag="UNKP"/>
    <form  suffix="сь" tag="UNKT"/>
    <form  suffix="сьте" tag="UNKT"/>
    <form  suffix="сим" tag="UNKW"/>
    <form  suffix="сит" tag="UNKW"/>
    <form  suffix="сите" tag="UNKW"/>
    <form  suffix="сишь" tag="UNKW"/>
    <form  suffix="сят" tag="UNKW"/>
    <form  suffix="шу" tag="UNKW"/>
  </table>
  <table name="UNK403" rads=".*(со|на|за|обо|про|разо)й" fast="-">
    <!-- 6 members: сойтись ; обойтись ; найтись ; разойтись ; пройтись ; зайтись -->
    <form  suffix="тись" tag="UNK"/>
    <form  suffix="демся" tag="UNKM"/>
    <form  suffix="детесь" tag="UNKM"/>
    <form  suffix="дется" tag="UNKM"/>
    <form  suffix="дешься" tag="UNKM"/>
    <form  suffix="дусь" tag="UNKM"/>
    <form  suffix="дутся" tag="UNKM"/>
    <form  suffix="дёмся" tag="UNKM"/>
    <form  suffix="дётесь" tag="UNKM"/>
    <form  suffix="дётся" tag="UNKM"/>
    <form  suffix="дёшься" tag="UNKM"/>
  </table>
  <table name="UNK404" rads=".*(сбол|исчер|подпор|причер|перепор|обессмер)" fast="-">
    <!-- 6 members: сболтить ; перепортить ; исчертить ; обессмертить ; подпортить ; причертить -->
    <form  suffix="тить" tag="UNK"/>
    <form  suffix="тил" tag="UNKL"/>
    <form  suffix="тила" tag="UNKL"/>
    <form  suffix="тили" tag="UNKL"/>
    <form  suffix="тило" tag="UNKL"/>
    <form  suffix="тив" tag="UNKR"/>
    <form  suffix="тим" tag="UNKW"/>
    <form  suffix="тит" tag="UNKW"/>
    <form  suffix="тите" tag="UNKW"/>
    <form  suffix="тишь" tag="UNKW"/>
    <form  suffix="тят" tag="UNKW"/>
    <form  suffix="чу" tag="UNKW"/>
  </table>
  <table name="UNK405" rads=".*(се|ла|та|ха|бле|леле)" fast="-">
    <!-- 6 members: сеять ; лаять ; лелеять ; блеять ; таять ; хаять -->
    <form  suffix="ять" tag="UNK"/>
    <form  suffix="ял" tag="UNKL"/>
    <form  suffix="яла" tag="UNKL"/>
    <form  suffix="яли" tag="UNKL"/>
    <form  suffix="яло" tag="UNKL"/>
    <form  suffix="я" tag="UNKP"/>
    <form  suffix="й" tag="UNKT"/>
    <form  suffix="йте" tag="UNKT"/>
    <form  suffix="ем" tag="UNKU"/>
    <form  suffix="ет" tag="UNKU"/>
    <form  suffix="ете" tag="UNKU"/>
    <form  suffix="ешь" tag="UNKU"/>
    <form  suffix="ю" tag="UNKU"/>
    <form  suffix="ют" tag="UNKU"/>
  </table>
  <table name="UNK406" rads=".*(ощи|выщи|досы|защи|нащи)п" fast="-">
    <!-- 5 members: выщипать ; досыпать ; ощипать ; защипать ; нащипать -->
    <form  suffix="ать" tag="UNK"/>
    <form  suffix="ал" tag="UNKL"/>
    <form  suffix="ала" tag="UNKL"/>
    <form  suffix="али" tag="UNKL"/>
    <form  suffix="ало" tag="UNKL"/>
    <form  suffix="аем" tag="UNKM"/>
    <form  suffix="ает" tag="UNKM"/>
    <form  suffix="аете" tag="UNKM"/>
    <form  suffix="аешь" tag="UNKM"/>
    <form  suffix="аю" tag="UNKM"/>
    <form  suffix="ают" tag="UNKM"/>
    <form  suffix="ав" tag="UNKR"/>
    <form  suffix="лем" tag="UNKV"/>
    <form  suffix="лет" tag="UNKV"/>
    <form  suffix="лете" tag="UNKV"/>
    <form  suffix="лешь" tag="UNKV"/>
    <form  suffix="лю" tag="UNKV"/>
    <form  suffix="лют" tag="UNKV"/>
  </table>
  <table name="UNK407" rads=".*(дн|шпакл|шпатл|штабел|штемпел)" fast="-">
    <!-- 5 members: штемпелевать ; шпаклевать ; штабелевать ; шпатлевать ; дневать -->
    <form  suffix="евать" tag="UNK"/>
    <form  suffix="евал" tag="UNKL"/>
    <form  suffix="евала" tag="UNKL"/>
    <form  suffix="евали" tag="UNKL"/>
    <form  suffix="евало" tag="UNKL"/>
    <form  suffix="юя" tag="UNKQ"/>
    <form  suffix="юем" tag="UNKU"/>
    <form  suffix="юет" tag="UNKU"/>
    <form  suffix="юете" tag="UNKU"/>
    <form  suffix="юешь" tag="UNKU"/>
    <form  suffix="юю" tag="UNKU"/>
    <form  suffix="юют" tag="UNKU"/>
  </table>
  <table name="UNK408" rads=".*(во|мал|гор|цикл|мухл)" fast="-">
    <!-- 5 members: малевать ; циклевать ; мухлевать ; горевать ; воевать -->
    <form  suffix="евать" tag="UNK"/>
    <form  suffix="евал" tag="UNKL"/>
    <form  suffix="евала" tag="UNKL"/>
    <form  suffix="евали" tag="UNKL"/>
    <form  suffix="евало" tag="UNKL"/>
    <form  suffix="юя" tag="UNKQ"/>
    <form  suffix="юй" tag="UNKT"/>
    <form  suffix="юйте" tag="UNKT"/>
    <form  suffix="юем" tag="UNKU"/>
    <form  suffix="юет" tag="UNKU"/>
    <form  suffix="юете" tag="UNKU"/>
    <form  suffix="юешь" tag="UNKU"/>
    <form  suffix="юю" tag="UNKU"/>
    <form  suffix="юют" tag="UNKU"/>
  </table>
  <table name="UNK409" rads=".*(поки|захри|проси|просо|переки)п" fast="-">
    <!-- 5 members: перекипеть ; покипеть ; захрипеть ; просипеть ; просопеть -->
    <form  suffix="еть" tag="UNK"/>
    <form  suffix="ел" tag="UNKL"/>
    <form  suffix="ела" tag="UNKL"/>
    <form  suffix="ели" tag="UNKL"/>
    <form  suffix="ело" tag="UNKL"/>
    <form  suffix="ев" tag="UNKR"/>
    <form  suffix="им" tag="UNKY"/>
    <form  suffix="ит" tag="UNKY"/>
    <form  suffix="ите" tag="UNKY"/>
    <form  suffix="ишь" tag="UNKY"/>
    <form  suffix="лю" tag="UNKY"/>
    <form  suffix="ят" tag="UNKY"/>
  </table>
  <table name="UNK410" rads=".*бо" fast="-">
    <!-- 5 members: боец ; китобоец ; сваебоец ; молотобоец ; свинобоец -->
    <form  suffix="ец" tag="UNK"/>
    <form  suffix="йца" tag="UNKO"/>
    <form  suffix="йцам" tag="UNKO"/>
    <form  suffix="йцами" tag="UNKO"/>
    <form  suffix="йцах" tag="UNKO"/>
    <form  suffix="йце" tag="UNKO"/>
    <form  suffix="йцу" tag="UNKO"/>
    <form  suffix="йцы" tag="UNKO"/>
  </table>
  <table name="UNK411" rads=".*(дет|пут|люд|сосед)" fast="-">
    <!-- 5 members: нелюди ; дети ; соседи ; пути ; люди -->
    <form  suffix="и" tag="UNK"/>
    <form  suffix="ям" tag="UNKO"/>
    <form  suffix="ях" tag="UNKO"/>
  </table>
  <table name="UNK412" rads=".*(за|по|рас|под|пере)стел" fast="-">
    <!-- 5 members: застелить ; расстелить ; постелить ; перестелить ; подстелить -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="и" tag="UNKB"/>
    <form  suffix="ите" tag="UNKB"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="ив" tag="UNKR"/>
    <form  suffix="ем" tag="UNKY"/>
    <form  suffix="ет" tag="UNKY"/>
    <form  suffix="ете" tag="UNKY"/>
    <form  suffix="ешь" tag="UNKY"/>
    <form  suffix="ю" tag="UNKY"/>
    <form  suffix="ют" tag="UNKY"/>
  </table>
  <table name="UNK413" rads=".*(бу|пя|свя|магни|конопа)т" fast="-">
    <!-- 5 members: магнитить ; бутить ; конопатить ; святить ; пятить -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK414" rads=".*(ку|забо|горба|брюха|лохма)т" fast="-">
    <!-- 5 members: горбатить ; кутить ; брюхатить ; лохматить ; заботить -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="я" tag="UNKP"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK415" rads=".*(ме|пя|тра|забо|горба)т" fast="-">
    <!-- 5 members: тратиться ; метиться ; горбатиться ; пятиться ; заботиться -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ясь" tag="UNKP"/>
    <form  suffix="ься" tag="UNKT"/>
    <form  suffix="ьтесь" tag="UNKT"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK416" rads=".*(лыж|клеш|ступ|голов|шестер)" fast="-">
    <!-- 5 members: лыжня ; клешня ; шестерня ; ступня ; головня -->
    <form  suffix="ня" tag="UNK"/>
    <form  suffix="ен" tag="UNKG"/>
    <form  suffix="не" tag="UNKG"/>
    <form  suffix="ней" tag="UNKG"/>
    <form  suffix="нею" tag="UNKG"/>
    <form  suffix="ни" tag="UNKG"/>
    <form  suffix="ню" tag="UNKG"/>
    <form  suffix="ням" tag="UNKG"/>
    <form  suffix="нями" tag="UNKG"/>
    <form  suffix="нях" tag="UNKG"/>
    <form  suffix="нёй" tag="UNKG"/>
    <form  suffix="нёю" tag="UNKG"/>
  </table>
  <table name="UNK417" rads=".*(кол|бор)" fast="-">
    <!-- 5 members: наколоться ; приколоться ; проколоться ; колоться ; бороться -->
    <form  suffix="оться" tag="UNK"/>
    <form  suffix="ись" tag="UNKB"/>
    <form  suffix="итесь" tag="UNKB"/>
    <form  suffix="олась" tag="UNKL"/>
    <form  suffix="олись" tag="UNKL"/>
    <form  suffix="олось" tag="UNKL"/>
    <form  suffix="олся" tag="UNKL"/>
    <form  suffix="емся" tag="UNKM"/>
    <form  suffix="етесь" tag="UNKM"/>
    <form  suffix="ется" tag="UNKM"/>
    <form  suffix="ешься" tag="UNKM"/>
    <form  suffix="юсь" tag="UNKM"/>
    <form  suffix="ются" tag="UNKM"/>
  </table>
  <table name="UNK418" rads=".*(изры|проры|выпле|напле|попле)" fast="-">
    <!-- 5 members: прорыскать ; выплескать ; наплескать ; изрыскать ; поплескать -->
    <form  suffix="скать" tag="UNK"/>
    <form  suffix="скал" tag="UNKL"/>
    <form  suffix="скала" tag="UNKL"/>
    <form  suffix="скали" tag="UNKL"/>
    <form  suffix="скало" tag="UNKL"/>
    <form  suffix="скаем" tag="UNKM"/>
    <form  suffix="скает" tag="UNKM"/>
    <form  suffix="скаете" tag="UNKM"/>
    <form  suffix="скаешь" tag="UNKM"/>
    <form  suffix="скаю" tag="UNKM"/>
    <form  suffix="скают" tag="UNKM"/>
    <form  suffix="скав" tag="UNKR"/>
    <form  suffix="щем" tag="UNKY"/>
    <form  suffix="щет" tag="UNKY"/>
    <form  suffix="щете" tag="UNKY"/>
    <form  suffix="щешь" tag="UNKY"/>
    <form  suffix="щу" tag="UNKY"/>
    <form  suffix="щут" tag="UNKY"/>
  </table>
  <table name="UNK419" rads=".*(оты|пои|обы|дои|разы)" fast="-">
    <!-- 5 members: отыскаться ; поискаться ; разыскаться ; обыскаться ; доискаться -->
    <form  suffix="скаться" tag="UNK"/>
    <form  suffix="скалась" tag="UNKL"/>
    <form  suffix="скались" tag="UNKL"/>
    <form  suffix="скалось" tag="UNKL"/>
    <form  suffix="скался" tag="UNKL"/>
    <form  suffix="скавшись" tag="UNKR"/>
    <form  suffix="щемся" tag="UNKY"/>
    <form  suffix="щетесь" tag="UNKY"/>
    <form  suffix="щется" tag="UNKY"/>
    <form  suffix="щешься" tag="UNKY"/>
    <form  suffix="щусь" tag="UNKY"/>
    <form  suffix="щутся" tag="UNKY"/>
  </table>
  <table name="UNK420" rads=".*(кля|вые|кла|пря)" fast="-">
    <!-- 5 members: поклясться ; клясться ; выесться ; класться ; прясться -->
    <form  suffix="сться" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
  </table>
  <table name="UNK421" rads=".*(ме|топ|пря|шеп|обхохо)" fast="-">
    <!-- 5 members: обхохотаться ; топтаться ; прятаться ; метаться ; шептаться -->
    <form  suffix="таться" tag="UNK"/>
    <form  suffix="талась" tag="UNKL"/>
    <form  suffix="тались" tag="UNKL"/>
    <form  suffix="талось" tag="UNKL"/>
    <form  suffix="тался" tag="UNKL"/>
    <form  suffix="чемся" tag="UNKY"/>
    <form  suffix="четесь" tag="UNKY"/>
    <form  suffix="чется" tag="UNKY"/>
    <form  suffix="чешься" tag="UNKY"/>
    <form  suffix="чусь" tag="UNKY"/>
    <form  suffix="чутся" tag="UNKY"/>
  </table>
  <table name="UNK422" rads=".*(выкоп|взвин|развин|расфран|прифран)" fast="-">
    <!-- 5 members: выкоптиться ; расфрантиться ; прифрантиться ; взвинтиться ; развинтиться -->
    <form  suffix="титься" tag="UNK"/>
    <form  suffix="тилась" tag="UNKL"/>
    <form  suffix="тились" tag="UNKL"/>
    <form  suffix="тилось" tag="UNKL"/>
    <form  suffix="тился" tag="UNKL"/>
    <form  suffix="тившись" tag="UNKR"/>
    <form  suffix="тимся" tag="UNKW"/>
    <form  suffix="титесь" tag="UNKW"/>
    <form  suffix="тится" tag="UNKW"/>
    <form  suffix="тишься" tag="UNKW"/>
    <form  suffix="тятся" tag="UNKW"/>
    <form  suffix="чусь" tag="UNKW"/>
  </table>
  <table name="UNK423" rads=".*(отере|вытере|запере|подпере|исторгну)" fast="-">
    <!-- 5 members: вытереться ; исторгнуться ; запереться ; отереться ; подпереться -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="вшись" tag="UNKR"/>
  </table>
  <table name="UNK424" rads=".*(сгоня|разреза|распада|прореза|пересыпа)" fast="-">
    <!-- 5 members: разрезаться ; пересыпаться ; распадаться ; сгоняться ; прорезаться -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="ется" tag="UNKD"/>
    <form  suffix="ются" tag="UNKD"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="ясь" tag="UNKP"/>
    <form  suffix="вшись" tag="UNKR"/>
  </table>
  <table name="UNK425" rads=".*(утык|венч|счит|выкуп|просып)а" fast="-">
    <!-- 5 members: утыкаться ; выкупаться ; просыпаться ; венчаться ; считаться -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="йся" tag="UNKB"/>
    <form  suffix="йтесь" tag="UNKB"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="емся" tag="UNKM"/>
    <form  suffix="етесь" tag="UNKM"/>
    <form  suffix="ется" tag="UNKM"/>
    <form  suffix="ешься" tag="UNKM"/>
    <form  suffix="юсь" tag="UNKM"/>
    <form  suffix="ются" tag="UNKM"/>
    <form  suffix="ясь" tag="UNKP"/>
    <form  suffix="вшись" tag="UNKR"/>
  </table>
  <table name="UNK426" rads=".*(о|раз)де" fast="-">
    <!-- 5 members: раздеться ; разодеться ; одеться ; приодеться ; переодеться -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="вшись" tag="UNKR"/>
    <form  suffix="немся" tag="UNKV"/>
    <form  suffix="нетесь" tag="UNKV"/>
    <form  suffix="нется" tag="UNKV"/>
    <form  suffix="нешься" tag="UNKV"/>
    <form  suffix="нусь" tag="UNKV"/>
    <form  suffix="нутся" tag="UNKV"/>
  </table>
  <table name="UNK427" rads=".*(выдох|вверг|вторг|проник|подверг)н" fast="-">
    <!-- 5 members: выдохнуться ; ввергнуться ; проникнуться ; вторгнуться ; подвергнуться -->
    <form  suffix="уться" tag="UNK"/>
    <form  suffix="ись" tag="UNKB"/>
    <form  suffix="итесь" tag="UNKB"/>
    <form  suffix="емся" tag="UNKM"/>
    <form  suffix="етесь" tag="UNKM"/>
    <form  suffix="ется" tag="UNKM"/>
    <form  suffix="ешься" tag="UNKM"/>
    <form  suffix="усь" tag="UNKM"/>
    <form  suffix="утся" tag="UNKM"/>
    <form  suffix="увшись" tag="UNKR"/>
  </table>
  <table name="UNK428" rads=".*(м|н|р|в)" fast="-">
    <!-- 5 members: крыть ; мыть ; ныть ; рыть ; выть -->
    <form  suffix="ыть" tag="UNK"/>
    <form  suffix="ой" tag="UNKB"/>
    <form  suffix="ойте" tag="UNKB"/>
    <form  suffix="ыл" tag="UNKL"/>
    <form  suffix="ыла" tag="UNKL"/>
    <form  suffix="ыли" tag="UNKL"/>
    <form  suffix="ыло" tag="UNKL"/>
    <form  suffix="оем" tag="UNKM"/>
    <form  suffix="оет" tag="UNKM"/>
    <form  suffix="оете" tag="UNKM"/>
    <form  suffix="оешь" tag="UNKM"/>
    <form  suffix="ою" tag="UNKM"/>
    <form  suffix="оют" tag="UNKM"/>
    <form  suffix="оя" tag="UNKP"/>
  </table>
  <table name="UNK429" rads=".*(жне|шве|зме|яче|стру)" fast="-">
    <!-- 5 members: струя ; жнея ; швея ; змея ; ячея -->
    <form  suffix="я" tag="UNK"/>
    <form  suffix="е" tag="UNKG"/>
    <form  suffix="ей" tag="UNKG"/>
    <form  suffix="ею" tag="UNKG"/>
    <form  suffix="и" tag="UNKG"/>
    <form  suffix="й" tag="UNKG"/>
    <form  suffix="ю" tag="UNKG"/>
    <form  suffix="ям" tag="UNKG"/>
    <form  suffix="ями" tag="UNKG"/>
    <form  suffix="ях" tag="UNKG"/>
    <form  suffix="ёй" tag="UNKG"/>
    <form  suffix="ёю" tag="UNKG"/>
  </table>
  <table name="UNK430" rads=".*(у|за|по|на|при)м" fast="-">
    <!-- 5 members: примять ; замять ; помять ; умять ; намять -->
    <form  suffix="ять" tag="UNK"/>
    <form  suffix="ял" tag="UNKL"/>
    <form  suffix="яла" tag="UNKL"/>
    <form  suffix="яли" tag="UNKL"/>
    <form  suffix="яло" tag="UNKL"/>
    <form  suffix="яв" tag="UNKR"/>
    <form  suffix="нем" tag="UNKY"/>
    <form  suffix="нет" tag="UNKY"/>
    <form  suffix="нете" tag="UNKY"/>
    <form  suffix="нешь" tag="UNKY"/>
    <form  suffix="ну" tag="UNKY"/>
    <form  suffix="нут" tag="UNKY"/>
    <form  suffix="нём" tag="UNKY"/>
    <form  suffix="нёт" tag="UNKY"/>
    <form  suffix="нёте" tag="UNKY"/>
    <form  suffix="нёшь" tag="UNKY"/>
  </table>
  <table name="UNK431" rads=".*(заб|дост|заст|отст|наст)о" fast="-">
    <!-- 5 members: достояться ; забояться ; застояться ; отстояться ; настояться -->
    <form  suffix="яться" tag="UNK"/>
    <form  suffix="ялась" tag="UNKL"/>
    <form  suffix="ялись" tag="UNKL"/>
    <form  suffix="ялось" tag="UNKL"/>
    <form  suffix="ялся" tag="UNKL"/>
    <form  suffix="явшись" tag="UNKR"/>
    <form  suffix="имся" tag="UNKU"/>
    <form  suffix="итесь" tag="UNKU"/>
    <form  suffix="ится" tag="UNKU"/>
    <form  suffix="ишься" tag="UNKU"/>
    <form  suffix="юсь" tag="UNKU"/>
    <form  suffix="ятся" tag="UNKU"/>
  </table>
  <table name="UNK432" rads=".*(пока|разве|рассе|раска|понаде)" fast="-">
    <!-- 5 members: развеяться ; понадеяться ; покаяться ; рассеяться ; раскаяться -->
    <form  suffix="яться" tag="UNK"/>
    <form  suffix="ялась" tag="UNKL"/>
    <form  suffix="ялись" tag="UNKL"/>
    <form  suffix="ялось" tag="UNKL"/>
    <form  suffix="ялся" tag="UNKL"/>
    <form  suffix="явшись" tag="UNKR"/>
    <form  suffix="йся" tag="UNKT"/>
    <form  suffix="йтесь" tag="UNKT"/>
    <form  suffix="емся" tag="UNKU"/>
    <form  suffix="етесь" tag="UNKU"/>
    <form  suffix="ется" tag="UNKU"/>
    <form  suffix="ешься" tag="UNKU"/>
    <form  suffix="юсь" tag="UNKU"/>
    <form  suffix="ются" tag="UNKU"/>
  </table>
  <table name="UNK433" rads=".*(стих|запах|изверг|подвиг)" fast="-">
    <!-- 4 members: изверг ; стих ; подвиг ; запах -->
    <form  suffix="" tag="UNK"/>
    <form  suffix="а" tag="UNKK"/>
    <form  suffix="ам" tag="UNKK"/>
    <form  suffix="ами" tag="UNKK"/>
    <form  suffix="ах" tag="UNKK"/>
    <form  suffix="е" tag="UNKK"/>
    <form  suffix="и" tag="UNKK"/>
    <form  suffix="ов" tag="UNKK"/>
    <form  suffix="ом" tag="UNKK"/>
    <form  suffix="у" tag="UNKK"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
  </table>
  <table name="UNK434" rads=".*(обо|про|подо|пере)жд" fast="-">
    <!-- 4 members: подождать ; переждать ; обождать ; прождать -->
    <form  suffix="ать" tag="UNK"/>
    <form  suffix="ал" tag="UNKL"/>
    <form  suffix="ала" tag="UNKL"/>
    <form  suffix="али" tag="UNKL"/>
    <form  suffix="ало" tag="UNKL"/>
    <form  suffix="ав" tag="UNKR"/>
    <form  suffix="ем" tag="UNKU"/>
    <form  suffix="ет" tag="UNKU"/>
    <form  suffix="ете" tag="UNKU"/>
    <form  suffix="ешь" tag="UNKU"/>
    <form  suffix="у" tag="UNKU"/>
    <form  suffix="ут" tag="UNKU"/>
    <form  suffix="ём" tag="UNKU"/>
    <form  suffix="ёт" tag="UNKU"/>
    <form  suffix="ёте" tag="UNKU"/>
    <form  suffix="ёшь" tag="UNKU"/>
  </table>
  <table name="UNK435" rads=".*(до|по|про|пере)сп" fast="-">
    <!-- 4 members: доспать ; проспать ; поспать ; переспать -->
    <form  suffix="ать" tag="UNK"/>
    <form  suffix="ал" tag="UNKL"/>
    <form  suffix="ала" tag="UNKL"/>
    <form  suffix="али" tag="UNKL"/>
    <form  suffix="ало" tag="UNKL"/>
    <form  suffix="ав" tag="UNKR"/>
    <form  suffix="им" tag="UNKY"/>
    <form  suffix="ит" tag="UNKY"/>
    <form  suffix="ите" tag="UNKY"/>
    <form  suffix="ишь" tag="UNKY"/>
    <form  suffix="лю" tag="UNKY"/>
    <form  suffix="ят" tag="UNKY"/>
  </table>
  <table name="UNK436" rads=".*(жр|ор|вр|рв)" fast="-">
    <!-- 4 members: жрать ; орать ; врать ; рвать -->
    <form  suffix="ать" tag="UNK"/>
    <form  suffix="ал" tag="UNKL"/>
    <form  suffix="ала" tag="UNKL"/>
    <form  suffix="али" tag="UNKL"/>
    <form  suffix="ало" tag="UNKL"/>
    <form  suffix="ем" tag="UNKY"/>
    <form  suffix="ет" tag="UNKY"/>
    <form  suffix="ете" tag="UNKY"/>
    <form  suffix="ешь" tag="UNKY"/>
    <form  suffix="у" tag="UNKY"/>
    <form  suffix="ут" tag="UNKY"/>
    <form  suffix="ём" tag="UNKY"/>
    <form  suffix="ёт" tag="UNKY"/>
    <form  suffix="ёте" tag="UNKY"/>
    <form  suffix="ёшь" tag="UNKY"/>
  </table>
  <table name="UNK437" rads=".*(за|вы|ото|про)сп" fast="-">
    <!-- 4 members: отоспаться ; заспаться ; выспаться ; проспаться -->
    <form  suffix="аться" tag="UNK"/>
    <form  suffix="алась" tag="UNKL"/>
    <form  suffix="ались" tag="UNKL"/>
    <form  suffix="алось" tag="UNKL"/>
    <form  suffix="ался" tag="UNKL"/>
    <form  suffix="авшись" tag="UNKR"/>
    <form  suffix="имся" tag="UNKY"/>
    <form  suffix="итесь" tag="UNKY"/>
    <form  suffix="ится" tag="UNKY"/>
    <form  suffix="ишься" tag="UNKY"/>
    <form  suffix="люсь" tag="UNKY"/>
    <form  suffix="ятся" tag="UNKY"/>
  </table>
  <table name="UNK438" rads=".*(по|на|об|ис)треп" fast="-">
    <!-- 4 members: потрепаться ; натрепаться ; обтрепаться ; истрепаться -->
    <form  suffix="аться" tag="UNK"/>
    <form  suffix="алась" tag="UNKL"/>
    <form  suffix="ались" tag="UNKL"/>
    <form  suffix="алось" tag="UNKL"/>
    <form  suffix="ался" tag="UNKL"/>
    <form  suffix="авшись" tag="UNKR"/>
    <form  suffix="лемся" tag="UNKV"/>
    <form  suffix="летесь" tag="UNKV"/>
    <form  suffix="лется" tag="UNKV"/>
    <form  suffix="лешься" tag="UNKV"/>
    <form  suffix="люсь" tag="UNKV"/>
    <form  suffix="лются" tag="UNKV"/>
  </table>
  <table name="UNK439" rads=".*(г|л|лихор)а" fast="-">
    <!-- 4 members: гладить ; гадить ; ладить ; лихорадить -->
    <form  suffix="дить" tag="UNK"/>
    <form  suffix="дил" tag="UNKL"/>
    <form  suffix="дила" tag="UNKL"/>
    <form  suffix="дили" tag="UNKL"/>
    <form  suffix="дило" tag="UNKL"/>
    <form  suffix="дя" tag="UNKP"/>
    <form  suffix="дь" tag="UNKT"/>
    <form  suffix="дьте" tag="UNKT"/>
    <form  suffix="дим" tag="UNKW"/>
    <form  suffix="дит" tag="UNKW"/>
    <form  suffix="дите" tag="UNKW"/>
    <form  suffix="дишь" tag="UNKW"/>
    <form  suffix="дят" tag="UNKW"/>
    <form  suffix="жу" tag="UNKW"/>
  </table>
  <table name="UNK440" rads=".*(оп|пок|нап)л" fast="-">
    <!-- 4 members: поклевать ; поплевать ; наплевать ; оплевать -->
    <form  suffix="евать" tag="UNK"/>
    <form  suffix="евал" tag="UNKL"/>
    <form  suffix="евала" tag="UNKL"/>
    <form  suffix="евали" tag="UNKL"/>
    <form  suffix="евало" tag="UNKL"/>
    <form  suffix="евав" tag="UNKR"/>
    <form  suffix="юй" tag="UNKT"/>
    <form  suffix="юйте" tag="UNKT"/>
    <form  suffix="юем" tag="UNKV"/>
    <form  suffix="юет" tag="UNKV"/>
    <form  suffix="юете" tag="UNKV"/>
    <form  suffix="юешь" tag="UNKV"/>
    <form  suffix="юю" tag="UNKV"/>
    <form  suffix="юют" tag="UNKV"/>
    <form  suffix="юём" tag="UNKV"/>
    <form  suffix="юёт" tag="UNKV"/>
    <form  suffix="юёте" tag="UNKV"/>
    <form  suffix="юёшь" tag="UNKV"/>
  </table>
  <table name="UNK441" rads=".*(стуш|взбуш|размеж|разбуш)" fast="-">
    <!-- 4 members: стушеваться ; размежеваться ; разбушеваться ; взбушеваться -->
    <form  suffix="еваться" tag="UNK"/>
    <form  suffix="евалась" tag="UNKL"/>
    <form  suffix="евались" tag="UNKL"/>
    <form  suffix="евалось" tag="UNKL"/>
    <form  suffix="евался" tag="UNKL"/>
    <form  suffix="евавшись" tag="UNKR"/>
    <form  suffix="уемся" tag="UNKU"/>
    <form  suffix="уетесь" tag="UNKU"/>
    <form  suffix="уется" tag="UNKU"/>
    <form  suffix="уешься" tag="UNKU"/>
    <form  suffix="уюсь" tag="UNKU"/>
    <form  suffix="уются" tag="UNKU"/>
  </table>
  <table name="UNK442" rads=".*(ис|от|на|рас)пл" fast="-">
    <!-- 4 members: исплеваться ; отплеваться ; наплеваться ; расплеваться -->
    <form  suffix="еваться" tag="UNK"/>
    <form  suffix="евалась" tag="UNKL"/>
    <form  suffix="евались" tag="UNKL"/>
    <form  suffix="евалось" tag="UNKL"/>
    <form  suffix="евался" tag="UNKL"/>
    <form  suffix="евавшись" tag="UNKR"/>
    <form  suffix="юемся" tag="UNKV"/>
    <form  suffix="юетесь" tag="UNKV"/>
    <form  suffix="юется" tag="UNKV"/>
    <form  suffix="юешься" tag="UNKV"/>
    <form  suffix="ююсь" tag="UNKV"/>
    <form  suffix="юются" tag="UNKV"/>
    <form  suffix="юёмся" tag="UNKV"/>
    <form  suffix="юётесь" tag="UNKV"/>
    <form  suffix="юётся" tag="UNKV"/>
    <form  suffix="юёшься" tag="UNKV"/>
  </table>
  <table name="UNK443" rads=".*(завер|отвер|повер|разле)т" fast="-">
    <!-- 4 members: завертеться ; отвертеться ; повертеться ; разлететься -->
    <form  suffix="еться" tag="UNK"/>
    <form  suffix="елась" tag="UNKL"/>
    <form  suffix="елись" tag="UNKL"/>
    <form  suffix="елось" tag="UNKL"/>
    <form  suffix="елся" tag="UNKL"/>
    <form  suffix="евшись" tag="UNKR"/>
    <form  suffix="ись" tag="UNKT"/>
    <form  suffix="итесь" tag="UNKT"/>
    <form  suffix="имся" tag="UNKY"/>
    <form  suffix="итесь" tag="UNKY"/>
    <form  suffix="ится" tag="UNKY"/>
    <form  suffix="ишься" tag="UNKY"/>
    <form  suffix="ятся" tag="UNKY"/>
  </table>
  <table name="UNK444" rads=".*(выл|прол|перел|безобр)а" fast="-">
    <!-- 4 members: безобразить ; вылазить ; перелазить ; пролазить -->
    <form  suffix="зить" tag="UNK"/>
    <form  suffix="зил" tag="UNKL"/>
    <form  suffix="зила" tag="UNKL"/>
    <form  suffix="зили" tag="UNKL"/>
    <form  suffix="зило" tag="UNKL"/>
    <form  suffix="жу" tag="UNKW"/>
    <form  suffix="зим" tag="UNKW"/>
    <form  suffix="зит" tag="UNKW"/>
    <form  suffix="зите" tag="UNKW"/>
    <form  suffix="зишь" tag="UNKW"/>
    <form  suffix="зят" tag="UNKW"/>
  </table>
  <table name="UNK445" rads=".*(ба|боя|тата|болга)р" fast="-">
    <!-- 4 members: барин ; боярин ; болгарин ; татарин -->
    <form  suffix="ин" tag="UNK"/>
    <form  suffix="" tag="UNKO"/>
    <form  suffix="ам" tag="UNKO"/>
    <form  suffix="ами" tag="UNKO"/>
    <form  suffix="ах" tag="UNKO"/>
    <form  suffix="ина" tag="UNKO"/>
    <form  suffix="ине" tag="UNKO"/>
    <form  suffix="ином" tag="UNKO"/>
    <form  suffix="ину" tag="UNKO"/>
    <form  suffix="ы" tag="UNKO"/>
  </table>
  <table name="UNK446" rads=".*(у|вы|на|про)стел" fast="-">
    <!-- 4 members: простелить ; устелить ; выстелить ; настелить -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="ив" tag="UNKR"/>
    <form  suffix="ем" tag="UNKY"/>
    <form  suffix="ет" tag="UNKY"/>
    <form  suffix="ете" tag="UNKY"/>
    <form  suffix="ешь" tag="UNKY"/>
    <form  suffix="ю" tag="UNKY"/>
    <form  suffix="ют" tag="UNKY"/>
  </table>
  <table name="UNK447" rads=".*(в|л|п|б)" fast="-">
    <!-- 4 members: вить ; лить ; пить ; бить -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="ьем" tag="UNKV"/>
    <form  suffix="ьет" tag="UNKV"/>
    <form  suffix="ьете" tag="UNKV"/>
    <form  suffix="ьешь" tag="UNKV"/>
    <form  suffix="ью" tag="UNKV"/>
    <form  suffix="ьют" tag="UNKV"/>
    <form  suffix="ьём" tag="UNKV"/>
    <form  suffix="ьёт" tag="UNKV"/>
    <form  suffix="ьёте" tag="UNKV"/>
    <form  suffix="ьёшь" tag="UNKV"/>
  </table>
  <table name="UNK448" rads=".*(кле|дра|стро|беспоко)" fast="-">
    <!-- 4 members: беспокоить ; строить ; клеить ; драить -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="я" tag="UNKP"/>
    <form  suffix="й" tag="UNKT"/>
    <form  suffix="йте" tag="UNKT"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="ю" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK449" rads=".*(ме|тра|баламу|колошма)т" fast="-">
    <!-- 4 members: колошматить ; баламутить ; метить ; тратить -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="я" tag="UNKP"/>
    <form  suffix="ь" tag="UNKT"/>
    <form  suffix="ьте" tag="UNKT"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK450" rads=".*(дослед|назализ|дифрагир|сцинтиллир)" fast="-">
    <!-- 4 members: сцинтиллировать ; назализовать ; доследовать ; дифрагировать -->
    <form  suffix="овать" tag="UNK"/>
    <form  suffix="овал" tag="UNKL"/>
    <form  suffix="овала" tag="UNKL"/>
    <form  suffix="овали" tag="UNKL"/>
    <form  suffix="овало" tag="UNKL"/>
    <form  suffix="уя" tag="UNKQ"/>
    <form  suffix="овав" tag="UNKR"/>
  </table>
  <table name="UNK451" rads=".*(атрофир|назализ|абсорбир|бюрократизир)" fast="-">
    <!-- 4 members: абсорбироваться ; бюрократизироваться ; атрофироваться ; назализоваться -->
    <form  suffix="оваться" tag="UNK"/>
    <form  suffix="овалась" tag="UNKL"/>
    <form  suffix="овались" tag="UNKL"/>
    <form  suffix="овалось" tag="UNKL"/>
    <form  suffix="овался" tag="UNKL"/>
    <form  suffix="уясь" tag="UNKQ"/>
    <form  suffix="овавшись" tag="UNKR"/>
  </table>
  <table name="UNK452" rads=".*(скол|отпор|пропол)" fast="-">
    <!-- 4 members: исколоть ; отпороть ; прополоть ; сколоть -->
    <form  suffix="оть" tag="UNK"/>
    <form  suffix="ол" tag="UNKL"/>
    <form  suffix="ола" tag="UNKL"/>
    <form  suffix="оли" tag="UNKL"/>
    <form  suffix="оло" tag="UNKL"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="ю" tag="UNKM"/>
    <form  suffix="ют" tag="UNKM"/>
    <form  suffix="ов" tag="UNKR"/>
  </table>
  <table name="UNK453" rads=".*(на|вы|по|рас)пле" fast="-">
    <!-- 4 members: наплескаться ; расплескаться ; выплескаться ; поплескаться -->
    <form  suffix="скаться" tag="UNK"/>
    <form  suffix="скалась" tag="UNKL"/>
    <form  suffix="скались" tag="UNKL"/>
    <form  suffix="скалось" tag="UNKL"/>
    <form  suffix="скался" tag="UNKL"/>
    <form  suffix="скаемся" tag="UNKM"/>
    <form  suffix="скаетесь" tag="UNKM"/>
    <form  suffix="скается" tag="UNKM"/>
    <form  suffix="скаешься" tag="UNKM"/>
    <form  suffix="скаюсь" tag="UNKM"/>
    <form  suffix="скаются" tag="UNKM"/>
    <form  suffix="скавшись" tag="UNKR"/>
    <form  suffix="щемся" tag="UNKY"/>
    <form  suffix="щетесь" tag="UNKY"/>
    <form  suffix="щется" tag="UNKY"/>
    <form  suffix="щешься" tag="UNKY"/>
    <form  suffix="щусь" tag="UNKY"/>
    <form  suffix="щутся" tag="UNKY"/>
  </table>
  <table name="UNK454" rads=".*(за|чихво|бесче|благове)" fast="-">
    <!-- 4 members: чихвостить ; благовестить ; застить ; бесчестить -->
    <form  suffix="стить" tag="UNK"/>
    <form  suffix="стил" tag="UNKL"/>
    <form  suffix="стила" tag="UNKL"/>
    <form  suffix="стили" tag="UNKL"/>
    <form  suffix="стило" tag="UNKL"/>
    <form  suffix="стя" tag="UNKP"/>
    <form  suffix="стим" tag="UNKW"/>
    <form  suffix="стит" tag="UNKW"/>
    <form  suffix="стите" tag="UNKW"/>
    <form  suffix="стишь" tag="UNKW"/>
    <form  suffix="стят" tag="UNKW"/>
    <form  suffix="щу" tag="UNKW"/>
  </table>
  <table name="UNK455" rads=".*(вкр|поп|подкр|прокр)а" fast="-">
    <!-- 4 members: подкрасться ; прокрасться ; вкрасться ; попасться -->
    <form  suffix="сться" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="вшись" tag="UNKR"/>
    <form  suffix="демся" tag="UNKY"/>
    <form  suffix="детесь" tag="UNKY"/>
    <form  suffix="дется" tag="UNKY"/>
    <form  suffix="дешься" tag="UNKY"/>
    <form  suffix="дусь" tag="UNKY"/>
    <form  suffix="дутся" tag="UNKY"/>
    <form  suffix="дёмся" tag="UNKY"/>
    <form  suffix="дётесь" tag="UNKY"/>
    <form  suffix="дётся" tag="UNKY"/>
    <form  suffix="дёшься" tag="UNKY"/>
  </table>
  <table name="UNK456" rads=".*(вин|чер|фин|коп)" fast="-">
    <!-- 4 members: винтить ; чертить ; финтить ; коптить -->
    <form  suffix="тить" tag="UNK"/>
    <form  suffix="ти" tag="UNKB"/>
    <form  suffix="тите" tag="UNKB"/>
    <form  suffix="тил" tag="UNKL"/>
    <form  suffix="тила" tag="UNKL"/>
    <form  suffix="тили" tag="UNKL"/>
    <form  suffix="тило" tag="UNKL"/>
    <form  suffix="тя" tag="UNKP"/>
    <form  suffix="тим" tag="UNKW"/>
    <form  suffix="тит" tag="UNKW"/>
    <form  suffix="тите" tag="UNKW"/>
    <form  suffix="тишь" tag="UNKW"/>
    <form  suffix="тят" tag="UNKW"/>
    <form  suffix="чу" tag="UNKW"/>
  </table>
  <table name="UNK457" rads=".*(в|вы|раз|запро)да" fast="-">
    <!-- 4 members: запродаться ; вдаться ; раздаться ; выдаться -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="вшись" tag="UNKR"/>
    <form  suffix="димся" tag="UNKY"/>
    <form  suffix="дитесь" tag="UNKY"/>
    <form  suffix="дутся" tag="UNKY"/>
    <form  suffix="мся" tag="UNKY"/>
    <form  suffix="стся" tag="UNKY"/>
    <form  suffix="шься" tag="UNKY"/>
  </table>
  <table name="UNK458" rads=".*(рык|слиз|запах|промок)н" fast="-">
    <!-- 4 members: слизнуть ; промокнуть ; рыкнуть ; запахнуть -->
    <form  suffix="уть" tag="UNK"/>
    <form  suffix="и" tag="UNKB"/>
    <form  suffix="ите" tag="UNKB"/>
    <form  suffix="ул" tag="UNKL"/>
    <form  suffix="ула" tag="UNKL"/>
    <form  suffix="ули" tag="UNKL"/>
    <form  suffix="уло" tag="UNKL"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="у" tag="UNKM"/>
    <form  suffix="ут" tag="UNKM"/>
    <form  suffix="ув" tag="UNKR"/>
    <form  suffix="ем" tag="UNKU"/>
    <form  suffix="ет" tag="UNKU"/>
    <form  suffix="ете" tag="UNKU"/>
    <form  suffix="ешь" tag="UNKU"/>
    <form  suffix="у" tag="UNKU"/>
    <form  suffix="ут" tag="UNKU"/>
    <form  suffix="ём" tag="UNKU"/>
    <form  suffix="ёт" tag="UNKU"/>
    <form  suffix="ёте" tag="UNKU"/>
    <form  suffix="ёшь" tag="UNKU"/>
  </table>
  <table name="UNK459" rads=".*(отма|пома|прома|заколы)" fast="-">
    <!-- 4 members: отмахать ; помахать ; промахать ; заколыхать -->
    <form  suffix="хать" tag="UNK"/>
    <form  suffix="хал" tag="UNKL"/>
    <form  suffix="хала" tag="UNKL"/>
    <form  suffix="хали" tag="UNKL"/>
    <form  suffix="хало" tag="UNKL"/>
    <form  suffix="хаем" tag="UNKM"/>
    <form  suffix="хает" tag="UNKM"/>
    <form  suffix="хаете" tag="UNKM"/>
    <form  suffix="хаешь" tag="UNKM"/>
    <form  suffix="хаю" tag="UNKM"/>
    <form  suffix="хают" tag="UNKM"/>
    <form  suffix="хав" tag="UNKR"/>
    <form  suffix="шем" tag="UNKY"/>
    <form  suffix="шет" tag="UNKY"/>
    <form  suffix="шете" tag="UNKY"/>
    <form  suffix="шешь" tag="UNKY"/>
    <form  suffix="шу" tag="UNKY"/>
    <form  suffix="шут" tag="UNKY"/>
  </table>
  <table name="UNK460" rads=".*(пром|разв|приум|полузакр)" fast="-">
    <!-- 4 members: промыться ; приумыться ; полузакрыться ; развыться -->
    <form  suffix="ыться" tag="UNK"/>
    <form  suffix="ылась" tag="UNKL"/>
    <form  suffix="ылись" tag="UNKL"/>
    <form  suffix="ылось" tag="UNKL"/>
    <form  suffix="ылся" tag="UNKL"/>
    <form  suffix="оемся" tag="UNKM"/>
    <form  suffix="оетесь" tag="UNKM"/>
    <form  suffix="оется" tag="UNKM"/>
    <form  suffix="оешься" tag="UNKM"/>
    <form  suffix="оюсь" tag="UNKM"/>
    <form  suffix="оются" tag="UNKM"/>
    <form  suffix="ывшись" tag="UNKR"/>
  </table>
  <table name="UNK461" rads=".*(вы|до|по|про)сто" fast="-">
    <!-- 4 members: выстоять ; достоять ; постоять ; простоять -->
    <form  suffix="ять" tag="UNK"/>
    <form  suffix="ял" tag="UNKL"/>
    <form  suffix="яла" tag="UNKL"/>
    <form  suffix="яли" tag="UNKL"/>
    <form  suffix="яло" tag="UNKL"/>
    <form  suffix="яв" tag="UNKR"/>
    <form  suffix="й" tag="UNKT"/>
    <form  suffix="йте" tag="UNKT"/>
    <form  suffix="им" tag="UNKU"/>
    <form  suffix="ит" tag="UNKU"/>
    <form  suffix="ите" tag="UNKU"/>
    <form  suffix="ишь" tag="UNKU"/>
    <form  suffix="ю" tag="UNKU"/>
    <form  suffix="ят" tag="UNKU"/>
  </table>
  <table name="UNK462" rads=".*(ла|ма|ка|наде)" fast="-">
    <!-- 4 members: лаяться ; маяться ; каяться ; надеяться -->
    <form  suffix="яться" tag="UNK"/>
    <form  suffix="ялась" tag="UNKL"/>
    <form  suffix="ялись" tag="UNKL"/>
    <form  suffix="ялось" tag="UNKL"/>
    <form  suffix="ялся" tag="UNKL"/>
    <form  suffix="ясь" tag="UNKP"/>
    <form  suffix="йся" tag="UNKT"/>
    <form  suffix="йтесь" tag="UNKT"/>
    <form  suffix="емся" tag="UNKU"/>
    <form  suffix="етесь" tag="UNKU"/>
    <form  suffix="ется" tag="UNKU"/>
    <form  suffix="ешься" tag="UNKU"/>
    <form  suffix="юсь" tag="UNKU"/>
    <form  suffix="ются" tag="UNKU"/>
  </table>
  <table name="UNK463" rads=".*(ор|ос|коз|кот)" fast="-">
    <!-- 4 members: орёл ; козёл ; котёл ; осёл -->
    <form  suffix="ёл" tag="UNK"/>
    <form  suffix="ла" tag="UNKO"/>
    <form  suffix="лам" tag="UNKO"/>
    <form  suffix="лами" tag="UNKO"/>
    <form  suffix="лах" tag="UNKO"/>
    <form  suffix="ле" tag="UNKO"/>
    <form  suffix="лов" tag="UNKO"/>
    <form  suffix="лом" tag="UNKO"/>
    <form  suffix="лу" tag="UNKO"/>
    <form  suffix="лы" tag="UNKO"/>
  </table>
  <table name="UNK464" rads=".*(мч|держ|стуч)" fast="-">
    <!-- 3 members: держаться ; мчаться ; стучаться -->
    <form  suffix="аться" tag="UNK"/>
    <form  suffix="алась" tag="UNKL"/>
    <form  suffix="ались" tag="UNKL"/>
    <form  suffix="алось" tag="UNKL"/>
    <form  suffix="ался" tag="UNKL"/>
    <form  suffix="ись" tag="UNKT"/>
    <form  suffix="атся" tag="UNKU"/>
    <form  suffix="имся" tag="UNKU"/>
    <form  suffix="итесь" tag="UNKU"/>
    <form  suffix="ится" tag="UNKU"/>
    <form  suffix="ишься" tag="UNKU"/>
    <form  suffix="усь" tag="UNKU"/>
  </table>
  <table name="UNK465" rads=".*з" fast="-">
    <!-- 3 members: зваться ; назваться ; дозваться -->
    <form  suffix="ваться" tag="UNK"/>
    <form  suffix="валась" tag="UNKL"/>
    <form  suffix="вались" tag="UNKL"/>
    <form  suffix="валось" tag="UNKL"/>
    <form  suffix="вался" tag="UNKL"/>
    <form  suffix="вавшись" tag="UNKR"/>
    <form  suffix="овемся" tag="UNKY"/>
    <form  suffix="оветесь" tag="UNKY"/>
    <form  suffix="овется" tag="UNKY"/>
    <form  suffix="овешься" tag="UNKY"/>
    <form  suffix="овусь" tag="UNKY"/>
    <form  suffix="овутся" tag="UNKY"/>
    <form  suffix="овёмся" tag="UNKY"/>
    <form  suffix="овётесь" tag="UNKY"/>
    <form  suffix="овётся" tag="UNKY"/>
    <form  suffix="овёшься" tag="UNKY"/>
  </table>
  <table name="UNK466" rads=".*(п|б|к)л" fast="-">
    <!-- 3 members: плевать ; блевать ; клевать -->
    <form  suffix="евать" tag="UNK"/>
    <form  suffix="евал" tag="UNKL"/>
    <form  suffix="евала" tag="UNKL"/>
    <form  suffix="евали" tag="UNKL"/>
    <form  suffix="евало" tag="UNKL"/>
    <form  suffix="юя" tag="UNKQ"/>
    <form  suffix="юй" tag="UNKT"/>
    <form  suffix="юйте" tag="UNKT"/>
    <form  suffix="юем" tag="UNKV"/>
    <form  suffix="юет" tag="UNKV"/>
    <form  suffix="юете" tag="UNKV"/>
    <form  suffix="юешь" tag="UNKV"/>
    <form  suffix="юю" tag="UNKV"/>
    <form  suffix="юют" tag="UNKV"/>
    <form  suffix="юём" tag="UNKV"/>
    <form  suffix="юёт" tag="UNKV"/>
    <form  suffix="юёте" tag="UNKV"/>
    <form  suffix="юёшь" tag="UNKV"/>
  </table>
  <table name="UNK467" rads=".*(до|на|раз)во" fast="-">
    <!-- 3 members: развоеваться ; довоеваться ; навоеваться -->
    <form  suffix="еваться" tag="UNK"/>
    <form  suffix="евалась" tag="UNKL"/>
    <form  suffix="евались" tag="UNKL"/>
    <form  suffix="евалось" tag="UNKL"/>
    <form  suffix="евался" tag="UNKL"/>
    <form  suffix="евавшись" tag="UNKR"/>
    <form  suffix="юемся" tag="UNKU"/>
    <form  suffix="юетесь" tag="UNKU"/>
    <form  suffix="юется" tag="UNKU"/>
    <form  suffix="юешься" tag="UNKU"/>
    <form  suffix="ююсь" tag="UNKU"/>
    <form  suffix="юются" tag="UNKU"/>
  </table>
  <table name="UNK468" rads=".*(ж|бич|рубц)" fast="-">
    <!-- 3 members: жеваться ; бичеваться ; рубцеваться -->
    <form  suffix="еваться" tag="UNK"/>
    <form  suffix="евалась" tag="UNKL"/>
    <form  suffix="евались" tag="UNKL"/>
    <form  suffix="евалось" tag="UNKL"/>
    <form  suffix="евался" tag="UNKL"/>
    <form  suffix="уясь" tag="UNKQ"/>
  </table>
  <table name="UNK469" rads=".*(го|смот|лицез)р" fast="-">
    <!-- 3 members: гореть ; смотреть ; лицезреть -->
    <form  suffix="еть" tag="UNK"/>
    <form  suffix="ел" tag="UNKL"/>
    <form  suffix="ела" tag="UNKL"/>
    <form  suffix="ели" tag="UNKL"/>
    <form  suffix="ело" tag="UNKL"/>
    <form  suffix="и" tag="UNKT"/>
    <form  suffix="ите" tag="UNKT"/>
    <form  suffix="им" tag="UNKY"/>
    <form  suffix="ит" tag="UNKY"/>
    <form  suffix="ите" tag="UNKY"/>
    <form  suffix="ишь" tag="UNKY"/>
    <form  suffix="ю" tag="UNKY"/>
    <form  suffix="ят" tag="UNKY"/>
  </table>
  <table name="UNK470" rads=".*(извер|довер|рассвис)т" fast="-">
    <!-- 3 members: извертеться ; довертеться ; рассвистеться -->
    <form  suffix="еться" tag="UNK"/>
    <form  suffix="елась" tag="UNKL"/>
    <form  suffix="елись" tag="UNKL"/>
    <form  suffix="елось" tag="UNKL"/>
    <form  suffix="елся" tag="UNKL"/>
    <form  suffix="евшись" tag="UNKR"/>
    <form  suffix="имся" tag="UNKY"/>
    <form  suffix="итесь" tag="UNKY"/>
    <form  suffix="ится" tag="UNKY"/>
    <form  suffix="ишься" tag="UNKY"/>
    <form  suffix="ятся" tag="UNKY"/>
  </table>
  <table name="UNK471" rads=".*(про|раз)бе" fast="-">
    <!-- 3 members: поразбежаться ; пробежаться ; разбежаться -->
    <form  suffix="жаться" tag="UNK"/>
    <form  suffix="жалась" tag="UNKL"/>
    <form  suffix="жались" tag="UNKL"/>
    <form  suffix="жалось" tag="UNKL"/>
    <form  suffix="жался" tag="UNKL"/>
    <form  suffix="жавшись" tag="UNKR"/>
    <form  suffix="гусь" tag="UNKY"/>
    <form  suffix="гутся" tag="UNKY"/>
    <form  suffix="жимся" tag="UNKY"/>
    <form  suffix="житесь" tag="UNKY"/>
    <form  suffix="жится" tag="UNKY"/>
    <form  suffix="жишься" tag="UNKY"/>
  </table>
  <table name="UNK472" rads=".*(в|с|об)ре" fast="-">
    <!-- 3 members: врезаться ; обрезаться ; срезаться -->
    <form  suffix="заться" tag="UNK"/>
    <form  suffix="залась" tag="UNKL"/>
    <form  suffix="зались" tag="UNKL"/>
    <form  suffix="залось" tag="UNKL"/>
    <form  suffix="зался" tag="UNKL"/>
    <form  suffix="заемся" tag="UNKM"/>
    <form  suffix="заетесь" tag="UNKM"/>
    <form  suffix="зается" tag="UNKM"/>
    <form  suffix="заешься" tag="UNKM"/>
    <form  suffix="заюсь" tag="UNKM"/>
    <form  suffix="заются" tag="UNKM"/>
    <form  suffix="заясь" tag="UNKP"/>
    <form  suffix="завшись" tag="UNKR"/>
    <form  suffix="жемся" tag="UNKY"/>
    <form  suffix="жетесь" tag="UNKY"/>
    <form  suffix="жется" tag="UNKY"/>
    <form  suffix="жешься" tag="UNKY"/>
    <form  suffix="жусь" tag="UNKY"/>
    <form  suffix="жутся" tag="UNKY"/>
  </table>
  <table name="UNK473" rads=".*(у|раз|пере)во" fast="-">
    <!-- 3 members: развозиться ; увозиться ; перевозиться -->
    <form  suffix="зиться" tag="UNK"/>
    <form  suffix="зилась" tag="UNKL"/>
    <form  suffix="зились" tag="UNKL"/>
    <form  suffix="зилось" tag="UNKL"/>
    <form  suffix="зился" tag="UNKL"/>
    <form  suffix="зясь" tag="UNKP"/>
    <form  suffix="жусь" tag="UNKW"/>
    <form  suffix="зимся" tag="UNKW"/>
    <form  suffix="зитесь" tag="UNKW"/>
    <form  suffix="зится" tag="UNKW"/>
    <form  suffix="зишься" tag="UNKW"/>
    <form  suffix="зятся" tag="UNKW"/>
  </table>
  <table name="UNK474" rads=".*(во|гро|гру)" fast="-">
    <!-- 3 members: грозиться ; грузиться ; возиться -->
    <form  suffix="зиться" tag="UNK"/>
    <form  suffix="зись" tag="UNKB"/>
    <form  suffix="зитесь" tag="UNKB"/>
    <form  suffix="зилась" tag="UNKL"/>
    <form  suffix="зились" tag="UNKL"/>
    <form  suffix="зилось" tag="UNKL"/>
    <form  suffix="зился" tag="UNKL"/>
    <form  suffix="зясь" tag="UNKP"/>
    <form  suffix="жусь" tag="UNKW"/>
    <form  suffix="зимся" tag="UNKW"/>
    <form  suffix="зитесь" tag="UNKW"/>
    <form  suffix="зится" tag="UNKW"/>
    <form  suffix="зишься" tag="UNKW"/>
    <form  suffix="зятся" tag="UNKW"/>
  </table>
  <table name="UNK475" rads=".*(дл|по|кор)" fast="-">
    <!-- 3 members: корить ; длить ; поить -->
    <form  suffix="ить" tag="UNK"/>
    <form  suffix="и" tag="UNKB"/>
    <form  suffix="ите" tag="UNKB"/>
    <form  suffix="ил" tag="UNKL"/>
    <form  suffix="ила" tag="UNKL"/>
    <form  suffix="или" tag="UNKL"/>
    <form  suffix="ило" tag="UNKL"/>
    <form  suffix="им" tag="UNKW"/>
    <form  suffix="ит" tag="UNKW"/>
    <form  suffix="ите" tag="UNKW"/>
    <form  suffix="ишь" tag="UNKW"/>
    <form  suffix="ю" tag="UNKW"/>
    <form  suffix="ят" tag="UNKW"/>
  </table>
  <table name="UNK476" rads=".*(суч|друж|утащ)" fast="-">
    <!-- 3 members: дружиться ; сучиться ; утащиться -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="атся" tag="UNKW"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="усь" tag="UNKW"/>
  </table>
  <table name="UNK477" rads=".*(задав|скреп|разонрав)" fast="-">
    <!-- 3 members: разонравиться ; задавиться ; скрепиться -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="люсь" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK478" rads=".*(п|б|в)" fast="-">
    <!-- 3 members: питься ; биться ; виться -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ьемся" tag="UNKV"/>
    <form  suffix="ьетесь" tag="UNKV"/>
    <form  suffix="ьется" tag="UNKV"/>
    <form  suffix="ьешься" tag="UNKV"/>
    <form  suffix="ьюсь" tag="UNKV"/>
    <form  suffix="ьются" tag="UNKV"/>
    <form  suffix="ьёмся" tag="UNKV"/>
    <form  suffix="ьётесь" tag="UNKV"/>
    <form  suffix="ьётся" tag="UNKV"/>
    <form  suffix="ьёшься" tag="UNKV"/>
  </table>
  <table name="UNK479" rads=".*(пок|стр)о" fast="-">
    <!-- 3 members: покоиться ; строиться ; беспокоиться -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ясь" tag="UNKP"/>
    <form  suffix="йся" tag="UNKT"/>
    <form  suffix="йтесь" tag="UNKT"/>
    <form  suffix="имся" tag="UNKW"/>
    <form  suffix="итесь" tag="UNKW"/>
    <form  suffix="ится" tag="UNKW"/>
    <form  suffix="ишься" tag="UNKW"/>
    <form  suffix="юсь" tag="UNKW"/>
    <form  suffix="ятся" tag="UNKW"/>
  </table>
  <table name="UNK480" rads=".*(дл|коп|гнезд)" fast="-">
    <!-- 3 members: копиться ; гнездиться ; длиться -->
    <form  suffix="иться" tag="UNK"/>
    <form  suffix="ись" tag="UNKB"/>
    <form  suffix="итесь" tag="UNKB"/>
    <form  suffix="ится" tag="UNKD"/>
    <form  suffix="ятся" tag="UNKD"/>
    <form  suffix="илась" tag="UNKL"/>
    <form  suffix="ились" tag="UNKL"/>
    <form  suffix="илось" tag="UNKL"/>
    <form  suffix="ился" tag="UNKL"/>
    <form  suffix="ясь" tag="UNKP"/>
  </table>
  <table name="UNK481" rads=".*(т|хн|мурл)ы" fast="-">
    <!-- 3 members: мурлыкать ; хныкать ; тыкать -->
    <form  suffix="кать" tag="UNK"/>
    <form  suffix="кай" tag="UNKB"/>
    <form  suffix="кайте" tag="UNKB"/>
    <form  suffix="кал" tag="UNKL"/>
    <form  suffix="кала" tag="UNKL"/>
    <form  suffix="кали" tag="UNKL"/>
    <form  suffix="кало" tag="UNKL"/>
    <form  suffix="каем" tag="UNKM"/>
    <form  suffix="кает" tag="UNKM"/>
    <form  suffix="каете" tag="UNKM"/>
    <form  suffix="каешь" tag="UNKM"/>
    <form  suffix="каю" tag="UNKM"/>
    <form  suffix="кают" tag="UNKM"/>
    <form  suffix="кая" tag="UNKP"/>
    <form  suffix="чем" tag="UNKY"/>
    <form  suffix="чет" tag="UNKY"/>
    <form  suffix="чете" tag="UNKY"/>
    <form  suffix="чешь" tag="UNKY"/>
    <form  suffix="чу" tag="UNKY"/>
    <form  suffix="чут" tag="UNKY"/>
  </table>
  <table name="UNK482" rads=".*(кли|ска|пла)" fast="-">
    <!-- 3 members: кликать ; скакать ; плакать -->
    <form  suffix="кать" tag="UNK"/>
    <form  suffix="кал" tag="UNKL"/>
    <form  suffix="кала" tag="UNKL"/>
    <form  suffix="кали" tag="UNKL"/>
    <form  suffix="кало" tag="UNKL"/>
    <form  suffix="чем" tag="UNKY"/>
    <form  suffix="чет" tag="UNKY"/>
    <form  suffix="чете" tag="UNKY"/>
    <form  suffix="чешь" tag="UNKY"/>
    <form  suffix="чу" tag="UNKY"/>
    <form  suffix="чут" tag="UNKY"/>
  </table>
  <table name="UNK483" rads=".*(запута|разобще|сконфуже)н" fast="-">
    <!-- 3 members: разобщенный ; сконфуженный ; запутанный -->
    <form  suffix="ный" tag="UNK"/>
    <form  suffix="ная" tag="UNKA"/>
    <form  suffix="ного" tag="UNKA"/>
    <form  suffix="ное" tag="UNKA"/>
    <form  suffix="ной" tag="UNKA"/>
    <form  suffix="ном" tag="UNKA"/>
    <form  suffix="ному" tag="UNKA"/>
    <form  suffix="ною" tag="UNKA"/>
    <form  suffix="ную" tag="UNKA"/>
    <form  suffix="ные" tag="UNKA"/>
    <form  suffix="ным" tag="UNKA"/>
    <form  suffix="ными" tag="UNKA"/>
    <form  suffix="ных" tag="UNKA"/>
    <form  suffix="нее" tag="UNKE"/>
    <form  suffix="ней" tag="UNKE"/>
    <form  suffix="" tag="UNKS"/>
    <form  suffix="а" tag="UNKS"/>
    <form  suffix="о" tag="UNKS"/>
    <form  suffix="ы" tag="UNKS"/>
    <form  suffix="но" tag="UNKZ"/>
  </table>
  <table name="UNK484" rads=".*(нас|расс|раск)" fast="-">
    <!-- 3 members: насовать ; рассовать ; расковать -->
    <form  suffix="овать" tag="UNK"/>
    <form  suffix="овал" tag="UNKL"/>
    <form  suffix="овала" tag="UNKL"/>
    <form  suffix="овали" tag="UNKL"/>
    <form  suffix="овало" tag="UNKL"/>
    <form  suffix="овав" tag="UNKR"/>
    <form  suffix="уем" tag="UNKV"/>
    <form  suffix="ует" tag="UNKV"/>
    <form  suffix="уете" tag="UNKV"/>
    <form  suffix="уешь" tag="UNKV"/>
    <form  suffix="ую" tag="UNKV"/>
    <form  suffix="уют" tag="UNKV"/>
    <form  suffix="уём" tag="UNKV"/>
    <form  suffix="уёт" tag="UNKV"/>
    <form  suffix="уёте" tag="UNKV"/>
    <form  suffix="уёшь" tag="UNKV"/>
  </table>
  <table name="UNK485" rads=".*(к|с|сн)" fast="-">
    <!-- 3 members: ковать ; совать ; сновать -->
    <form  suffix="овать" tag="UNK"/>
    <form  suffix="овал" tag="UNKL"/>
    <form  suffix="овала" tag="UNKL"/>
    <form  suffix="овали" tag="UNKL"/>
    <form  suffix="овало" tag="UNKL"/>
    <form  suffix="уя" tag="UNKQ"/>
    <form  suffix="уй" tag="UNKT"/>
    <form  suffix="уйте" tag="UNKT"/>
    <form  suffix="уем" tag="UNKV"/>
    <form  suffix="ует" tag="UNKV"/>
    <form  suffix="уете" tag="UNKV"/>
    <form  suffix="уешь" tag="UNKV"/>
    <form  suffix="ую" tag="UNKV"/>
    <form  suffix="уют" tag="UNKV"/>
    <form  suffix="уём" tag="UNKV"/>
    <form  suffix="уёт" tag="UNKV"/>
    <form  suffix="уёте" tag="UNKV"/>
    <form  suffix="уёшь" tag="UNKV"/>
  </table>
  <table name="UNK486" rads=".*(о|на)пи" fast="-">
    <!-- 3 members: пописать ; описать ; написать -->
    <form  suffix="сать" tag="UNK"/>
    <form  suffix="сай" tag="UNKB"/>
    <form  suffix="сайте" tag="UNKB"/>
    <form  suffix="сал" tag="UNKL"/>
    <form  suffix="сала" tag="UNKL"/>
    <form  suffix="сали" tag="UNKL"/>
    <form  suffix="сало" tag="UNKL"/>
    <form  suffix="саем" tag="UNKM"/>
    <form  suffix="сает" tag="UNKM"/>
    <form  suffix="саете" tag="UNKM"/>
    <form  suffix="саешь" tag="UNKM"/>
    <form  suffix="саю" tag="UNKM"/>
    <form  suffix="сают" tag="UNKM"/>
    <form  suffix="сав" tag="UNKR"/>
    <form  suffix="ши" tag="UNKT"/>
    <form  suffix="шите" tag="UNKT"/>
    <form  suffix="шем" tag="UNKY"/>
    <form  suffix="шет" tag="UNKY"/>
    <form  suffix="шете" tag="UNKY"/>
    <form  suffix="шешь" tag="UNKY"/>
    <form  suffix="шу" tag="UNKY"/>
    <form  suffix="шут" tag="UNKY"/>
  </table>
  <table name="UNK487" rads=".*(фор|вопро|отпро)" fast="-">
    <!-- 3 members: вопросить ; отпросить ; форсить -->
    <form  suffix="сить" tag="UNK"/>
    <form  suffix="сил" tag="UNKL"/>
    <form  suffix="сила" tag="UNKL"/>
    <form  suffix="сили" tag="UNKL"/>
    <form  suffix="сило" tag="UNKL"/>
    <form  suffix="сим" tag="UNKW"/>
    <form  suffix="сит" tag="UNKW"/>
    <form  suffix="сите" tag="UNKW"/>
    <form  suffix="сишь" tag="UNKW"/>
    <form  suffix="сят" tag="UNKW"/>
    <form  suffix="шу" tag="UNKW"/>
  </table>
  <table name="UNK488" rads=".*(кук|нано|взно)" fast="-">
    <!-- 3 members: наноситься ; кукситься ; взноситься -->
    <form  suffix="ситься" tag="UNK"/>
    <form  suffix="силась" tag="UNKL"/>
    <form  suffix="сились" tag="UNKL"/>
    <form  suffix="силось" tag="UNKL"/>
    <form  suffix="сился" tag="UNKL"/>
    <form  suffix="сясь" tag="UNKP"/>
    <form  suffix="симся" tag="UNKW"/>
    <form  suffix="ситесь" tag="UNKW"/>
    <form  suffix="сится" tag="UNKW"/>
    <form  suffix="сишься" tag="UNKW"/>
    <form  suffix="сятся" tag="UNKW"/>
    <form  suffix="шусь" tag="UNKW"/>
  </table>
  <table name="UNK489" rads=".*(и|сы|запле)" fast="-">
    <!-- 3 members: искаться ; сыскаться ; заплескаться -->
    <form  suffix="скаться" tag="UNK"/>
    <form  suffix="скалась" tag="UNKL"/>
    <form  suffix="скались" tag="UNKL"/>
    <form  suffix="скалось" tag="UNKL"/>
    <form  suffix="скался" tag="UNKL"/>
    <form  suffix="щемся" tag="UNKY"/>
    <form  suffix="щетесь" tag="UNKY"/>
    <form  suffix="щется" tag="UNKY"/>
    <form  suffix="щешься" tag="UNKY"/>
    <form  suffix="щусь" tag="UNKY"/>
    <form  suffix="щутся" tag="UNKY"/>
  </table>
  <table name="UNK490" rads=".*блю" fast="-">
    <!-- 3 members: блюсти ; соблюсти ; наблюсти -->
    <form  suffix="сти" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
  </table>
  <table name="UNK491" rads=".*(чи|по|кре)" fast="-">
    <!-- 3 members: креститься ; чиститься ; поститься -->
    <form  suffix="ститься" tag="UNK"/>
    <form  suffix="стись" tag="UNKB"/>
    <form  suffix="ститесь" tag="UNKB"/>
    <form  suffix="стилась" tag="UNKL"/>
    <form  suffix="стились" tag="UNKL"/>
    <form  suffix="стилось" tag="UNKL"/>
    <form  suffix="стился" tag="UNKL"/>
    <form  suffix="стясь" tag="UNKP"/>
    <form  suffix="стимся" tag="UNKW"/>
    <form  suffix="ститесь" tag="UNKW"/>
    <form  suffix="стится" tag="UNKW"/>
    <form  suffix="стишься" tag="UNKW"/>
    <form  suffix="стятся" tag="UNKW"/>
    <form  suffix="щусь" tag="UNKW"/>
  </table>
  <table name="UNK492" rads=".*па" fast="-">
    <!-- 3 members: пасть ; напасть ; пропасть -->
    <form  suffix="сть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="стей" tag="UNKN"/>
    <form  suffix="сти" tag="UNKN"/>
    <form  suffix="стью" tag="UNKN"/>
    <form  suffix="стям" tag="UNKN"/>
    <form  suffix="стями" tag="UNKN"/>
    <form  suffix="стях" tag="UNKN"/>
    <form  suffix="в" tag="UNKR"/>
    <form  suffix="дем" tag="UNKY"/>
    <form  suffix="дет" tag="UNKY"/>
    <form  suffix="дете" tag="UNKY"/>
    <form  suffix="дешь" tag="UNKY"/>
    <form  suffix="ду" tag="UNKY"/>
    <form  suffix="дут" tag="UNKY"/>
    <form  suffix="дём" tag="UNKY"/>
    <form  suffix="дёт" tag="UNKY"/>
    <form  suffix="дёте" tag="UNKY"/>
    <form  suffix="дёшь" tag="UNKY"/>
  </table>
  <table name="UNK493" rads=".*(п|в)ая" fast="-">
    <!-- 3 members: паять ; изваять ; ваять -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="й" tag="UNKB"/>
    <form  suffix="йте" tag="UNKB"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
  </table>
  <table name="UNK494" rads=".*(недо|запро|пересоз)да" fast="-">
    <!-- 3 members: пересоздать ; запродать ; недодать -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="в" tag="UNKR"/>
    <form  suffix="дим" tag="UNKY"/>
    <form  suffix="дите" tag="UNKY"/>
    <form  suffix="дут" tag="UNKY"/>
    <form  suffix="м" tag="UNKY"/>
    <form  suffix="ст" tag="UNKY"/>
    <form  suffix="шь" tag="UNKY"/>
  </table>
  <table name="UNK495" rads=".*(п|с)лы" fast="-">
    <!-- 3 members: сплыть ; плыть ; слыть -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="вем" tag="UNKY"/>
    <form  suffix="вет" tag="UNKY"/>
    <form  suffix="вете" tag="UNKY"/>
    <form  suffix="вешь" tag="UNKY"/>
    <form  suffix="ву" tag="UNKY"/>
    <form  suffix="вут" tag="UNKY"/>
    <form  suffix="вём" tag="UNKY"/>
    <form  suffix="вёт" tag="UNKY"/>
    <form  suffix="вёте" tag="UNKY"/>
    <form  suffix="вёшь" tag="UNKY"/>
  </table>
  <table name="UNK496" rads=".*(докуп|оббег|почерп)а" fast="-">
    <!-- 3 members: почерпать ; докупать ; оббегать -->
    <form  suffix="ть" tag="UNK"/>
    <form  suffix="л" tag="UNKL"/>
    <form  suffix="ла" tag="UNKL"/>
    <form  suffix="ли" tag="UNKL"/>
    <form  suffix="ло" tag="UNKL"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="ю" tag="UNKM"/>
    <form  suffix="ют" tag="UNKM"/>
    <form  suffix="я" tag="UNKP"/>
    <form  suffix="в" tag="UNKR"/>
  </table>
  <table name="UNK497" rads=".*(на|раз|про)ду" fast="-">
    <!-- 3 members: раздуться ; продуться ; надуться -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="вшись" tag="UNKR"/>
    <form  suffix="емся" tag="UNKV"/>
    <form  suffix="етесь" tag="UNKV"/>
    <form  suffix="ется" tag="UNKV"/>
    <form  suffix="ешься" tag="UNKV"/>
    <form  suffix="юсь" tag="UNKV"/>
    <form  suffix="ются" tag="UNKV"/>
  </table>
  <table name="UNK498" rads=".*(о|рас)ста" fast="-">
    <!-- 3 members: достаться ; расстаться ; остаться -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="вшись" tag="UNKR"/>
    <form  suffix="немся" tag="UNKY"/>
    <form  suffix="нетесь" tag="UNKY"/>
    <form  suffix="нется" tag="UNKY"/>
    <form  suffix="нешься" tag="UNKY"/>
    <form  suffix="нусь" tag="UNKY"/>
    <form  suffix="нутся" tag="UNKY"/>
  </table>
  <table name="UNK499" rads=".*(высып|пробег|простир)а" fast="-">
    <!-- 3 members: простираться ; высыпаться ; пробегаться -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="лся" tag="UNKL"/>
    <form  suffix="емся" tag="UNKM"/>
    <form  suffix="етесь" tag="UNKM"/>
    <form  suffix="ется" tag="UNKM"/>
    <form  suffix="ешься" tag="UNKM"/>
    <form  suffix="юсь" tag="UNKM"/>
    <form  suffix="ются" tag="UNKM"/>
    <form  suffix="ясь" tag="UNKP"/>
    <form  suffix="вшись" tag="UNKR"/>
  </table>
  <table name="UNK500" rads=".*грыз" fast="-">
    <!-- 3 members: перегрызться ; грызться ; вгрызться -->
    <form  suffix="ться" tag="UNK"/>
    <form  suffix="лась" tag="UNKL"/>
    <form  suffix="лись" tag="UNKL"/>
    <form  suffix="лось" tag="UNKL"/>
    <form  suffix="ся" tag="UNKL"/>
  </table>
  <table name="UNK501" rads=".*(увя|засты|просты)н" fast="-">
    <!-- 3 members: простынуть ; увянуть ; застынуть -->
    <form  suffix="уть" tag="UNK"/>
    <form  suffix="ем" tag="UNKM"/>
    <form  suffix="ет" tag="UNKM"/>
    <form  suffix="ете" tag="UNKM"/>
    <form  suffix="ешь" tag="UNKM"/>
    <form  suffix="у" tag="UNKM"/>
    <form  suffix="ут" tag="UNKM"/>
    <form  suffix="ув" tag="UNKR"/>
    <form  suffix="ь" tag="UNKT"/>
    <form  suffix="ьте" tag="UNKT"/>
  </table>
  <table name="UNK502" rads=".*(трях|втолк|хлест)н" fast="-">
    <!-- 3 members: втолкнуться ; тряхнуться ; хлестнуться -->
    <form  suffix="уться" tag="UNK"/>
    <form  suffix="улась" tag="UNKL"/>
    <form  suffix="улись" tag="UNKL"/>
    <form  suffix="улось" tag="UNKL"/>
    <form  suffix="улся" tag="UNKL"/>
    <form  suffix="емся" tag="UNKU"/>
    <form  suffix="етесь" tag="UNKU"/>
    <form  suffix="ется" tag="UNKU"/>
    <form  suffix="ешься" tag="UNKU"/>
    <form  suffix="усь" tag="UNKU"/>
    <form  suffix="утся" tag="UNKU"/>
    <form  suffix="ёмся" tag="UNKU"/>
    <form  suffix="ётесь" tag="UNKU"/>
    <form  suffix="ётся" tag="UNKU"/>
    <form  suffix="ёшься" tag="UNKU"/>
  </table>
  <table name="UNK503" rads=".*(съ|про|разъ)е" fast="-">
    <!-- 3 members: разъехаться ; съехаться ; проехаться -->
    <form  suffix="хаться" tag="UNK"/>
    <form  suffix="халась" tag="UNKL"/>
    <form  suffix="хались" tag="UNKL"/>
    <form  suffix="халось" tag="UNKL"/>
    <form  suffix="хался" tag="UNKL"/>
    <form  suffix="хавшись" tag="UNKR"/>
    <form  suffix="демся" tag="UNKV"/>
    <form  suffix="детесь" tag="UNKV"/>
    <form  suffix="дется" tag="UNKV"/>
    <form  suffix="дешься" tag="UNKV"/>
    <form  suffix="дусь" tag="UNKV"/>
    <form  suffix="дутся" tag="UNKV"/>
  </table>
  <table name="UNK504" rads=".*(отпа|напа|набре)" fast="-">
    <!-- 3 members: отпахаться ; напахаться ; набрехаться -->
    <form  suffix="хаться" tag="UNK"/>
    <form  suffix="халась" tag="UNKL"/>
    <form  suffix="хались" tag="UNKL"/>
    <form  suffix="халось" tag="UNKL"/>
    <form  suffix="хался" tag="UNKL"/>
    <form  suffix="хавшись" tag="UNKR"/>
    <form  suffix="шемся" tag="UNKY"/>
    <form  suffix="шетесь" tag="UNKY"/>
    <form  suffix="шется" tag="UNKY"/>
    <form  suffix="шешься" tag="UNKY"/>
    <form  suffix="шусь" tag="UNKY"/>
    <form  suffix="шутся" tag="UNKY"/>
  </table>
  <table name="UNK505" rads=".*воло" fast="-">
    <!-- 3 members: приволочься ; волочься ; поволочься -->
    <form  suffix="чься" tag="UNK"/>
    <form  suffix="клась" tag="UNKL"/>
    <form  suffix="клись" tag="UNKL"/>
    <form  suffix="клось" tag="UNKL"/>
    <form  suffix="кся" tag="UNKL"/>
    <form  suffix="кусь" tag="UNKM"/>
    <form  suffix="кутся" tag="UNKM"/>
    <form  suffix="чемся" tag="UNKM"/>
    <form  suffix="четесь" tag="UNKM"/>
    <form  suffix="чется" tag="UNKM"/>
    <form  suffix="чешься" tag="UNKM"/>
    <form  suffix="чёмся" tag="UNKM"/>
    <form  suffix="чётесь" tag="UNKM"/>
    <form  suffix="чётся" tag="UNKM"/>
    <form  suffix="чёшься" tag="UNKM"/>
  </table>
  <table name="UNK506" rads=".*сып" fast="-">
    <!-- 3 members: пересыпь ; сыпь ; насыпь -->
    <form  suffix="ь" tag="UNK"/>
    <form  suffix="ьте" tag="UNKB"/>
    <form  suffix="ей" tag="UNKN"/>
    <form  suffix="и" tag="UNKN"/>
    <form  suffix="ью" tag="UNKN"/>
    <form  suffix="ям" tag="UNKN"/>
    <form  suffix="ями" tag="UNKN"/>
    <form  suffix="ях" tag="UNKN"/>
  </table>
  <table name="UNK507" rads=".*(о|за|об)сме" fast="-">
    <!-- 3 members: засмеять ; осмеять ; обсмеять -->
    <form  suffix="ять" tag="UNK"/>
    <form  suffix="ял" tag="UNKL"/>
    <form  suffix="яла" tag="UNKL"/>
    <form  suffix="яли" tag="UNKL"/>
    <form  suffix="яло" tag="UNKL"/>
    <form  suffix="яв" tag="UNKR"/>
    <form  suffix="ем" tag="UNKV"/>
    <form  suffix="ет" tag="UNKV"/>
    <form  suffix="ете" tag="UNKV"/>
    <form  suffix="ешь" tag="UNKV"/>
    <form  suffix="ю" tag="UNKV"/>
    <form  suffix="ют" tag="UNKV"/>
    <form  suffix="ём" tag="UNKV"/>
    <form  suffix="ёт" tag="UNKV"/>
    <form  suffix="ёте" tag="UNKV"/>
    <form  suffix="ёшь" tag="UNKV"/>
  </table>
  <table name="UNK508" rads=".*(у|на|пере)сто" fast="-">
    <!-- 3 members: настоять ; перестоять ; устоять -->
    <form  suffix="ять" tag="UNK"/>
    <form  suffix="ял" tag="UNKL"/>
    <form  suffix="яла" tag="UNKL"/>
    <form  suffix="яли" tag="UNKL"/>
    <form  suffix="яло" tag="UNKL"/>
    <form  suffix="яв" tag="UNKR"/>
    <form  suffix="им" tag="UNKU"/>
    <form  suffix="ит" tag="UNKU"/>
    <form  suffix="ите" tag="UNKU"/>
    <form  suffix="ишь" tag="UNKU"/>
    <form  suffix="ю" tag="UNKU"/>
    <form  suffix="ят" tag="UNKU"/>
  </table>
  <table name="UNK509" rads=".*(за|по|рас)сме" fast="-">
    <!-- 3 members: рассмеяться ; засмеяться ; посмеяться -->
    <form  suffix="яться" tag="UNK"/>
    <form  suffix="ялась" tag="UNKL"/>
    <form  suffix="ялись" tag="UNKL"/>
    <form  suffix="ялось" tag="UNKL"/>
    <form  suffix="ялся" tag="UNKL"/>
    <form  suffix="явшись" tag="UNKR"/>
    <form  suffix="йся" tag="UNKT"/>
    <form  suffix="йтесь" tag="UNKT"/>
    <form  suffix="емся" tag="UNKV"/>
    <form  suffix="етесь" tag="UNKV"/>
    <form  suffix="ется" tag="UNKV"/>
    <form  suffix="ешься" tag="UNKV"/>
    <form  suffix="юсь" tag="UNKV"/>
    <form  suffix="ются" tag="UNKV"/>
    <form  suffix="ёмся" tag="UNKV"/>
    <form  suffix="ётесь" tag="UNKV"/>
    <form  suffix="ётся" tag="UNKV"/>
    <form  suffix="ёшься" tag="UNKV"/>
  </table>
</description>
